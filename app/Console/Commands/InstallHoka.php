<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class InstallHoka extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install:hoka';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This function is used install hoka';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
    */
    public function handle()
    {
        $env = env('APP_ENV', '');
        if (strtolower($env) == 'production') {
            $this->info("");
            $this->info("<fg=yellow>*********************************</>");
            $this->info("<fg=yellow>*   Application In Production!  *</>");
            $this->info("<fg=yellow>*********************************</>");

            if($this->confirm('Do you really wish to run this command? (yes|no)'))
            {
                \Artisan::call('key:generate', [
                    '--force' => true,
                ]);

                \Artisan::call('migrate:fresh', [
                    '--force' => true,
                ]);

                \Artisan::call('db:seed', [
                    '--force' => true,
                ]);

                \Artisan::call('passport:install', [
                    '--force' => true,
                ]);

                $this->info('Hoka install successfully!');
            } else {
                $this->info("<fg=yellow>Command Cancelled!</>");
            }
        } else { 
            \Artisan::call("key:generate");
            \Artisan::call("migrate:fresh");
            \Artisan::call("db:seed");
            \Artisan::call("passport:install");
            
            $this->info('Hoka install successfully!');
        }
    }
}
