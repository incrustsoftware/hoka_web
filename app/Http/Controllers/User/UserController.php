<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Config;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Lcobucci\JWT\Parser;
use Soumen\Agent\Agent;
use App\Helper\UserHelper;
use App\Helper\HttpRequestHelper;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    protected $success;
    protected $error;
    protected $userModel;

    /**
     * Instantiate a user controller instances.
    */
    public function __construct() {
        $this->success = getSuccessMessages();
        $this->error = getErrorMessages();
        $this->userModel = new User();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
    * Display a listing of the users.
    *
    * @return Illuminate\View\View
    */
    public function loginView()
    {
        return view('auth.login');
    }

     /**
    * This function is used to load logout 
    *
    * @return Illuminate\View\View
    */
    public function logoutView() {
        return view('logout');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * This method is used for user login.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $validator = $this->userModel
            ->validateUserLoginData($request->all());

        if ($validator->fails()) {
            return getValidatorFailedResponse($validator);
        }

        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return getErrorResponse([$this->error['invalid_login']]);
        }

        //$remember_me = $request->remember_me === 'true' ? true : false;
        if (Auth::attempt(['email' => $request->email, 'password' => $request
            ->password, 'is_active' => 1])) { //, $remember_me
            
            return $this->userModel->manageUserLogin($request);
        } else {
            
            return getErrorResponse([$this->error['invalid_login']]);
        }
    }

    /**
     * Logout user session.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $value = $request->bearerToken();
        if ($value) {
            $user = $request->user();
            $id = (new Parser())->parse($value)->getHeader('jti');
            $token = $request->user()->tokens->find($id);
            $token->revoke();
        }

        return getSuccessResponse('', [$this->success['logout_user']]);
    }

    /**
     * Change user password.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request)
    {
        $rules = [
            'user_id' => 'required|integer',
            'old_password' => 'required|min:6',
            'new_password' => 'required_with:confirm_password|min:6|different:old_password|same:confirm_password',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return getValidatorFailedResponse($validator);
        }

        $user = User::where('id', $request->user_id)
            ->where('is_active', 1)
            ->where('is_delete', 0)
            ->first();

        if ($user) {
            error_logs($user);
            if(!Hash::check($request->old_password, $user->password)) {
                return getErrorResponse([getErrorMessages('old_password_error')]);
            }

            error_logs('hash match');
            $password = UserHelper::generatePassword($request->new_password);
            $user->password = $password['encrypt_password'];
            $user->update();

            error_logs('password_success');
            return getSuccessResponse('', [getSuccessMessages('password_success')]);
        } else {
            error_logs('user_not_found');
            return getErrorResponse([getErrorMessages('user_not_found')]);
        }
    }

    /**
    * Display a support page
    *
    * @return Illuminate\View\View
    */
    public function supportView()
    {
        return view('support.support');
    }
}
