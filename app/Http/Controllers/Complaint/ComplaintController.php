<?php

namespace App\Http\Controllers\Complaint;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Config;
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Model\ProductComplaintData;
use Aws\Credentials\CredentialProvider;
use Aws\Textract\TextractClient;
use Aws\Exception\AwsException;
use Illuminate\Support\Str;
use App\Helper\NotificationHelper;

class ComplaintController extends Controller
{
	/**
     * Register a new complaint (old).
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function registerComplaintData(Request $request)
	{
		$rules = [
            'product_details' => 'required|string',
            'dealer_name' => 'required|string',
            'product_images' => 'required',
            'article_no' => 'required|string',
            'customer_id' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return getValidatorFailedResponse($validator);
        }

		return (new ProductComplaintData())->manageComplaintData($request->all());
	}

	/**
	* Register a new complaint (new).
	* @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
	*/
	public function registerComplaint(Request $request)
	{
		error_logs('*****************************************');
		error_logs('Register complaint request :');
		error_logs('dealer_name : '.$request->dealer_name);
		error_logs('dealer_email : '.$request->email);
		error_logs('gender : '.$request->gender);
		error_logs('model_name : '.$request->model_name);
		error_logs('version : '.$request->version);
		error_logs('model_number : '.$request->model_number);
		error_logs('color_code : '.$request->color_code);
		error_logs('size : '.$request->size);
		error_logs('free_text : '.$request->free_text);
		error_logs('manufacturer_code : '.$request->manufacturer_code);
		//error_logs('claim_type : '.$request->claim_type);
		error_logs('purchase_date : '.$request->purchase_date);

		if (is_array($request->product_images)) {
			error_logs('product_images count : '.count($request->product_images));
		}

        if (isset($request->customer_reference_number)) {
            error_logs('customer_reference_number : '.$request->customer_reference_number);
        }

        if (isset($request->device_type)) {
            error_logs('Device Type : '.$request->device_type);
        }

        if (isset($request->is_sport)) {
            error_logs('Is Sport : '.$request->is_sport);
        }

		try {

			$rules = [
	            'product_details' => 'required|string|regex:/^[^<>]*$/',
	            'dealer_name' => 'required|string|regex:/^[^<>]*$/',
	            'product_images' => 'required|array|min:4|max:10',
	            'article_no' => 'required|string|alpha_num',
	            'customer_id' => 'required|alpha_num',
	            'address1' => 'required|string|regex:/^[^<>]*$/',
	            'address2' => 'required|string|regex:/^[^<>]*$/',
	            'city' => 'required|string|regex:/^[^<>]*$/',
	            'company' => 'required|string|regex:/^[^<>]*$/',
	            'phone' => 'required|string|alpha_num',
	            'zip_code' => 'required|string|alpha_num',
	            //'claim_type' => 'required',
	            //'purchase_date' => 'required',
	            'email' => 'required|email',
	            'size' => 'required|numeric',
	            'model_number' => 'required|alpha_num',
	            'model_name' => 'required|string|regex:/^[^<>]*$/',
	            'color_code' => 'required|alpha_num',
	            'manufacturer_code' => 'required|alpha_num',
	            'gender' => 'required|alpha_num',
	            'version' => 'required|string|regex:/^[^<>]*$/',
	            'free_text' => 'required|string|regex:/^[^<>]*$/',
	        ];

	        $validator = Validator::make($request->all(), $rules);
	        if ($validator->fails()) {
	        	error_logs('Validation error: ');
	        	//error_logs(json_encode($validator->errors()->all()));
	        	$validator_response = getValidatorFailedResponse($validator);
	        	error_logs(json_encode($validator_response));
	            return $validator_response;
	        }

	        $requestData = $request->all();
	        if (!isset($request->customer_reference_number)) {
	            $requestData['is_sport'] = 0;
	        }

	        $fromDate = Carbon::now()->subSeconds(180)->toDateTimeString();
	    	$toDate = Carbon::now()->toDateTimeString();

	        $check_complaint_is_exist = ProductComplaintData::where('gender', $request->gender)
	        	->where('model_name',$request->model_name)
	        	->where('version',$request->version)
	        	->where('model_number',$request->model_number)
	        	->where('size',$request->size)
	        	->where('color_code',$request->color_code)
	        	->where('manufacturer_code',$request->manufacturer_code)
	        	->where('dealer_name',$request->dealer_name)
	        	->where('complaint_email',$request->email)
	        	->where('free_text',$request->free_text)
	        	->whereBetween('created_at', [$fromDate, $toDate])
	        	->first();

	        if ($check_complaint_is_exist) {
	        	$error_response = getErrorResponse(['Beklager, denne klagen eksisterer allerede, prøv igjen senere for ny klage.']);
	        	error_logs('Response Return 1: '.json_encode($error_response));
	        	//return $error_response;

	        	$complaint_number = $check_complaint_is_exist->complaint_number;
	        	$complaint_success = "Reklamasjon mottatt.\nDin reklamasjon er mottatt med.\nreklamasjonsnummer: ".$complaint_number."\n\nRegistrere ny reklamasjon";
	        	$success_response = getSuccessResponse('', $complaint_success);
	        	error_logs('Response Return 2: '.json_encode($success_response));

            	return $success_response;
	        }

	        // if (strtolower($request->free_text) == 'sanket') {
	        // 	$error_response = getErrorResponse(['Beklager, denne klagen eksisterer allerede, prøv igjen senere for ny klage....test']);
	        // 	error_logs('Test Response Return : '.json_encode($error_response));
	        // 	return $error_response;
	        // }

	        $success_response = (new ProductComplaintData())->storeComplaintData($requestData);
	        error_logs('Response Return : '.json_encode($success_response));

	        return $success_response;
		} catch (\Exception $ex) {
			error_logs('In store complaint Exception : '.$ex->getMessage());
			return getErrorResponse([$ex->getMessage()]);
		}
	}

	/**
	* This method is used to update the complaint data.
	* @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
	*/
	public function editComplaintData(Request $request)
	{
		try {

			$rules = [
	            'size' => 'required|numeric',
	            'model_number' => 'required|alpha_num',
	            'model_name' => 'required|string|regex:/^[^<>]*$/',
	            'color_code' => 'required|alpha_num',
	            'manufacturer_code' => 'required|alpha_num',
	            'gender' => 'required|alpha_num',
	            'version' => 'required|string|regex:/^[^<>]*$/',
	            'complaint_id' => 'required|integer',
	            'claims_reason' => 'required|integer',
	            'free_text' => 'required|regex:/^[^<>]*$/',
	            'customer_reference_number' => 'nullable|regex:/^[^<>]*$/',
	            'user_id' => 'nullable|integer',
	            //'claim_type' => 'required',
	        ];

	        $errors['free_text.required'] = 'The comment from customer field is required.';

	        $validator = Validator::make($request->all(), $rules, $errors);
	        if ($validator->fails()) {
	            return getValidatorFailedResponse($validator);
	        }

	        return (new ProductComplaintData())->updateComplaintData($request->all());
		} catch (Exception $ex) {
			error_logs('In Update complaint Exception : '.$ex->getMessage());
			return getErrorResponse($ex->getMessage());
		}
	}

	/**
	* Get image text using AWS OCR.
	*
	* @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
	*/
	public function getOCRData(Request $request)
	{
		error_logs('**************** In Get AWS OCR Data ***********************');
		try {

			$rules = [
	            'image' => 'required|string',
	        ];

	        $validator = Validator::make($request->all(), $rules);
	        if ($validator->fails()) {
	            $validation_failed = getValidatorFailedResponse($validator);
	        	error_logs($validation_failed);

	        	return $validation_failed;
	        }
			
			//http://hoka.incrustsoftware.com/uploads/ocr/1618984581.png
			//https://support.abbyy.com/hc/en-us/articles/360017326479-processImage-Method

	        $base64image = $request->image;
	        $path = base_path('uploads/ocr/');
	        $filename = $path.strtotime("now").'.png';
			error_logs($filename);
			$image_base64_decode = base64_decode($base64image);
			file_put_contents($filename, $image_base64_decode);

			if (!file_exists($filename)) {
				return getErrorResponse('Please provide valid base64 image data.');
			}

			$aws = \Config::get("aws.ocr");
			
			$client = new TextractClient([
				'region' => 'ap-south-1',
				'version' => '2018-06-27',
				'http' => [
					'verify' => base_path().'/cacert.pem'
				],
				'credentials' => [
					'key'    => $aws['key'],
					'secret' => $aws['secret']
				]
			]);


			// $file = fopen($filename, "rb");
			// $contents = fread($file, filesize($filename));
			// fclose($file);
			
			$options = [
			    'Document' => [
					'Bytes' => $image_base64_decode
			    ],
			    'FeatureTypes' => ['FORMS'], // REQUIRED
			];
			$result = $client->analyzeDocument($options);

			$blocks = $result['Blocks'];
			$lines = [];
			//$words = [];
			$confidence = 0;
			foreach ($blocks as $key => $value) {
				if (isset($value['BlockType']) && $value['BlockType']) {
					$blockType = $value['BlockType'];
					if (isset($value['Text']) && $value['Text']) {
						$text = $value['Text'];
						$conf = $value['Confidence'];
						if ($blockType == 'WORD') {
							//array_push($words, $text);
						} else if ($blockType == 'LINE') {
							array_push($lines, $text);
							$confidence = $confidence + $conf;
						}
					}
				}
			}

			if ($confidence > 0 && count($lines) > 0 && is_numeric($confidence)) {
				$accuracy = $confidence/count($lines);
			} else {
				$accuracy = null;//$confidence/count($lines);
			}

			$textdata['Accuracy'] = $accuracy;
			$textdata['Line'] = $lines;
			//$textdata['Word'] = $words;

			//unlink($filename);
			
			error_logs($textdata);
			
			//error_logs('***********************************************************');

	        return getSuccessResponse($textdata, 'Image data read successfully.');
		} catch (Exception $ex) {
			//error_logs($request->all());
			error_logs('In OCR Exception : '.$ex->getMessage());
			return response([
				'Status' => false,
				'Message' => $ex->getMessage(),
				'Code' => 400
			], 200);
			//return getErrorResponse($ex->getMessage());
		}
	}


	/**
	* Get image text using AWS OCR.
	*
	* @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
	*/
	public function getOCRData1(Request $request)
	{
		error_logs('**************** In Get Abbyy OCR Data ***********************');
		try {

			$rules = [
	            'image' => 'required|string',
	        ];

	        $validator = Validator::make($request->all(), $rules);
	        if ($validator->fails()) {
	            $validation_failed = getValidatorFailedResponse($validator);
	        	error_logs($validation_failed);

	        	return $validation_failed;
	        }
			

	        $base64image = $request->image;
	        $path = base_path('uploads/ocr');
	        $_fileName = strtotime("now").'.png';
	        $filename = $path.'/'.$_fileName;
			error_logs($filename);
			$image_base64 = base64_decode($base64image);
			file_put_contents($filename, $image_base64);

			if (!file_exists($filename)) {
				return getErrorResponse('Please provide valid base64 image data.');
			}
			
			$response = $this->abbyyOCRScanner($path, $_fileName);
			error_logs('Abbyy OCR Scanner Response : ');
			error_logs($response);
			if ($response['Status'] == false) {
				return getErrorResponse($response['Message']);
			}

			$data = explode(PHP_EOL, trim($response['Data']));
			$json = json_encode($data);
			$json = str_replace(array('\ufeff', '\r'), '', $json);
			$data = json_decode($json, true);

			$textdata['Line'] = $data;

			error_logs($textdata);

			$final_res = getSuccessResponse($textdata, 'Image data read successfully.');

	        return $final_res;
		} catch (Exception $ex) {
			//error_logs($request->all());
			error_logs('In OCR Exception : '.$ex->getMessage());
			return response([
				'Status' => false,
				'Message' => $ex->getMessage(),
				'Code' => 400
			], 200);
			//return getErrorResponse($ex->getMessage());
		}
	}

	public function abbyyOCRScanner($directory, $fileName) {

		$ocrRes['Status'] = false;

		$applicationId = '1b787ccf-ac8e-4d47-ac8c-5e188825c383';
		// Password should be sent to your e-mail after application was created
		$password = 'MUvl40IU6gRWKqZ99MRZln8l';
		
		// URL of the processing service. Change to http://cloud-westus.ocrsdk.com
		// if you created your application in US location
		$serviceUrl = 'http://cloud-eu.ocrsdk.com';

		// Get path to file that we are going to recognize
		$local_directory =$directory;
		$filePath = $local_directory.'/'.$fileName;
		if(!file_exists($filePath))
		{
			$ocrRes['Status'] = false;
			$ocrRes['Message'] = 'Filepath not found.';

			return $ocrRes;
		    //die('File '.$filePath.' not found.');
		}

		if(!is_readable($filePath) )
		{
			$ocrRes['Status'] = false;
			$ocrRes['Message'] = 'Please provide read permission to filepath.';

			return $ocrRes;
		    //die('Access to file '.$filePath.' denied.');
		}

		$url = $serviceUrl.'/processImage?language=english&exportFormat=txt';
		  
		// Send HTTP POST request and ret xml response
		$curlHandle = curl_init();
		curl_setopt($curlHandle, CURLOPT_URL, $url);
		curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curlHandle, CURLOPT_USERPWD, "$applicationId:$password");
		curl_setopt($curlHandle, CURLOPT_POST, 1);
		curl_setopt($curlHandle, CURLOPT_USERAGENT, "PHP Cloud OCR SDK Sample");
		curl_setopt($curlHandle, CURLOPT_FAILONERROR, true);
		$post_array = array();
		if((version_compare(PHP_VERSION, '5.5') >= 0)) {
			$file = new \CURLFile($filePath);
		  $post_array["my_file"] = $file;
		} else {
		  $post_array["my_file"] = "@".$filePath;
		}
		curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $post_array); 
		$response = curl_exec($curlHandle);
		if($response == FALSE) {
		  $errorText = curl_error($curlHandle);
		  curl_close($curlHandle);
		  $ocrRes['Status'] = false;
		  $ocrRes['Message'] = $errorText;

		  return $ocrRes;
		  //die($errorText);
		}
		$httpCode = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
		curl_close($curlHandle);

		// Parse xml response
		$xml = simplexml_load_string($response);
		if($httpCode != 200) {
		    if(property_exists($xml, "message")) {
		       die($xml->message);
		    }

		    $ocrRes['Status'] = false;
		  	$ocrRes['Message'] = $response;

		  	return $ocrRes;
		    //die("unexpected response ".$response);
		}

		$arr = $xml->task[0]->attributes();
		$taskStatus = $arr["status"];
		if($taskStatus != "Queued") {
			$ocrRes['Status'] = false;
		  	$ocrRes['Message'] = 'Unexpected task status '.$taskStatus;

		  	return $ocrRes;
		  	//die("Unexpected task status ".$taskStatus);
		}
		
		// Task id
		$taskid = $arr["id"];  
		
		// 4. Get task information in a loop until task processing finishes
		// 5. If response contains "Completed" staus - extract url with result
		// 6. Download recognition result (text) and display it

		$url = $serviceUrl.'/getTaskStatus';
		// Note: a logical error in more complex surrounding code can cause
		// a situation where the code below tries to prepare for getTaskStatus request
		// while not having a valid task id. Such request would fail anyway.
		// It's highly recommended that you have an explicit task id validity check
		// right before preparing a getTaskStatus request.
		if(empty($taskid) || (strpos($taskid, "00000000-0") !== false)) {

			$ocrRes['Status'] = false;
		  	$ocrRes['Message'] = 'Invalid task id used when preparing getTaskStatus request';

		  	return $ocrRes;
		  	//die("Invalid task id used when preparing getTaskStatus request");
		}
		$qry_str = "?taskid=$taskid";

		// Check task status in a loop until it is finished
		// Note: it's recommended that your application waits
		// at least 2 seconds before making the first getTaskStatus request
		// and also between such requests for the same task.
		// Making requests more often will not improve your application performance.
		// Note: if your application queues several files and waits for them
		// it's recommended that you use listFinishedTasks instead (which is described
		// at https://ocrsdk.com/documentation/apireference/listFinishedTasks/).
		while(true)
		{
		    sleep(5);
		    $curlHandle = curl_init();
		    curl_setopt($curlHandle, CURLOPT_URL, $url.$qry_str);
		    curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($curlHandle, CURLOPT_USERPWD, "$applicationId:$password");
		    curl_setopt($curlHandle, CURLOPT_USERAGENT, "PHP Cloud OCR SDK Sample");
		    curl_setopt($curlHandle, CURLOPT_FAILONERROR, true);
		    $response = curl_exec($curlHandle);
		    $httpCode = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
		    curl_close($curlHandle);
		  
		    // parse xml
		    $xml = simplexml_load_string($response);
		    if($httpCode != 200) {
		    	$ocrRes['Status'] = false;
		  		$ocrRes['Message'] = "Unexpected response ".$response;

		  		return $ocrRes;
		    }

		    $arr = $xml->task[0]->attributes();
		    $taskStatus = $arr["status"];
		    if($taskStatus == "Queued" || $taskStatus == "InProgress") {
		      // continue waiting
		      continue;
		    }

		    if($taskStatus == "Completed") {
		      // exit this loop and proceed to handling the result
		      break;
		    }

		    if($taskStatus == "ProcessingFailed") {
		      	//die("Task processing failed: ".$arr["error"]);
		      	$ocrRes['Status'] = false;
		  		$ocrRes['Message'] = "Task processing failed: ".$arr["error"];

		  		return $ocrRes;
		    }


		    //die("Unexpected task status ".$taskStatus);
		    $ocrRes['Status'] = false;
		  	$ocrRes['Message'] = "Unexpected task status ".$taskStatus;

		  	return $ocrRes;
		}

		// Result is ready. Download it

		$url = $arr["resultUrl"];   
		$curlHandle = curl_init();
		curl_setopt($curlHandle, CURLOPT_URL, $url);
		curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
		// Warning! This is for easier out-of-the box usage of the sample only.
		// The URL to the result has https:// prefix, so SSL is required to
		// download from it. For whatever reason PHP runtime fails to perform
		// a request unless SSL certificate verification is off.
		curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curlHandle);
		curl_close($curlHandle);

		//error_logs($response);
		$ocrRes['Status'] = true;
		$ocrRes['Data'] = $response;
		$ocrRes['Message'] = "successfully";

		//echo $response;
		return $ocrRes;
	}


	/**
	* This method is used to manage unique complaint tokens.
	*
	* @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
	*/
	public function manageTokens(Request $request) {
		$complaints = ProductComplaintData::where('is_active', 1)->get();
		foreach ($complaints as $key => $complaint) {
			$token = Str::random(30).uniqid();
			$complaintData = ProductComplaintData::where('id', $complaint->id)->first();
			if ($complaintData) {
				$complaintData->token = $token;
				$complaintData->update();
			}
		}
			return getSuccessResponse('', 'Token Updated !!!');
	}

	/**
	* This method is used to manage complaint numbers
	*
	* @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
	*/
	public function updateAllComplaintNumber(Request $request) {
		$complaints = ProductComplaintData::where('is_active', 1)->get();
		foreach ($complaints as $key => $complaint) {
			(new ProductComplaintData())->updateComplaintNumber($complaint->id);
		}
		
		return getSuccessResponse('', 'Complaint numbers updated !!!');
	}

	/**
	* This method is used to send the complaint receipt email only testing purpose.
	*
	* @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
	*/
	public function sendComplaintReceiveEmails(Request $request) {
		$complaint_id = $request->complaint_id;
		
		//send accepted notification to user.
	    (new NotificationHelper())->sendComplaintReceivedNotification($complaint_id);
		
		return getSuccessResponse($complaint_id, 'Complaint receive email sent successfully !!!');
	}

	/**
	* This method is used to upload server images to s3
	*
	* @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
	*/
	public function uploadComplaintImagesToS3(Request $request) {
		$complaints = ProductComplaintData::with('images')
			->where('is_active', 1)
			->get();
			//->where('id',827)
			
		foreach ($complaints as $key => $complaint) {
			(new ProductComplaintData())->uploadComplaintImagesToS3Bucket($complaint);
		}
		
		return getSuccessResponse('', 'Complaint images uploaded on s3 bucket !!!');
	}
}
