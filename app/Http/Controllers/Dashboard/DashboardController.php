<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Config;
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Model\ProductComplaintData;
use App\Model\InvoiceData;
use App\Model\ClaimsReason;
use App\Helper\UserHelper;
use App\Helper\NotificationHelper;
use App\Helper\HttpRequestHelper;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
	/**
	* This function is used to load dashboard view page.
	*
	* @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
	*/
    public function index(Request $request, $user_token, $web_token) {

    	// $cookie = $request->header('cookie');
    	// $hoka_session = $request->cookie('hoka_session');
    	// $XSRF_TOKEN = $request->cookie('XSRF-TOKEN');
    	// $requestData = $request->all();
    	// $headers = $request->header();
    	// $session = $request->session();

    	// error_logs('Cookies : '.$cookie);
    	// error_logs('hoka_session : '.$hoka_session);

    	$user_id = base64_decode($user_token);
    	$token = strrev(base64_decode($web_token));


    	$check_token = DB::table('oauth_access_tokens')
	    	->where('id','=',$token)
	    	->where('revoked','=',0)
	    	->where('user_id','=',$user_id)
	    	->where('expires_at','>',date('Y-m-d H:i:s'))
	    	->first();

	    if (!$check_token) {
	    	return redirect()->route('login');
	    }

    	return view('dashboard.dashboard')->with('user_token', $user_token)->with('web_token', $web_token);
    }

    /**
	* This function is used to load complaint view page.
	*
	* @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
	*/
    public function closeComplaintDetail(Request $request, $token) {

    	$complaint = ProductComplaintData::with('images')
	        	->where('token', $token)
	        	->where('is_active', 1)
	        	->where('complaint_email', '!=', null)
	        	->first();

	    if ($complaint) {
	    	$complaint_status = $complaint->complaint_status;
	        $complaint = $complaint->toArray();

	        $status = '<span style="color:#004085;">Accepted</span>';
	        if ($complaint_status == 1) {
	        	$status = '<span style="color:green;">Accepted</span>';
	        } else if ($complaint_status == 0) {
	        	$status = '<span style="color:red;">Not Accepted</span>';
	        }

    		return view('dashboard.complaint', compact('complaint', 'status'));
	    } else {

	    	$message = 'Sorry, Complaint not found.';

	    	return view('dashboard.complaint', compact('message'));
	    }


    }

    /**
	* This function is used to get complaint data
	*
	* @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
	*/
	public function getComplaintDataForSlider(Request $request) {
		try {
			$rules = [
	            'from_date' => 'required|date_format:d-m-Y',
	            'to_date' => 'required|date_format:d-m-Y',
	            'user_id' => 'nullable|integer'
	        ];

	        $validator = Validator::make($request->all(), $rules);
	        if ($validator->fails()) {
	            return getValidatorFailedResponse($validator);
	        }

	        $from_date = Carbon::parse($request->from_date)->toDateString().' 00:00:00';
	        $to_date = Carbon::parse($request->to_date)->toDateString().' 23:59:59';

	        $complaints = ProductComplaintData::with('images')
	        	->where('is_active', 1)
	        	->where('complaint_email', '!=', null)
	        	->where('complaint_status', null)
	        	->whereBetween('created_at', [$from_date, $to_date])
	        	->orderBy('id', 'DESC')
	        	->select('id','gender','model_name','version','model_number','size','color_code','manufacturer_code','product_details', 'dealer_name', 'complaint_email', 'address1', 'city', 'zip_code', 'claim_type', 'purchase_date', 'complaint_number', 'company', 'created_at')
	        	->get();
	        	//->take(1)

			return getSuccessResponse($complaints, ['Complaint data selected successfully.']);
		} catch (Exception $ex) {
			return getErrorResponse([$ex->getMessage()]);
		}
	}

	/**
	* This function is used to get specific complaint data
	*
	* @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
	*/
	public function getComplaintData(Request $request) {
		try {
	        $validator = Validator::make($request->all(), [
	            'complaint_id' => 'required|integer',
	            'user_id' => 'nullable|integer'
	        ]);

	        if ($validator->fails()) {
	            return getValidatorFailedResponse($validator);
	        }

	        $complaint = ProductComplaintData::with('images')
	        	->where('id', $request->complaint_id)
	        	->where('is_active', 1)
	        	->where('complaint_email', '!=', null)
	        	->select('id','gender','model_name','version','model_number','size','color_code','manufacturer_code','product_details', 'dealer_name', 'complaint_email', 'address1', 'city', 'zip_code', 'claim_type', 'purchase_date', 'claims_reason_id', 'free_text', 'complaint_number', 'company', 'created_at', 'customer_reference_number','is_sport')
	        	->first();

	        if (!$complaint) {
	        	return getErrorResponse(['Please provide valid complaint.']);
	        }

	        $dealer_name = strtolower($complaint->dealer_name);
	        if (strpos($dealer_name, 'sport') !== false) {
	        	//$complaint->is_sport = 1;
	        }	        

	       	$created_at = Carbon::createFromFormat('Y-m-d H:i:s', $complaint->created_at)->format('d.m.Y');
	       	$complaint->received_date = $created_at;
	
			return getSuccessResponse($complaint, ['Complaint data selected successfully.']);
		} catch (Exception $ex) {
			return getErrorResponse([$ex->getMessage()]);
		}
	}

		/**
	* This function is used to get specific complaint data
	*
	* @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
	*/
	public function getComplaintInvoiceData(Request $request) {
		try {
	        $validator = Validator::make($request->all(), [
	            'complaint_id' => 'required|integer',
	            'user_id' => 'nullable|integer'
	        ]);

	        if ($validator->fails()) {
	            return getValidatorFailedResponse($validator);
	        }

	        $complaint = ProductComplaintData::with('images')
	        	->where('id', $request->complaint_id)
	        	->where('is_active', 1)
	        	->where('complaint_email', '!=', null)
	        	->first();

	        	//->select('id','product_details', 'dealer_name', 'complaint_email', 'address1', 'city', 'zip_code')
	        if (!$complaint) {
	        	return getErrorResponse(['Please provide valid complaint.']);
	        }

	        $is_sport = 0;//$complaint->is_sport;
	        $dealer_name = strtolower($complaint->dealer_name);
	        if (strpos($dealer_name, 'sport') !== false) {
	        	$is_sport = 1;
	        }

	        $ModelNumber = $complaint->model_number;
			$Size = $complaint->size;
			$ColorCode = $complaint->color_code;
			$customer_id = $complaint->customer_id;

			// $ModelNumber = "1110534";
			// $Size = "11";
			// $ColorCode = "BWHT";
			// $customer_id = 40821;

	        $invoices = (new HttpRequestHelper())->getInvoicesData($ModelNumber, $Size, $ColorCode, $customer_id);
	        if ($invoices['Status'] === false) {

	        	$data['Invoice_API_Error'] = 0;
	        	if ($is_sport == 1) {
	        		$data['Invoice_API_Error'] = $invoices['Invoice_API_Error'];
	        	}

	        	return getErrorResponse($invoices['Message'], $data);
	        }

	        if (is_array($invoices['Data'])) {
				$complaint->invoices = $invoices['Data'];        	
	        }
	
			return getSuccessResponse($complaint, ['Complaint and invoices data selected successfully.']);
		} catch (Exception $ex) {
			return getErrorResponse([$ex->getMessage()]);
		}
	}


	/**
	* This function is used to accept complaint order.
	*
	* @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
	*/
	public function acceptComplaint(Request $request) {

		try {
			$validator = Validator::make($request->all(), [
	            'product_complaint_data_id' => 'required|integer',
	            'customer_id' => 'required|integer',
	            'invoice_no' => 'required|integer',
	            'invoice_date' => 'required|date',
	            'accept_message' => 'required|regex:/^[^<>]*$/',
	            'line_id' => 'required|integer',
	            'order_no' => 'required|integer',
	            'article_no' => 'required|string|regex:/^[^<>]*$/',
	            'quantity' => 'required|integer',
	            'net_price' => 'required|numeric',
	            'discount1' => 'required|numeric',
	            'discount2' => 'required|numeric',
	            'discount3' => 'required|numeric',
	            'credit_note_endpoint' => 'required|string|regex:/^[^<>]*$/ ',
	            'credit_note_body' => 'required|string|regex:/^[^<>]*$/',
	            'complaint_status' => 'required|min:0|max:1',
	            //'invoice_api_error' => 'required',
	        ]);

	        if ($validator->fails()) {
	            return getValidatorFailedResponse($validator);
	        }

	        $complaint = ProductComplaintData::where('id', $request->product_complaint_data_id)->first();
	        if (!$complaint) {
	        	return getErrorResponse(['Please provide valid complaint.']);
	        }

	        if (!is_numeric($complaint->claims_reason_id)) {
	        	return getErrorResponse(['Sorry, Complaint reason number not found.']);
	        }

	    	$CustomerNo = $complaint->customer_id;
	    	$InvoiceNo = $request->invoice_no;
	    	$OrderNo = $request->order_no;
	    	$LineID = $request->line_id;
	        $ComplaintReasonNo = $complaint->claims_reason_id;
	        $ClaimID = $complaint->complaint_number;
	        $CustomerReference = $complaint->customer_reference_number;
	        $ProductionID = trim($complaint->manufacturer_code);

	        $invoices = (new HttpRequestHelper())->acceptComplaintRequest($CustomerNo, $InvoiceNo, $OrderNo, $LineID, $ComplaintReasonNo,$ClaimID,$CustomerReference,$ProductionID);
	        if ($invoices['Status'] === false) {
	        	return getErrorResponse($invoices['Message']);
	        }

	        $complaint->complaint_status = $request->complaint_status;
	        $complaint->complaint_message = $request->accept_message;
	        $ceditlineData = $invoices['Data'];
	        if (isset($ceditlineData['CreditOrderNo'])) {
	        	$complaint->credit_order_number = $ceditlineData['CreditOrderNo'];
	        }

	        $complaint->complaint_close_date = Carbon::now()->toDateTimeString();
	        $complaint->update();

	        $data = $request->all();
	        $data['invoice_date'] = Carbon::parse($request->invoice_date)->toDateTimeString();

	        $invoiceData = InvoiceData::where('product_complaint_data_id', $request->product_complaint_data_id)->where('invoice_no', $InvoiceNo)
	        	->where('line_id', $LineID)
	        	->where('order_no', $OrderNo)
	        	->first();

	        if ($invoiceData) {
	        	$invoiceData->update($data);
	        } else {
	        	InvoiceData::create($data);
	        }

	        //send accepted notification to user.
	        (new NotificationHelper())->sendComplaintAcceptedNotification($complaint->id);

	        return getSuccessResponse($ceditlineData, ['Complaint accepted successfully.']);
		} catch (\Exception $ex) {
			return getErrorResponse([$ex->getMessage()]);
		}
	}

		/**
	* This function is used to accept complaint order.
	*
	* @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
	*/
	public function acceptComplaintToInvoiceError(Request $request) {

		try {
			$validator = Validator::make($request->all(), [
	            'product_complaint_data_id' => 'required|integer',
	            'accept_message' => 'required|regex:/^[^<>]*$/',
	            'complaint_status' => 'required|integer|min:0|max:1',
	            //'invoice_api_error' => 'required',
	        ]);

	        if ($validator->fails()) {
	            return getValidatorFailedResponse($validator);
	        }

	        $complaint = ProductComplaintData::where('id', $request->product_complaint_data_id)->first();
	        if (!$complaint) {
	        	return getErrorResponse(['Please provide valid complaint.']);
	        }

	        $complaint->complaint_status = $request->complaint_status;
	        $complaint->complaint_message = $request->accept_message;
	        $complaint->complaint_close_date = Carbon::now()->toDateTimeString();
	        $complaint->update();

	        //send accepted notification to user.
	        (new NotificationHelper())->sendComplaintAcceptedNotification($complaint->id);

	        return getSuccessResponse($complaint, ['Complaint accepted successfully.']);
		} catch (\Exception $ex) {
			return getErrorResponse([$ex->getMessage()]);
		}
	}

	/**
	* This function is used to not accept complaint.
	*
	* @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
	*/
	public function notAcceptComplaint(Request $request) {
		$validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'user_id' => 'nullable|integer',
            'not_accept_message' => 'required|string|regex:/^[^<>]*$/'
        ]);

        if ($validator->fails()) {
            return getValidatorFailedResponse($validator);
        }

        $complaint = ProductComplaintData::where('id', $request->id)->first();
        if (!$complaint) {
        	return getErrorResponse(['Please provide valid complaint.']);
        }

        $complaint->complaint_status = 0;
        $complaint->complaint_message = $request->not_accept_message;
        $complaint->complaint_close_date = Carbon::now()->toDateTimeString();
        $complaint->update();

        //send not accepted notification to user.
	    (new NotificationHelper())->sendComplaintNotAcceptedNotification($complaint->id);

        return getSuccessResponse('', ['Complaint status updated successfully.']);
	}

	/**
	* This function is used to get data for line chart
	*
	* @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
	*/
	public function getDashboardChart(Request $request) {
		$validator = Validator::make($request->all(), [
            'user_id' => 'required|integer',
            'from_date' => 'nullable|date_format:d-m-Y',
            'to_date' => 'nullable|date_format:d-m-Y',
        ]);

        if ($validator->fails()) {
            return getValidatorFailedResponse($validator);
        }

        $unique_models = ProductComplaintData::whereRaw('product_complaint_data.id IN (select MAX(product_complaint_data.id) from product_complaint_data GROUP BY product_complaint_data.model_name)')
        	->select('model_name','model_number')
        	->get();

        if (isset($request->from_date)) {
        	$from_date = Carbon::parse($request->from_date)->toDateTimeString();
        	$to_date = Carbon::parse($request->to_date)->endOfMonth()->toDateTimeString();
        	$diff_in_months = Carbon::parse($to_date)->diffInMonths(Carbon::parse($from_date));
        	$months = $diff_in_months + 1;

        } else {
        	$from_date = Carbon::now()->startOfMonth()->subMonths(12)->toDateTimeString();
        	$to_date = Carbon::now()->endOfMonth()->toDateTimeString();
        	$months = 12;
        }

        $ct = 0;
        $dates = [];
       	while($months > $ct) {
       		$fromDate = Carbon::parse($to_date)->startOfMonth()->subMonth($ct)->toDateTimeString();
       		array_push($dates, $fromDate);
       		$ct++;
       	}
        
        $lables = [];
        $complaint = [];

        foreach ($unique_models as $key => $model) {

        	$model_data = [];
    		foreach($dates as $index => $date) {
    			if ($key == 0) {
		        	$month = Carbon::parse($date)->startOfMonth()->format('M');
		        	$year = Carbon::parse($date)->format('Y');
		        	$label = $month.' '.$year;
		        	array_push($lables, $label);
            	}

        		$fromDate = Carbon::parse($date)->startOfMonth();
            	$toDate = Carbon::parse($date)->endOfMonth();
        		$complaint_count = ProductComplaintData::whereBetween('created_at', [$fromDate, $toDate])
        			->where('model_name', $model->model_name)
        			->count();

    			array_push($model_data, $complaint_count);
    		}

        	$complaint[$key]['value'] = $model_data;
        	$complaint[$key]['legend'] = substr($model->model_name, 0, 20);//.' ('.$model->model_number.')';
    	}


    	$response['datasets'] = $complaint;
        $response['lables'] = $lables;
        
        return getSuccessResponse($response, ['Data selected for line chart.']);
	}

	/**
     * This function is used to get close cases report.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
    */
    public function getCloseCases(Request $request) {

    	$validator = Validator::make($request->all(), [
            'user_id' => 'required|integer',
            'from_date' => 'required|date_format:d-m-Y',
            'to_date' => 'required|date_format:d-m-Y',
        ]);

        if ($validator->fails()) {
            return getValidatorFailedResponse($validator);
        }

        $sensor_id = $request->user_id;
        $from_date = Carbon::parse($request->from_date)->toDateString(). ' 00:00:00';
        $to_date =  Carbon::parse($request->to_date)->toDateString(). ' 23:59:59';

        $columns = array( 
            0 => 'complaint_number', 
            1 => 'customer_id', 
            2 => 'dealer_name',
            3 => 'dealer_name', //link
            4 => 'model_number',
            5 => 'manufacturer_code',
            6 => 'complaint_message',
            7 => 'manufacturer_code' //status
        );

        $complaint_data = ProductComplaintData::whereBetween('complaint_close_date', [$from_date, $to_date])
            ->whereIN('complaint_status', [1,0]) //1 = Accepted, 0 = Not Accepted
            ->where('is_active', 1);

        $totalData = $complaint_data->count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_number = $request->input('order.0.column');
        $dir = $request->input('order.0.dir');

        if (!is_numeric($order_number) || !is_numeric($start) || !is_numeric($limit)) {
		    return getErrorResponse(['Please provide valid input parameters.']);
		}

		if ($order_number < 0 || $order_number > 7) {
			return getErrorResponse(['Please provide valid order.']);
		}

		if ($dir != 'ASC' && $dir != 'asc' && $dir != 'DESC' && $dir != 'desc') {
			return getErrorResponse(['Please provide valid dir input parameters.']);
		}

		$order = $columns[$order_number];
            
        if(empty($request->input('search.value')))
        {   
            $close_complaints = $complaint_data->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
            
        } else {
            $search = $request->input('search.value');

            $close_complaint_data = $complaint_data->where(function ($query) use ($search) {
                $query->orWhere('customer_id', 'LIKE', "%{$search}%")
                ->orWhere('dealer_name', 'LIKE', "%{$search}%")
                ->orWhere('model_number', 'LIKE', "%{$search}%")
                ->orWhere('manufacturer_code', 'LIKE', "%{$search}%")
                ->orWhere('complaint_number', 'LIKE', "%{$search}%")
                ->orWhere('complaint_message', 'LIKE', "%{$search}%");
            });

            $totalFiltered = $close_complaint_data->count();

            $close_complaints = $close_complaint_data->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }

        $data = array();
        if(!empty($close_complaints))
        {
        	$app_url = url('/complaint');
            foreach ($close_complaints as $close_complaint)
            {
                $nestedData['complaint_number'] = $close_complaint->complaint_number;
                $nestedData['customer_id'] = $close_complaint->customer_id;
                $nestedData['dealer_name'] = $close_complaint->dealer_name;
                $complaint_url = $app_url.'/'.$close_complaint->token;
                $nestedData['complaint_link'] = '<a href="'.$complaint_url.'" target="__blank">'.$complaint_url.'</a>';
                $nestedData['model_number'] = $close_complaint->model_number;
                $nestedData['manufacturer_code'] = $close_complaint->manufacturer_code;
                $nestedData['complaint_message'] = $close_complaint->complaint_message;

                if ($close_complaint->complaint_status == 1) {
                	$nestedData['status'] = '<i class="fa fa-check fa_right_check" aria-hidden="true"></i>';
                } else {
                	$nestedData['status'] = '<i class="fa fa-times fa_cross_check" aria-hidden="true"></i>';
                }

                $data[] = $nestedData;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
            
        echo json_encode($json_data); 
    }

    /**
     * Get close cases data for export
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
    */
    public function getCloseCasesReport(Request $request) {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|integer',
            'from_date' => 'required|date_format:d-m-Y',
            'to_date' => 'required|date_format:d-m-Y',
            'order' => 'required|integer|min:0|max:7',
            'dir' => 'required|in:desc,DESC,asc,ASC'
        ]);

        if ($validator->fails()) {
            return getValidatorFailedResponse($validator);
        }

        $user_id = $request->user_id;
        $from_date = Carbon::parse($request->from_date)->toDateString().' 00:00:00';
        $to_date =  Carbon::parse($request->to_date)->toDateString(). ' 23:59:59';

        $columns = array( 
            0 => 'complaint_number', 
            1 => 'customer_id', 
            2 => 'dealer_name',
            3 => 'dealer_name', //link
            4 => 'model_number',
            5 => 'manufacturer_code',
            6 => 'complaint_message',
            7 => 'manufacturer_code' //status
        );

        $complaint_data = ProductComplaintData::whereBetween('complaint_close_date', [$from_date, $to_date])
            ->whereIN('complaint_status', [1,0]) //1 = Accepted, 0 = Not Accepted
            ->where('is_active', 1);

        $order = $columns[$request->order];
        $dir = $request->dir;

        if(empty($request->input('search')))
        {   
            $close_complaints = $complaint_data->orderBy($order,$dir)->get();
            
        } else {
            $search = $request->input('search');

            $close_complaints = $complaint_data->where(function ($query) use ($search) {
                $query->orWhere('customer_id', 'LIKE', "%{$search}%")
                ->orWhere('dealer_name', 'LIKE', "%{$search}%")
                ->orWhere('model_number', 'LIKE', "%{$search}%")
                ->orWhere('manufacturer_code', 'LIKE', "%{$search}%")
                ->orWhere('complaint_number', 'LIKE', "%{$search}%")
                ->orWhere('complaint_message', 'LIKE', "%{$search}%");
            })->orderBy($order,$dir)->get();
        }

        $data = [];
        if(!empty($close_complaints))
        {
        	$headerarray = array();
            $idh['text'] = "Complaint ID";
            $idh['style'] = "tableHeader";
            array_push($headerarray, $idh);

            $idh['text'] = "Customer ID";
            $idh['style'] = "tableHeader";
            array_push($headerarray, $idh);

            $delearh['text'] = "Forhandler";
            $delearh['style'] = "tableHeader";
            array_push($headerarray, $delearh);

            $linkh['text'] = "Link til reklamasjonen";
            $linkh['style'] = "tableHeader";
            array_push($headerarray, $linkh);

            $modellh['text'] = "Modell";
            $modellh['style'] = "tableHeader";
            array_push($headerarray, $modellh);

            $produksjonh['text'] = "Produksjon";
            $produksjonh['style'] = "tableHeader";
            array_push($headerarray, $produksjonh);

            $reasonh['text'] = "Reason";
            $reasonh['style'] = "tableHeader";
            array_push($headerarray, $reasonh);

            $statush['text'] = "Status";
            $statush['style'] = "tableHeader";
            array_push($headerarray, $statush);

            $data[] = $headerarray;
            $app_url = url('/complaint');
            foreach ($close_complaints as $close_complaint)
            {
                $complaint_url = $app_url.'/'.$close_complaint->token;
                if ($close_complaint->complaint_status == 1) {
                	$complaint_status = 'Accepted';
                } else {
                	$complaint_status = 'Not Accepted';
                }

                $varsubarray = array();

                $cidv['text'] = $close_complaint->complaint_number;
                array_push($varsubarray, $cidv);

                $idv['text'] = $close_complaint->customer_id;
                array_push($varsubarray, $idv);

                $dealer_namev['text'] = $close_complaint->dealer_name;
                array_push($varsubarray, $dealer_namev);

                $linkv['text'] = $complaint_url;
                array_push($varsubarray, $linkv);

                $model_numberv['text'] = $close_complaint->model_number;
                array_push($varsubarray, $model_numberv);

                $manufacturer_codev['text'] = $close_complaint->manufacturer_code;
                array_push($varsubarray, $manufacturer_codev);

                $complaint_messagev['text'] = $close_complaint->complaint_message;
                array_push($varsubarray, $complaint_messagev);

                $complaint_statusv['text'] = $complaint_status;
                array_push($varsubarray, $complaint_statusv);
                
                $data[] = $varsubarray;
            }
        }

        $dates['from_date'] = Carbon::parse($from_date)->toDateString();
        $dates['to_date'] = Carbon::parse($to_date)->toDateString();
        $dates['current_date'] = Carbon::now()->toDateTimeString();

        $response['export'] = $data;
        $response['dates'] = $dates;
            
        return getSuccessResponse($response, ['Data selected for close cases export.']);
    }

    /**
     * Get specific and all cliam reasons.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
    */
    public function getClaimsReason(Request $request) {

    	$validator = Validator::make($request->all(), [
            'claims_reason_id' => 'nullable|integer',
        ]);

        if ($validator->fails()) {
            return getValidatorFailedResponse($validator);
        }

        $claims_reasons = ClaimsReason::where('is_active', 1);
    	if (isset($request->claims_reason_id)) {
    		$claims_reasons = $claims_reasons->where('id', $request->claims_reason_id)
    			->select('id','name')
    			->first();
    	} else {
    		$claims_reasons = $claims_reasons->select('id','name')->get();
    	}
    	
    	return getSuccessResponse($claims_reasons, ['Claim reasons selected successfully.']);
    }
}
