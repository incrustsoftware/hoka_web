<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $table = 'users';
    protected $success;
    protected $error;

    /**
     * Instantiate a user model instances.
    */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->success = getSuccessMessages();
        $this->error = getErrorMessages();
    }

    // Override the model update method.
    public function update(array $attributes = [], array $options = []) {
        return parent::update($attributes, $options);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'mobile_number',
        'role_id',
        'username',
        'email',
        'password',
        'profile_picture',
        'created_by',
        'updated_by',
        'is_active',
        'is_delete',
        'remember_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

        /**
     * The attributes used for validation.
     *
     * @var array
     */
    public $u_firstname = ['firstname' => 'required|max:50'];
    public $u_lastname = ['lastname' => 'required|max:50'];
    public $u_name = ['name' => 'required|max:50'];
    public $u_username = ['username' => 'required|max:100'];
    public $u_email = ['email' =>
        'required|email|max:100|unique:users'];
    public $user_email = ['email' => 'required|email|max:100'];
    public $u_phonenumber = ['phonenumber' => 'required'];
    public $u_roleid = ['role_id' => 'required|numeric'];
    public $u_isactive = ['isactive' => 'required|numeric'];
    public $u_password = ['password' => 'required|min:6'];
    public $u_loginuserid = ['loginuserid' => 'required|numeric'];
    public $u_userid = ['userid' => 'required|numeric'];

    /**
     * Validate request data for user login.
     * @param  $data
     * @return $validator
     */
    public function validateUserLoginData($data)
    {
        $rules = array_merge(
            $this->user_email,
            $this->u_password
        );
        $validator = Validator::make($data, $rules);

        return $validator;
    }

    /**
     * Validate request data for email.
     * @param  $data
     * @return $validator
     */
    public function validateUserEmail($data)
    {
        $validator = Validator::make($data, $this->user_email);

        return $validator;
    }

    /**
     * Validate user data for update
     * @param  $data,$flag
     * @return $validator
     */
    public function validateUserRequestData($data)
    {
        $rules = array_merge(
            $this->u_firstname,
            $this->u_lastname,
            $this->u_phonenumber,
            $this->u_roleid,
            $this->u_email
        );
        $validator = Validator::make($data, $rules);

        return $validator;
    }

    /**
     * This method is used for user statistics;
     * @param $user, $status
     * @return null
     */
    // public function userStatistics(
    //     $user,
    //     $status
    // ) {
    //     $data['userid'] = $user->id;
    //     $data['datetime'] = Carbon::now();
    //     $data['islogedin'] = 1;

    //     if ($status) {
    //         $data['loginattempt'] = 0;
    //         $statistics = UserLoginStatastics::create($data);
    //     } else {
    //         $statistics = UserLoginStatastics::where('userid', $user->id)
    //             ->orderBy('id', 'DESC')->first();
    //         if ($statistics) {
    //             $attempt = $statistics->loginattempt;
    //             if ($attempt == 3) {
    //                 $user->isactive = 0;
    //                 $user->update();
    //             } else {
    //                 $attempt++;
    //                 $statistics->loginattempt = $attempt;
    //                 $statistics->update();
    //             }
    //         } else {
    //             $data['loginattempt'] = 1;
    //             $statistics = UserLoginStatastics::create($data);
    //         }
    //     }
    // }

    /**
     * This method is used for manage user data.
     * @param  $request
     * @return $response
     */
    public function manageUserLogin($request)
    {
        $user = Auth::user();
        $token = $user->createToken('user_token');
        //$this->manageBrowserNotification($user);
        $data['user'] = $user;
        $data['token'] = $token;
        $data['web_token'] = base64_encode(strrev($token->token->id));
        $data['user_token'] = base64_encode($user->id);
    
        $response = getSuccessResponse(
            $data,
            [$this->success['login_success']]
        );
        
        return $response;
    }

    /**
     * This method is used for manage browser notification.
     * @param  $user
     * @return null
     */
    public function manageBrowserNotification($user)
    {
        $ip_address = Agent::ip();
        $userAgent = Agent::userAgent();
        $browser_name = Agent::browser()->name;

        if ($user->browser_ip != $ip_address || $user->browser_name != $userAgent) {
            $message = "Your APS-SureVia account is just logged in on new " . $browser_name . " browser and it's IP address is - " . $ip_address;
            (new NotificationHelper())->sendBrowserNotification($user, $message);
            $user->browser_ip = $ip_address;
            $user->browser_name = $userAgent;
            $user->update();
        }
    }
}
