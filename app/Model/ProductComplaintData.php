<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Exception;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use App\Helper\NotificationHelper;

class ProductComplaintData extends Model
{
    protected $table = "product_complaint_data";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
    */
    protected $fillable = [
        'complaint_number',
        'customer_id',
        'product_details',
        'gender',
        'model_name',
        'version',
        'model_number',
        'size',
        'color_code',
        'manufacturer_code',
        'dealer_name',
        'company',
        'complaint_email',
        'article_no',
        'phone',
        'city',
        'address1',
        'address2',
        'zip_code',
        'claim_type',
        'purchase_date',
        'created_by',
        'updated_by',
        'complaint_status',
        'complaint_close_date',
        'credit_order_number',
        'complaint_message',
        'is_active',
        'is_delete',
        'token',
        'claims_reason_id',
        'free_text',
        'customer_reference_number',
        'is_sport',
        'device_type'
    ];

    /**
     * Get the complaint images
     */
    public function images()
    {
        return $this->hasMany('App\Model\ProductComplaintImages', 'product_complaint_data_id')
            ->where('is_active', 1);
    }

    /**
     * Get the complaint invoices
     */
    public function invoices()
    {
        return $this->hasMany('App\Model\InvoiceData', 'product_complaint_data_id')
            ->where('is_active', 1);
    }

    /**
     * Store complaint data
     *
     * @param  $data
     * @return $response
    */
    public function manageComplaintData($data) {
        try {
            $product_details = $data['product_details'];
            $dealer_name = $data['dealer_name'];
            $product_images = $data['product_images'];
            $article_no = $data['article_no'];
            $customer_id = $data['customer_id'];
            $created_by = $customer_id;
            $updated_by = $customer_id;

            $data = array(
                'customer_id' => $customer_id,
                'product_details' => $product_details,
                'dealer_name'=> $dealer_name,
                'article_no'=> $article_no,
                'created_by' => $created_by,
                'updated_by' => $updated_by
            );

            $complaint = ProductComplaintData::create($data);
            if (!$complaint) {
                return getErrorResponse('Something went wrong.');
            }

            $complaint_id = $complaint->id;

            $this->updateComplaintNumber($complaint_id);

            if (!empty($product_images)) 
            {
                foreach ($product_images as $product_image_str) 
                {
                    $filePath = '';
                    if ($product_image_str != '') 
                    {
                        $image = base64_decode($product_image_str);
                        $image_name = md5(uniqid(rand(), true));
                        // image name generating with random number with 32 characters
                        $filename = $image_name . '.' . 'png';
                        $path = base_path('uploads/product_complaint_images/');
                        //image uploading folder path
                        file_put_contents($path . $filename, $image);
                        // image is bind and upload to respective folder
                        $filePath = 'uploads/product_complaint_images/'.$filename;

                        $data_img = array(
                            'product_complaint_data_id' => $complaint_id,
                            'product_image' => $filePath,
                            'created_by' => $created_by,
                            'updated_by' => $updated_by
                        );

                        ProductComplaintImages::create($data_img);
                    }
                }
            }

            return getSuccessResponse('', 'Complaint registered successfully.');

        } catch (Exception $ex) {
            return getErrorResponse($ex->getMessage());
        }
    }

     /**
     * Store complaint data
     *
     * @param  $data
     * @return $response
    */
    public function storeComplaintData($data) {
        try {

            //error_logs('storeComplaintData : ');

            $customer_id = $data['customer_id'];
            $product_details = $data['product_details'];
            $dealer_name = $data['dealer_name'];
            $product_images = $data['product_images'];
            $article_no = $data['article_no'];
            $address1 = $data['address1'];
            $address2 = $data['address2'];
            $city = $data['city'];
            $company = $data['company'];
            $phone = $data['phone'];
            $zip_code = $data['zip_code'];
            
            $purchase_date = null;
            if (isset($data['purchase_date'])) {
                $purchase_date = $this->convertPurchaseDate($data['purchase_date']);
            }

            $complaint_email = $data['email'];
            $created_by = $customer_id;
            $updated_by = $customer_id;

            $model_number = $data['model_number'];
            $model_name = $data['model_name'];
            $size = $data['size'];
            $color_code = $data['color_code'];
            $manufacturer_code = $data['manufacturer_code'];
            $gender = $data['gender'];
            $version = $data['version'];
            $token = Str::random(30).uniqid();
            $manufacturer_code = $this->RemoveSpecialChar($manufacturer_code);

            $claim_type = null;
            if (isset($data['claim_type'])) {
                $claim_type = $data['claim_type'];
            }

            $free_text = null;
            if (isset($data['free_text'])) {
                $free_text = $data['free_text'];
            }

            $customer_reference_number = null;
            if (isset($data['customer_reference_number'])) {
                $customer_reference_number = $data['customer_reference_number'];
            }

            $is_sport = 0;
            if (isset($data['is_sport'])) {
                $is_sport = $data['is_sport'];
            }

            $device_type = null;
            if (isset($data['device_type'])) {
                $device_type = $data['device_type'];
            }

            $data = array(
                'customer_id' => $customer_id,
                'product_details' => $product_details,
                'gender' => $gender,
                'model_name' => $model_name,
                'version' => $version,
                'model_number' => $model_number,
                'size' => $size,
                'color_code' => $color_code,
                'manufacturer_code' => $manufacturer_code,
                'dealer_name'=> $dealer_name,
                'complaint_email'=> $complaint_email,
                'zip_code'=> $zip_code,
                'claim_type'=> $claim_type,
                'purchase_date'=> $purchase_date,
                'phone'=> $phone,
                'company'=> $company,
                'city'=> $city,
                'address2'=> $address2,
                'address1'=> $address1,
                'article_no'=> $article_no,
                'created_by' => $created_by,
                'updated_by' => $updated_by,
                'token' => $token,
                'free_text' => $free_text,
                'customer_reference_number' => $customer_reference_number,
                'is_sport' => $is_sport,
                'device_type' => $device_type,
            );

            $complaint = ProductComplaintData::create($data);
            if (!$complaint) {
                return getErrorResponse(['Something went wrong.']);
            }

            $complaint_id = $complaint->id;

            $complaint_number = $this->updateComplaintNumber($complaint_id);

            error_logs('Stored complaint_id : '.$complaint_id);
            error_logs('Stored complaint_number : '.$complaint_number);

            if (!empty($product_images)) 
            {
                foreach ($product_images as $product_image_str) 
                {
                    $filePath = '';
                    if ($product_image_str != '') 
                    {
                        $image = base64_decode($product_image_str);
                        $image_name = md5(uniqid(rand(), true));
                        // image name generating with random number with 32 characters

                        /*$filename = $image_name . '.' . 'png';
                        $path = base_path('uploads/product_complaint_images/');
                        file_put_contents($path . $filename, $image);
                        $filePath = 'uploads/product_complaint_images/'.$filename;*/

                        $filename = 'images/'.$image_name . '.' . 'png';
                        $put = \Storage::disk('s3')->put($filename, $image, 'public');
                        $filePath = \Storage::disk('s3')->url($filename);

                        $data_img = array(
                            'product_complaint_data_id' => $complaint_id,
                            's3_product_image' => $filePath,
                            'created_by' => $created_by,
                            'updated_by' => $updated_by,
                        );

                        ProductComplaintImages::create($data_img);
                    }
                }
            }


            (new NotificationHelper())->sendComplaintReceivedNotification($complaint_id);
            
            //$complaint_success = nl2br("Reklamasjon mottatt.\nDin reklamasjon er mottatt med.\nreklamasjonsnummer: ".$complaint_number." \nRegistrere ny reklamasjon");
            
            //'Complaint registered successfully.'
            
            $complaint_success = "Reklamasjon mottatt.\nDin reklamasjon er mottatt med.\nreklamasjonsnummer: ".$complaint_number."\n\nRegistrere ny reklamasjon";

            return getSuccessResponse('', $complaint_success);

        } catch (Exception $ex) {
            return getErrorResponse([$ex->getMessage()]);
        }
    }

    /**
     * Remove special char from string.
     *
     * @param  $str
     * @return $res
    */
    public function RemoveSpecialChar($str){

        $res = preg_replace("/[^0-9a-zA-Z]/",' ',$str);
        $res = preg_replace('/\s+/', '', $res);
        
        // Returning the result
        return $res;
    }

    /**
     * Convert_purchase_date
     *
     * @param  $purchase_date
     * @return $formated_purchase_date
    */
    public function convertPurchaseDate($purchase_date) {
        $formated_purchase_date = Carbon::createFromFormat('d.m.Y', $purchase_date)->toDateString();

        return $formated_purchase_date;
    }

    /**
     * Update complaint data.
     *
     * @param  $data
     * @return $response
    */
    public function updateComplaintData($data) {
        try {

            $data['claims_reason_id'] = $data['claims_reason'];

            $complaint = ProductComplaintData::where('id', $data['complaint_id'])
                ->where('is_active', 1)
                ->first();

            if (!$complaint) {
                return getErrorResponse(['Please provide valid complaint']);
            }

            $complaint->update($data);

            $complaint = ProductComplaintData::with('images')
                ->where('id', $data['complaint_id'])
                ->where('is_active', 1)
                ->where('complaint_email', '!=', null)
                ->select('id','gender','model_name','version','model_number','size','color_code','manufacturer_code','product_details', 'dealer_name', 'complaint_email', 'address1', 'city', 'zip_code','claim_type', 'purchase_date', 'claims_reason_id', 'free_text', 'complaint_number', 'company', 'created_at', 'customer_reference_number','is_sport')
                ->first();

            return getSuccessResponse($complaint, 'Complaint updated successfully.');
        } catch (Exception $ex) {
            return getErrorResponse($ex->getMessage());
        }
    }

    /**
     * Get complaint number with leading year and zeros.
     *
     * @param  $number
     * @return $complaint_number
    */
    public function updateComplaintNumber($number) {

        $complaint_id = $number;

        $year = Carbon::now()->format('y');
        if (strlen($number) < 5) {
            $number = str_pad($number, 5, '0', STR_PAD_LEFT);
        }

        $complaint_number = $year.$number;

        $complaint = ProductComplaintData::where('id', $complaint_id)->first();
        if ($complaint) {
            $complaint->complaint_number = $complaint_number;
            $complaint->update();

            return $complaint_number;
        }

        return false;
    }

    /**
     * Get complaint number with leading year and zeros.
     *
     * @param  $number
     * @return $complaint_number
    */
    public function updateComplaintNumber_new($number) {
        $complaint_id = $number;
        $current_year = Carbon::now()->format('y');
        $complaint_number = 0;

        if ($number == 1) {
            if (strlen($number) < 5) {
                $number = str_pad($number, 5, '0', STR_PAD_LEFT);
            }

            $complaint_number = $current_year.$number;

            $complaint = ProductComplaintData::where('id', $complaint_id)->first();
            if ($complaint) {
                $complaint->complaint_number = $complaint_number;
                $complaint->update();
                
                return $complaint_number;
            }

        } else {
            $complaint = ProductComplaintData::where('id', $complaint_id)->first();
            if ($complaint) {
                $existing_id = $complaint_id-1;
                $existing_complaint = ProductComplaintData::where('id', $existing_id)->first();
                if ($existing_complaint) {
                    $existing_complaint_number = $existing_complaint->complaint_number;
                    $old_year = substr($existing_complaint_number,0,2);
                    if ($current_year == $old_year) {
                        $complaint_number = (int)$existing_complaint_number + 1;
                    } else {
                        $number = str_pad(1, 5, '0', STR_PAD_LEFT);
                        $complaint_number = $current_year.$number;
                    }

                    $complaint->complaint_number = $complaint_number;
                    $complaint->update();
                    
                    return $complaint_number;
                } 
            }
        }

        return false;
    }

    /**
     * Upload server images to s3 bucket.
     *
     * @param  $complaint
     * @return boolean
    */
    public function uploadComplaintImagesToS3Bucket($complaint) {

        //error_logs(json_encode($complaint));

        foreach ($complaint->images as $key => $value) {
            $complaintImages = ProductComplaintImages::where('id',$value['id'])->first();
            if ($complaintImages) {
                if (!empty($value['product_image']) && empty($value['s3_product_image'])) {
                    $existing_path = base_path($value['product_image']);
                    //error_logs($existing_path);
                    $image_name = md5(uniqid(rand(), true));
                    $image = \File::get($existing_path);
                    $filename = 'images/'.$image_name . '.' . 'png';
                    $put = \Storage::disk('s3')->put($filename, $image, 'public');
                    $s3_product_image = \Storage::disk('s3')->url($filename);
                    //error_logs($s3_product_image);

                    $complaintImages->s3_product_image = $s3_product_image;
                    $complaintImages->update();
                }
            }
        }

        return true;
    }
}
