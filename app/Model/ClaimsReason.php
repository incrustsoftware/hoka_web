<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClaimsReason extends Model
{
    protected $table = "claims_reason";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
    */
    protected $fillable = [
        'id',
        'name',
        'is_active'
    ];
}
