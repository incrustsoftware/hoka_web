<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = "roles";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
    */
    protected $fillable = [
        'rolename',
        'description',
        'is_active',
        'is_delete',
    ];
}
