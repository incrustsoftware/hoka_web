<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductComplaintImages extends Model
{
    protected $table = "product_complaint_images";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
    */
    protected $fillable = [
        'product_complaint_data_id',
        'product_image',
        's3_product_image',
        'created_by',
        'updated_by',
        'is_active',
        'is_delete',
    ];
}
