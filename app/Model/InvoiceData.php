<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InvoiceData extends Model
{
    protected $table = "invoice_data";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
    */
    protected $fillable = [
        'product_complaint_data_id',
        'invoice_no',
        'line_id',
        'order_no',
        'article_no',
        'quantity',
        'net_price',
        'discount1',
        'discount2',
        'discount3',
        'credit_note_endpoint',
        'credit_note_body',
        'invoice_date',
        'json_data',
        'complaint_status',
        'accept_message',
        'is_active',
        'is_delete',
    ];

    /**
     * Get the invoice  complaint.
     */
    public function getComplaint()
    {
        return $this->belongsTo('App\Model\ProductComplaintData', 'product_complaint_data_id');
    }

}
