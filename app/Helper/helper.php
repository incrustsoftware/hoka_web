<?php

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

//exceptionLog(__METHOD__, $ex);
if (!function_exists('error_logs')) {
    function error_logs($log, $flag = false)
    {
        if ($flag == false) {
            Log::info($log);
        } else {
            Log::info($log);
        }
    }
}

if (!function_exists('temp_log')) {
    function temp_log($log)
    {
        Log::info($log);
    }
}

if (!function_exists('getValidatorFailedResponse')) {
    function getValidatorFailedResponse($validator)
    {
        return response([
            'Status' => false,
            'Message' => $validator->errors()->all(),
            'Code' => 400
        ], 400);
    }
}

if (!function_exists('getErrorResponse')) {
    function getErrorResponse($message, $data = null)
    {
        return response([
            'Status' => false,
            'Data' => $data,
            'Message' => $message,
            'Code' => 400
        ], 400);
    }
}

if (!function_exists('getSuccessResponse')) {
    function getSuccessResponse(
        $data = '',
        $message = []
    ) {
        return response([
            'Status' => true,
            'Data' => $data,
            'Message' => $message,
            'Code' => 200
        ], 200);
    }
}

if (!function_exists('getUnauthorizedResponse')) {
    function getUnauthorizedResponse()
    {
        return response([
            'Status' => false,
            'Message' => ["unauthorized access."],
            'Code' => 401
        ], 401);
    }
}

if (!function_exists('getMaintenanceModeResponse')) {
    function getMaintenanceModeResponse()
    {
        return response([
            'Status' => false,
            'Message' => ["Sorry, Application is now in maintenance mode."],
            'Code' => 503
        ], 503);
    }
}

if (!function_exists('getTimeZone')) {
    function getTimeZone()
    {
        $timezone = date_default_timezone_get();

        return $timezone;
    }
}

if (!function_exists('getAppUrl')) {
    function getAppUrl()
    {
        return Config::get("app.url");
    }
}

if (!function_exists('getLoginUrl')) {
    function getLoginUrl()
    {
        return Config::get("app.login_url");
    }
}

if (!function_exists('getErrorMessages')) {
    function getErrorMessages($key = null)
    {
        $error = Config::get("messages.ErrorMessages");

        if ($key != null) {
            $error = $error[$key];
        }

        return $error;
    }
}

if (!function_exists('getSuccessMessages')) {
    function getSuccessMessages($key = null)
    {
        $success = Config::get("messages.SucessMessages");

        if ($key != null) {
            $success = $success[$key];
        }

        return $success;
    }
}

if (!function_exists('getSeetings')) {
    function getSeetings()
    {
        return Config::get("settings");
    }
}

if (!function_exists('getPaginationCount')) {
    function getPaginationCount()
    {
        return Config::get("settings.pagination");
    }
}

if (!function_exists('exceptionLog')) {
    function exceptionLog($l_name, $ex, $exce = true)
    {
        if ($exce === true) {
            $e_log = $ex->getFile().' / '.$ex->getMessage().' on line '.$ex->getLine().' / '.$ex->getCode();//.' / '.$ex->getTraceAsString();
        } else {
            $e_log = $ex;
        }

        if (!is_array($e_log)) {
            $e_log = array($e_log);
        }

        $exception = new Logger('exception');
        $exception->pushHandler(
            new StreamHandler(storage_path('logs/exception.log')), Logger::INFO);
        $exception->info('', ['************************************************']);
        $exception->info($l_name, $e_log);
    }
}

if (!function_exists('getCurrentDateTime')) {
    function getCurrentDateTime()
    {
        return Carbon::now()->toDateTimeString();
    }
}

if (!function_exists('getAppEnvironment')) {
    function getAppEnvironment()
    {
        $environment = App::environment();

        return $environment;
    }
}

if (!function_exists('getQueryLogs')) {
    function getQueryLogs()
    {
        //error_logs('In getQueryLogs');
        DB::enableQueryLog();
        $log = DB::getQueryLog();
        error_logs($log);

        return $log;
    }
}