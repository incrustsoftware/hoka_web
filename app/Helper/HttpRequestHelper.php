<?php

namespace App\Helper;

use App\User;
use Config;
use GuzzleHttp\Client;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Helper\UserHelper;

class HttpRequestHelper
{
    protected $uri;
    protected $method;
    protected $path;
    protected $request_data;

    /**
     * Instantiate a new UploadHelper instances.
    */
    public function __construct()
    {
        $this->setClient(new Client());
    }

    /**
     * @return mixed
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param mixed $uri
     */
    public function setUri($uri)
    {
        $this->uri = $uri.'/';
    }

    /**
     * @return \GuzzleHttp\Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @param \GuzzleHttp\Client $client
     */
    public function setClient(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return method
     */
    public function getRequestMethod()
    {
        return $this->method;
    }

    /**
     * @param mixed $method
     */
    public function setRequestMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return path
     */
    public function getRequestPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setRequestPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return req_data
     */
    public function getRequestData()
    {
        return $this->request_data;
    }

    /**
     * @param mixed $request_data
     */
    public function setRequestData($request_data)
    {
        $this->request_data = $request_data;
    }

    /**
     * This method is used to send http reuqest.
     * @param string        $method
     * @param string        $path
     * @param array         $headers
     * @param array|NULL    $data
     * @param array|NULL    $options
     * @return mixed
     */
    public function sendHttpRequest(
        string $method,
        string $path,
        array  $headers = [],
        $data = null,
        array  $options = []
    ) {
        $endPoint = $this->getUri() . $path;
        ////error_logs($endPoint);
        if (!empty($headers)) {
            $options['headers'] = $headers;
        }

        if (is_array($data)) {
            if (!empty($data)) {
                $options['form_params'] = $data;
            }
        } else {
            $options['body'] = $data;
        }

        error_logs('************* Start Request *****************');
        error_logs($method);
        error_logs($endPoint);
        error_logs($options);

        try {

            $response = $this->getClient()->request(
                $method,
                $endPoint,
                $options
            );

            $res = $response->getBody()->getContents();

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $guzzleResult = $e->getResponse();
            $res = $guzzleResult->getBody()->getContents();
            $status_code = $guzzleResult->getStatusCode();
            error_logs('Error Code :'.$status_code);
        }

        error_logs('------------- Request Response --------------');
        error_logs($res);

        return $res;
    }

    /**
     * This method is used to get hoka server token.
     * @param null
     * @return $response
    */
    public function getHokaServerBearerToken()
    {
        $method = $this->getRequestMethod();
        $path = $this->getRequestPath();
        $data = $this->getRequestData();
        $option = [];

        $header['Content-Type'] = 'text/html';

        return $this->sendHttpRequest(
            $method,
            $path,
            $header,
            $data,
            $option
        );
    }

    /**
     * This method is used to manage shooes size.
     * @param $Size
     * @return $Size
    */
    public function manageSize($Size)
    {

        if ($Size < 10) {
            $Size = (float) $Size;
            $Size = '0'.$Size;
        }

        return $Size;
    }

    /**
     * This method is used to get invoices data.
     * @param $ModelNumber, $Size, $ColorCode
     * @return $response
    */
    public function getInvoicesData($ModelNumber, $Size, $ColorCode, $customer_id)
    {

        $response = (new UserHelper())->getServerBearerToken();
        if ($response['Status'] === false) {
            $response['Invoice_API_Error'] = 0;

            return  $response;
        }

        //$Size = $this->manageSize($Size);

        $this->setUri(Config::get("app.hoka_server"));
        $bearerToken = $response['Token'];
        $request['ModelNumber'] = $ModelNumber;
        $request['Size'] = $Size;
        $request['ColorCode'] = $ColorCode;
        $method = "POST";
        $path = "RUNAPI/api/complaint/".$customer_id."/invoicelines";
        $data = json_encode($request);
        $option = [];
        $header['Content-Type'] = 'application/json';
        $header['Authorization'] = 'Bearer ' . $bearerToken;

        $response = $this->sendHttpRequest(
            $method,
            $path,
            $header,
            $data,
            $option
        );

        //$response = '{"CustomerNo":40821,"Invoices":[{"InvoiceNo":96428,"InvoiceDate":"2021-01-08T00:00:00","InvoiceLines":[{"LineID":776256,"OrderNo":"60645","ArticleNo":"SS1110534BWHT11","Quantity":1.0,"NetPrice":999.0,"Discount1":18.0,"Discount2":0.0,"Discount3":0.0,"CreditnoteEndpoint":"http://87.248.15.59/RUNAPI/api/complaint/40821/96428/creditorderline","CreditnoteBody":"ew0KICAiT3JkZXJObyI6ICI2MDY0NSIsDQogICJMaW5lSUQiOiA3NzYyNTYNCn0="}]}],"IsError":false,"ErrorResponse":null}';


        $invoices = json_decode($response, true);
        if ($invoices['IsError'] == true) {
            //Errors
            $result['Status'] = false;
            $errors = [];
            $ErrorResponse = $invoices['ErrorResponse'];
            foreach ($ErrorResponse as $key => $value) {
                $message = 'Invoices API : '.$value['ErrorMessage'];
                array_push($errors, $message);
            }

            $result['Message'] = $errors;
            $result['Invoice_API_Error'] = 1;
        } else {
            $result['Status'] = true;
            $result['Data'] = $invoices;
        }

        return $result;
    }



    /**
     * This method is used accept complaint request
     * @param $CustomerNo, $InvoiceNo, $OrderNo, $LineID
     * @return $response
    */
    public function acceptComplaintRequest(
        $CustomerNo,
        $InvoiceNo,
        $OrderNo,
        $LineID,
        $ComplaintReasonNo,
        $ClaimID,
        $CustomerReference,
        $ProductionID
    ) {

        $response = (new UserHelper())->getServerBearerToken();
        if ($response['Status'] === false) {
            return  $response;
        }

        $this->setUri(Config::get("app.hoka_server"));
        $bearerToken = $response['Token'];

        $request['OrderNo'] = $OrderNo;
        $request['LineID'] = $LineID;
        $request['ComplaintReasonNo'] = $ComplaintReasonNo;
        $request['ClaimID'] = $ClaimID;

        if ($CustomerReference != null && $CustomerReference != 'null' && $CustomerReference != '') {
            $request['CustomerReference'] = $CustomerReference;
        }

        $request['ProductionID'] = $ProductionID;
        
        $method = "POST";

        //$CustomerNo = 40821;

        $path = "RUNAPI/api/complaint/".$CustomerNo."/".$InvoiceNo."/creditorderline";
        $data = json_encode($request);
        $option = [];
        $header['Content-Type'] = 'application/json';
        $header['Authorization'] = 'Bearer ' . $bearerToken;

        $response = $this->sendHttpRequest(
            $method,
            $path,
            $header,
            $data,
            $option
        );

        // $response = '{
        //     "CreditOrderNo": null,
        //     "IsError": true,
        //     "ErrorResponse": [
        //         {
        //             "ErrorMessage": "CustomerNo 1 does not exist"
        //         },
        //         {
        //             "ErrorMessage": "Validation failed. See list for more info"
        //         }
        //     ]
        // }';

        $accepted_response = json_decode($response, true);
        if ($accepted_response['IsError'] == true) {
            //Errors
            $result['Status'] = false;
            $errors = [];
            $ErrorResponse = $accepted_response['ErrorResponse'];
            foreach ($ErrorResponse as $key => $value) {
                $message = 'Credit API : '.$value['ErrorMessage'];
                array_push($errors, $message);
            }

            $result['Message'] = $errors;
        } else {
            $result['Status'] = true;
            $result['Data'] = $accepted_response;
        }

        return $result;
    }

    /**
     * This method is used to prepare http request.
     * @param null
     * @return $response
     */
    public function prepareHttpRequest()
    {
        $method = $this->getRequestMethod();
        $path = $this->getRequestPath();
        $data = $this->getRequestData();
        $header = [];
        $option = [];

        return $this->sendHttpRequest(
            $method,
            $path,
            $header,
            $data,
            $option
        );
    }

    /**
     * This method is used to check internet connections.
     * @param  null
     * @return Boolean
    */
    public function isInternet()
    {
        $connected = @fsockopen("www.google.com", 80);
        if ($connected) {
            $is_conn = true;
            //error_logs('Internet : yes');
            fclose($connected);
        } else {
            //error_logs('Internet : No');
            $is_conn = false;
        }

        return $is_conn;
    }

}
