<?php

namespace App\Helper;

use App\User;
use App\Model\Settings;
use Config;
use Illuminate\Support\Carbon;
use App\Helper\HttpRequestHelper;

class UserHelper
{
    /**
     * Instantiate a new user helper instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Generate the password.
     *
     * @param $password 
     * @return $password
    */
    public static function generatePassword($password = null)
    {
        if ($password == null) {
            $password = str_random(8); //str_random(8); //'Admin1234'
        }

        $data['plain_password'] = $password;
        $data['encrypt_password'] = bcrypt($password);
        return $data;
    }


    /**
     * Register a new complaint (old).
     *
     * @param  
     * @return $token
    */
    public function getServerBearerToken()
	{
		$response['Status'] = false;
		$current_date = getCurrentDateTime();
		$http = new HttpRequestHelper();

		try {
			$token = Settings::where('name', 'BearerToken')
				->where('expiry_date', '>', $current_date)
				->first();

			if ($token) {
                $resultArr = json_decode($token->value, true);
				$response['Status'] = true;
				$response['Token'] =  $resultArr['access_token'];
			} else {
				$http->setUri(Config::get("app.hoka_server"));
				$http->setRequestMethod("POST");
				$http->setRequestPath("RUNAPI/token");
				$data = "grant_type=password&username=".Config::get("app.hoka_username")."&password=".Config::get("app.hoka_password");
				$http->setRequestData($data);
				$result = $http->getHokaServerBearerToken();

                $resultArr = json_decode($result, true);
                if (isset($resultArr['access_token'])) {
                    $expiry_date = Carbon::now()->addSeconds($resultArr['expires_in'])->toDateTimeString();

                    $settings['name'] = 'BearerToken';
                    $settings['value'] = $result;
                    $settings['expiry_date'] = Carbon::parse($expiry_date)->subDay()->toDateTimeString();
                    $settings['is_active'] = 1;

                    $token = Settings::where('name', 'BearerToken')->first();
                    if ($token) {
                        $token->update($settings);
                    } else {
                        Settings::create($settings);
                    }

    				$response['Status'] = true;
    				$response['Token'] = $resultArr['access_token'];
                    $response['Message'] = 'Success';

                } else {
                    $response['Status'] = false;
                    $response['Message'] = "Authentication token is not generating from Hoka's CRM website to access the invoice line data.";
                }
			}

		} catch (\Exception $ex) {
			$response['Status'] = false;
			$response['Message'] = $ex->getMessage();
		}

		return $response;
	}
}
