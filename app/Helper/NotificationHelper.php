<?php

namespace App\Helper;

use AWS;
use Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use App\Helper\HttpRequestHelper;
use App\Model\ProductComplaintData;
use Illuminate\Support\Facades\Crypt;
use App\Mail\TestMail;
use App\Mail\SignUp;
use App\Mail\ForgotPassword;
use App\Mail\BrowserNotification;
use App\Mail\AcceptComplaint;
use App\Mail\NotAcceptComplaint;
use App\Mail\CustomerReceipt;
use App\Mail\HokaReceipt;

class NotificationHelper
{
    protected $http_request_helper;
    protected $success;
    protected $error;
    protected $upload;
    
    /**
     * Instantiate a new notification instance.
     */
    public function __construct()
    {
        $this->http_request_helper = new HttpRequestHelper();
        $this->success = getSuccessMessages();
        $this->error = getErrorMessages();
        $this->upload = new HttpRequestHelper();

        // $MAIL_USERNAME = env('MAIL_USERNAME','');
        // $MAIL_PASSWORD = env('MAIL_PASSWORD','');

        $MAIL_USERNAME = Config::get('mail.username');
        $MAIL_PASSWORD = Config::get('mail.password');
        $username = base64_decode($MAIL_USERNAME);
        $password = base64_decode($MAIL_PASSWORD);
        Config::set('mail.username', $username);
        Config::set('mail.password', $password);
    }

    /**
     * This method is used to send sign up email for user.
     *
     * @param $user, $password
     * @return null
     */
    public function sendSignUpEmail(
        $user,
        $password
    ) {
        $response = false;
        try {
            $data['name'] = ucfirst($user->firstname);
            $data['email'] = $user->email;
            $data['password'] = $password;
            $data['url'] = getLoginUrl();
            //$this->setMailConfiguration($user->id);
            if ($this->upload->isInternet()) {
                Mail::to($user->email, $user->firstname)->send(new SignUp($data));
                $response = true;
            }
        } catch (\Exception $ex) {
            $response = false;
        }
        
        return $response;
    }

    /**
     * This method is used to send forgot password email for user.
     *
     * @param $user, $password
     * @return null
     */
    public function sendForgotPasswordEmail(
        $user,
        $password
    ) {
        $response = false;
        try {
            $data['name'] = ucfirst($user->firstname);
            $data['email'] = $user->email;
            $data['password'] = $password;
            $data['url'] = getLoginUrl();
            //$this->setMailConfiguration($user->id);
            if ($this->isInternet()) {
                Mail::to($user->email, $user->firstname)->send(new ForgotPassword($data));
                $response = true;
            }
        } catch (\Exception $ex) {
            $response = false;
        }

        return $response;
    }

    /**
     * This method is used to send browser notification to the user.
     *
     * @param $user, $message
     * @return null
     */
    public function sendBrowserNotification(
        $user,
        $message
    ) {
        $response = false;
        try {
            $data['name'] = ucfirst($user->firstname);
            $data['email'] = $user->email;
            $data['message'] = $message;
            //$this->setMailConfiguration($user->id);
            if ($this->upload->isInternet()) {
                Mail::to($user->email, $user->firstname)->send(new BrowserNotification($data));
                $response = true;
            }
        } catch (\Exception $ex) {
            $response = false;
        }

        return $response;
    }

    /**
     * This method is used to send complaint accepted email to user.
     *
     * @param $compliant_id
     * @return boolean
     */
    public function sendComplaintAcceptedNotification(
        $compliant_id
    ) {
        $response = false;

        $complaint = ProductComplaintData::where('id', $compliant_id)
            ->where('complaint_email', '!=', null)
            ->first();

        if (!$complaint) {
            error_logs('Email : Complaint not found for id '.$compliant_id);

            return $response;
        }

        try {
            $data['name'] = ucfirst($complaint->dealer_name);
            $data['email'] = $complaint->complaint_email;
            $data['message'] = $complaint->complaint_message;
            $data['url'] = url('/complaint').'/'.$complaint->token;
            $data['complaint_number'] = $complaint->complaint_number;
            $data['customer_reference_number'] = $complaint->customer_reference_number;
            //$this->setMailConfiguration($user->id);

            if ($this->upload->isInternet()) {
                Mail::to($complaint->complaint_email, $complaint->dealer_name)->send(new AcceptComplaint($data));

                if (Mail::failures()) {
                    $response = false;
                } else {
                    $response = true;
                }
            }
        } catch (\Exception $ex) {
            error_logs('Email Exception : Send Complaint Accepted Notification : '.$ex->getMessage());
            error_logs('Complaint Id : '.$compliant_id);
            $response = false;
        }

        return $response;
    }

    /**
     * This method is used to send complaint not accepted email to user.
     *
     * @param $compliant_id
     * @return boolean
    */
    public function sendComplaintNotAcceptedNotification(
        $compliant_id
    ) {
        $response = false;

        $complaint = ProductComplaintData::where('id', $compliant_id)
            ->where('complaint_email', '!=', null)
            ->first();

        //error_logs($complaint);
        
        if (!$complaint) {
            error_logs('Email : Complaint not found for id '.$compliant_id);

            return $response;
        }

        try {
            $data['name'] = ucfirst($complaint->dealer_name);
            $data['email'] = $complaint->complaint_email;
            $data['message'] = $complaint->complaint_message;
            $data['url'] = url('/complaint').'/'.$complaint->token;
            $data['complaint_number'] = $complaint->complaint_number;
            $data['customer_reference_number'] = $complaint->customer_reference_number;
            //$this->setMailConfiguration($user->id);
            
            if ($this->upload->isInternet()) {
                Mail::to($complaint->complaint_email, $complaint->dealer_name)->send(new NotAcceptComplaint($data));

                if (Mail::failures()) {
                    $response = false;
                } else {
                    $response = true;
                }
            }
        } catch (\Exception $ex) {
            error_logs('Email Exception : Send Complaint Not Accepted Notification : '.$ex->getMessage());
            error_logs('Complaint Id : '.$compliant_id);
            $response = false;
        }

        return $response;
    }

    /**
     * This method is used to send complaint received email to admin and customer.
     *
     * @param $compliant_id
     * @return boolean
    */
    public function sendComplaintReceivedNotification(
        $compliant_id
    ) {
        $response = false;

        $complaint = ProductComplaintData::where('id', $compliant_id)
            ->where('complaint_email', '!=', null)
            ->first();

        if (!$complaint) {
            error_logs('Email : Complaint not found for id '.$compliant_id);

            return $response;
        }

        $data = $complaint->toArray();

        try {
            error_logs('Complaint Receipt Notification to Customer: ');
            //error_logs(json_encode($data));

            if ($this->upload->isInternet()) {
                Mail::to($complaint->complaint_email, $complaint->dealer_name)->send(new CustomerReceipt($data));

                if (Mail::failures()) {
                    error_logs('Not Sent to: '.$complaint->dealer_name);
                    $response = false;
                } else {
                    error_logs('Sent to: '.$complaint->dealer_name);
                    $response = true;
                }
            }
        } catch (\Exception $ex) {
            error_logs('Email Exception : Send Complaint Receipt Notification to Customer : '.$ex->getMessage());
            error_logs('Complaint Id : '.$compliant_id);
            $response = false;
        }

        try {
            error_logs('Complaint received email to admin: ');
            //error_logs(json_encode($data));

            $name = 'Hoka Admin';
            $email = Config::get('mail.hoka_receipt');
            $subject = 'Complaint from '.$complaint->dealer_name.' - '.$complaint->complaint_number;

            if (!empty($complaint->customer_reference_number)) {
                $subject = $subject.'/Egen referance '.$complaint->customer_reference_number;
            }

            $data['subject'] = $subject;
            error_logs($email);
            error_logs($data['subject']);
            if ($this->upload->isInternet()) {
                Mail::to($email, $name)->send(new HokaReceipt($data));

                if (Mail::failures()) {
                    error_logs('Not Sent to: '.$name);
                    $response = false;
                } else {
                    error_logs('Sent to: '.$name);
                    $response = true;
                }
            }
        } catch (\Exception $ex) {
            error_logs('Email Exception : Send Complaint Receipt Notification to Admin: '.$ex->getMessage());
            error_logs('Complaint Id : '.$compliant_id);
            $response = false;
        }

        return $response;
    }

    /**
     * This method is used to send sms for user.
     *
     * @param $phone_number, $message
     * @return null
     */
    public function sendSMS(
        $phone_number,
        $message,
        $userid
    ) {

        $response = false;
        try { 
            //$this->setSMSConfiguration($userid);
            $sms = AWS::createClient('sns');
            $result = $sms->publish([
                'Message' => $message,
                'PhoneNumber' => $phone_number,
                'MessageAttributes' => [
                    'AWS.SNS.SMS.SMSType' => [
                        'DataType' => 'String',
                        'StringValue' => 'Transactional',
                    ],
                ],
            ]);

            if ($result['@metadata']['statusCode'] == 200) {
                $response = true;
            }
        } catch (\Exception $ex) {
            $response = false;
        }

        return $response;
    }


    /**
     * This method is used to set mail configuration.
     *
     * @param $userid
     * @return null
     */
    public function setMailConfiguration($userid)
    {
        $from = [
            'address' => 'sender_email',
            'name' => 'PermAlert',
        ];

        //$password = Crypt::decryptString('password');
       
        Config::set('mail.driver', 'smtp');
        Config::set('mail.host', 'carrier_domain');
        Config::set('mail.port', 'port_number');
        Config::set('mail.username', 'sender_email');
        Config::set('mail.password', 'password');
        Config::set('mail.from', 'from');
        Config::set('mail.encryption', null);
    }

    /**
     * This method is used to set sms configuration.
     *
     * @param $userid
     * @return null
     */
    public function setSMSConfiguration($userid) {
        $credentials = [
            'key'    => 'aws_access_key',
            'secret' => 'aws_secret_key',
        ];

        Config::set('aws.credentials', $credentials);
        Config::set('aws.region', 'aws_region');
    }

    /**
     * This method is used to send test sms for user.
     *
     * @param $phone_number, $message
     * @return null
     */
    public function testSendSMS(
        $phone_number,
        $message,
        $userid,
        $request
    ) {
        $response = false;
        try {
            $credentials = [
                'key'    => $request->aws_accesskey,
                'secret' => $request->aws_secretkey,
            ];

            Config::set('aws.credentials', $credentials);
            Config::set('aws.region', $request->aws_region);

            if ($this->upload->isInternet()) {
                $sms = AWS::createClient('sns');
                $result = $sms->publish([
                    'Message' => $message,
                    'PhoneNumber' => $phone_number,
                    'MessageAttributes' => [
                        'AWS.SNS.SMS.SMSType' => [
                            'DataType' => 'String',
                            'StringValue' => 'Transactional',
                        ],
                    ],
                ]);

                if ($result['@metadata']['statusCode'] == 200) {
                    $response = true;
                }

            } else {
                $response = 'Internet';
            }
        } catch (\Exception $ex) {
            $response = false;
        }

        return $response;
    }

    /**
     * This method is used to send test email for user.
     *
     * @param $user, $password
     * @return null
     */
    public function sendTestEmail(
        $user,
        $request
    ) {
        $response = 'False';
        try {
            $data['name'] = ucfirst($user->firstname);
            $from = [
                'address' => $request->sender_email,
                'name' => 'PermAlert',
            ];

            Config::set('mail.driver', 'smtp');
            Config::set('mail.host', $request->carrier_domain);
            Config::set('mail.port', $request->port_number);
            Config::set('mail.username', $request->sender_email);
            Config::set('mail.password', $request->sender_password);
            Config::set('mail.from', $from);

            $encryption = $request->encryption;
            if ($request->encryption == '' ||  $request->encryption == null || $request->encryption == 'none') {
                $encryption = null;
            }
            Config::set('mail.encryption', $encryption);
            // $email_con = Config::get('mail');
            // error_logs($email_con);

            if ($this->upload->isInternet()) {
                Mail::to($user->email, $user->firstname)->send(new TestMail($data));
                $response = 'True';
            } else {
                $response = 'Internet';
            }
        } catch (\Exception $ex) {
            $response = 'False';
        } finally {
            return $response;
        }
    }
}
