<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerReceipt extends Mailable
{
    use Queueable, SerializesModels;

    protected $args;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($args)
    {
        $this->args = $args;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'Hoka Complaint Receipt - '.$this->args['complaint_number'];
        if (!empty($this->args['customer_reference_number'])) {
            $subject = $subject.'/Egen referance '.$this->args['customer_reference_number'];
        }

        return $this->subject($subject)->markdown('emails.CustomerReceipt', ['data' => $this->args]);
    }
}
