<title>@yield('title')</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="{{asset('themes/frontend/assets/css/jquery-ui.css')}}">
<link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/ti-icons/css/themify-icons.css')}}">
<link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/css/vendor.bundle.base.css')}}">
<link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/font-awesome/css/font-awesome.min.css')}}">
<!-- endinject -->
<link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/simple-line-icons/css/simple-line-icons.css')}}">
<!-- Plugin css for this page -->
<!-- Plugin css for this page -->
<link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}">
<!-- End plugin css for this page -->

<!-- End plugin css for this page -->
<link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/owl-carousel-2/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/owl-carousel-2/owl.theme.default.min.css')}}">
<!-- inject:css -->
<link rel="stylesheet" href="{{asset('themes/frontend/assets/css/horizontal-layout-light/style.css')}}">
<!-- endinject -->
<!-- <link rel="shortcut icon" href="{{asset('themes/frontend/assets/images/favicon.png')}}" /> -->
<link rel="shortcut icon" href="{{asset('themes/images/hoka_logo.png')}}" />
<!-- Date rang picker -->
<link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css')}}">

<!-- <script src="{{asset('themes/frontend/assets/js/jquery-3.3.1.js')}}"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="{{asset('themes/frontend/assets/app_js/common.js')}}"></script>
<script src="{{asset('themes/frontend/assets/app_js/jquery.cookie.js')}}"></script>

<style type="text/css">
    .sidebar .nav .nav-item.active > .nav-link i, .sidebar .nav .nav-item.active > .nav-link .menu-title, .sidebar .nav .nav-item.active > .nav-link .menu-arrow {
        color: #fff;
    }
    .toaster .alert-danger {
    	color: #a94442;
    	background-color: #f2dede;
    	border-color: #ebccd1;
    }

    .toaster  .alert-success {
	    color: #3c763d;
	    background-color: #dff0d8;
	    border-color: #d6e9c6;
	}
</style>