  <!-- endinject -->
  
 
  <script src="{{asset('themes/frontend/assets/js/jquery-ui.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/vendors/js/vendor.bundle.base.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/vendors/chart.js/Chart.min.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/vendors/owl-carousel-2/owl.carousel.min.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/vendors/sweetalert/sweetalert.min.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/vendors/jquery.avgrund/jquery.avgrund.min.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/js/off-canvas.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/js/template.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/js/settings.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/js/todolist.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/vendors/datatables.net/jquery.dataTables.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/js/owl-carousel.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/vendors/moment/moment.min.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>

  <!-- Custom js for this page-->
  <script src="{{asset('themes/frontend/assets/js/alerts.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/js/avgrund.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/app_js/loadingoverlay.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/app_js/jquery.toaster.js')}}"></script>
  
  <!-- <script src="{{asset('themes/frontend/assets/app_js/jquery.exzoom.js')}}"></script> -->
  <!-- End custom js for this page-->