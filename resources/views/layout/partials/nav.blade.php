<!-- partial:partials/_navbar.html -->
<div class="horizontal-menu">
    <nav class="navbar top-navbar col-lg-12 col-12 p-0 header_background">
        <div class="container header_new">
            <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                <a class="logo_image" href="{{ route('dashboard.dashboard',[$user_token,$web_token]) }}"><img src="{{asset('themes/images/hoka_logo.png')}}" alt="logo"/></a>
            </div>

            <input type="hidden" id="base_path" value="{{ url('/') }}">
            <input type="hidden" id="logout_path" value="{{ route('logout') }}">

            <div class="infoMsg">
          
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">

                <ul class="navbar-nav navbar-nav-right firstHeadNavRight">
                  
                    <input type="hidden" name="plantIdVal" id="plantIdVal" value="">

                    <li class="nav-item username_show">
                        
                        <a class="nav-link" href="#"  id="username">
                        <p class="plantname_show_p"></p>
                        </a>
                    </li>
                    
                    <li class="nav-item username_show">
                        
                        <a class="nav-link" href="#"  id="username">
                        <p class="username_show_p">Welcome</p>
                        <p class="username_show_p"></p>
                        </a>
                    </li>

                  
                    <li class="nav-item nav-profile dropdown">
                        <a class="nav-link" href="#" data-toggle="dropdown" id="profileDropdown">
                        <img src="{{asset('themes/images/user.png')}}" alt="profile"/>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                             
                            <a class="dropdown-item" data-toggle="modal" href="#changePassword"  id="change_password">
                                <i class="icon-user" style="color: #248afd;"></i>
                                Change Password?
                            </a>
                            <a class="dropdown-item" id="logout_user_session" href="{{route('logout')}}"> <!-- onclick="logout();" -->
                            <i class="ti-power-off text-primary" style="color: #248afd !important;" ></i>
                            Logout
                            </a>
                        </div>
                    </li>
                </ul>
                <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="horizontal-menu-toggle">
                <!-- <span class="ti-menu"></span> -->
                </button>
            </div>
        </div>
    </nav>

    <!-- Modal -->
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="changePassword" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header fg_password_modal">
                    <h4 class="modal-title ">Change password</h4>
                    <button type="button" class="modal_close btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <!-- <form class="pt-3" action="#" 
                  method="POST" novalidate> -->
                <form class="pt-3" action="#" method="POST" novalidate>
                    <div class="row modal_form_content">
                        <div class="col-md-2">
                            
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="current_password" class="control-label" >Old password</label>
                                <input type="password" class="form-control form-control-lg changepw_form_group" id="current_password" name="current_password" placeholder="Current password">
                                
                            </div>
                            <div class="form-group ">
                                <label for="new_password" class="control-label" >New password</label>
                                <input type="password" class="form-control form-control-lg " id="new_password"  name="new_password" placeholder="New password">
                               
                            </div>
                            <div class="form-group ">
                                <label for="new_password" class="control-label" >Confirm password</label>
                                <input type="password" class="form-control form-control-lg " id="confirm_password"  name="confirm_password" placeholder="Confirm password">
                            </div>
                           
                            <div class="roleSelectionButtonRow">
                                <input class="btn btn-block btn-primary btn-md" type="button" value="Save" onclick="changePassword();"> 
                            </div>
                      </div>
                    </div>
                </form>
              
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
//Logout user session.
function logout_old() {
    $.LoadingOverlay("show");

    request_url = getBaseUrl()+'/api/users/logout';

    $.ajax({
        type:"GET",
        url:request_url,
        dataType: "json",
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', getBearerToken());
        },
        success: function(response) {
            //console.log('User logout - '+JSON.stringify(response));
            $.LoadingOverlay("hide");
            localStorage.clear();
            //$.removeCookie("accessToken");
            if (response.Status) {
                window.location.href = getBaseUrl()+'/login';
            }
        },
        error: function(xhr, error) {
            $.LoadingOverlay("hide");
            localStorage.clear();
            //$.removeCookie("accessToken");
            window.location.href = getBaseUrl()+'/login';
        }
    });
}

function logout() {
    window.location.href = $("#logout_path").val();
}

function changePassword() {

    var old_password = $("#current_password").val();
    var new_password = $("#new_password").val();
    var confirm_password = $("#confirm_password").val();

    var request = {};
    request.new_password = new_password;
    request.old_password = old_password;
    request.confirm_password = confirm_password;
    request.user_id = getUserId();

    request_url = getBaseUrl()+'/api/users/changepassword';
    $.LoadingOverlay("show");
    $.ajax({
        type:"POST",
        url:request_url,
        data:request,
        dataType: "json",
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', getBearerToken());
        },
        success: function(response) {
            $.LoadingOverlay("hide");
            console.log('User change password - '+JSON.stringify(response));
            if (response.Status) {
                $("#changePassword").modal('hide');
                    $("#current_password").val('');
                    $("#new_password").val('');
                    $("#confirm_password").val('');
                $.toaster({ priority: 'success', title: 'Success', message: response.Message[0] });
            }
        },
        error: function(xhr, error) {
            $.LoadingOverlay("hide");
            manageErrorResponse(xhr, error);
        }
    });
}
</script>