<!doctype html>
<html lang="en">
    <head>
        @include('layout.partials.head')
    </head>
    <body id="my_html_body" style="display:none;">
        <div class="container-scroller">
            @include('layout.partials.nav')
            <!-- partial -->
            <div class="container-fluid page-body-wrapper">
                <div class="main-panel_old">
                    @include('layout.partials.header')
                    @yield('content')
                    @include('layout.partials.footer')
                    @include('layout.partials.footer-scripts')
                </div>
              <!-- main-panel ends -->
            </div>
            <!-- page-body-wrapper ends -->
        </div>
        <!-- container-scroller -->
    </body>
</html>
