<?php 

    $default = false;

    $user_read = isset($permission['user_read']) ? $permission['user_read'] : $default;
    $user_write = isset($permission['user_write']) ? $permission['user_write'] : $default;
    $role_read = isset($permission['role_read']) ? $permission['role_read'] : $default;
    $role_write = isset($permission['role_write']) ? $permission['role_write'] : $default;
    $device_read = isset($permission['device_read']) ? $permission['device_read'] : $default;
    $device_write = isset($permission['device_write']) ? $permission['device_write'] : $default;
    $template_read = isset($permission['template_read']) ? $permission['template_read'] : $default;
    $template_write = isset($permission['template_write']) ? $permission['template_write'] : $default;
    $report_read = isset($permission['report_read']) ? $permission['report_read'] : $default;
    $report_write = isset($permission['report_write']) ? $permission['report_write'] : $default;
    $calibration_certificate_read = isset($permission['calibration_certificate_read']) ? $permission['calibration_certificate_read'] : $default;
    $calibration_certificate_write = isset($permission['calibration_certificate_write']) ? $permission['calibration_certificate_write'] : $default;
    $calibration_check_read = isset($permission['calibration_check_read']) ? $permission['calibration_check_read'] : $default;
    $calibration_check_write = isset($permission['calibration_check_write']) ? $permission['calibration_check_write'] : $default;
    $notification_read = isset($permission['notification_read']) ? $permission['notification_read'] : $default;
    $notification_write = isset($permission['notification_write']) ? $permission['notification_write'] : $default;
    $settings = isset($permission['settings']) ? $permission['settings'] : $default;
    $admin = isset($permission['admin']) ? $permission['admin'] : $default;
    $customers = isset($permission['customers']) ? $permission['customers'] : $default;



    $is_admin = isset($portal['Admin']) ? $portal['Admin'] : $default;
    $is_staff = isset($portal['Staff']) ? $portal['Staff'] : $default;
    $is_agent = isset($portal['Agent']) ? $portal['Agent'] : $default;
    $is_oem = isset($portal['OEM']) ? $portal['OEM'] : $default;
    $is_customer = isset($portal['Customer']) ? $portal['Customer'] : $default;

//  else {
//     $user_read = false;
//     $user_write = false;
//     $role_read = false;
//     $role_write = false;
//     $device_read = $permission['device_read'];
//     $device_write = $permission['device_write'];
//     $template_read = $permission['template_read'];
//     $template_write = $permission['template_write'];
//     $report_read = $permission['report_read'];
//     $report_write = $permission['report_write'];
//     $calibration_certificate_read = $permission['calibration_certificate_read'];
//     $calibration_certificate_write = $permission['calibration_certificate_write'];
//     $calibration_check_read = $permission['calibration_check_read'];
//     $calibration_check_write = $permission['calibration_check_write'];
//     $notification_read = $permission['notification_read'];
//     $notification_write = $permission['notification_write'];
//     $settings = $permission['settings'];
//     $admin = $permission['admin'];
//     $customers = $permission['customers'];
// }

?>

<script src="{{asset('app-js/locale/jquery.i18n.js') }}"></script>
<script src="{{asset('app-js/locale/jquery.i18n.messagestore.js') }}"></script>
<script src="{{asset('app-js/locale/jquery.i18n.fallbacks.js') }}"></script>
<script src="{{asset('app-js/locale/jquery.i18n.parser.js') }}"></script>
<script src="{{asset('app-js/locale/jquery.i18n.emitter.js') }}"></script>
<script src="{{asset('app-js/locale/jquery.i18n.language.js') }}"></script>
<script src="{{asset('app-js/languages.js') }}"></script>
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->

  <input type="hidden" id="is_admin_" value="{{$is_admin}}">
  <input type="hidden" id="is_staff_" value="{{$is_staff}}">
  <input type="hidden" id="is_agent_" value="{{$is_agent}}">
  <input type="hidden" id="is_oem_" value="{{$is_oem}}">
  <input type="hidden" id="is_customer_" value="{{$is_customer}}">

  <a href="javascript::void(0);" class="brand-link" style="background-color:#0081b8; padding:3px">
      <!-- <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
      <!-- <span class="brand-text font-weight-light">Calibration Portal</span> -->
      <img src="{{asset('dist/img/logo2.png') }}" alt="RIVERTRACE Logo" style="margin: 0px;
      max-height: unset; width:100%">

  </a>
  <!-- Sidebar -->
  <div class="sidebar">
      <!-- Sidebar user panel (optional) pb-3 -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
              <img id="sidebar_profile" src="{{asset('dist/img/2.jpeg') }}" class="img-circle elevation-2" alt="User Image" style="height:2.1rem;">
          </div>
          <div class="info">
              <a id="username_" href="javascript::void(0);" class="d-block">John Doe</a>
          </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
          <nav class="mt-2">
              <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                  <!-- Add icons to the links using the .nav-icon class
     with font-awesome or any other icon font library -->



                <li class="nav-item">
                    <a id="render_dashboard_page" href="api/dashboards/dashboard" class="nav-link render_page">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p data-i18n="Dashboard">
                            Dashboard
                        </p>
                    </a>
                </li>

                @if($role_read === true)
                <li class="nav-item">
                    <a id="render_roles_page" href="api/roles" class="nav-link render_page">
                        <i class="nav-icon fas fa-unlock"></i>
                        <p data-i18n="Roles">
                            Roles
                        </p>
                    </a>
                </li>
                @endif

                @if($user_read === true)
                <li class="nav-item">
                    <a id="render_users_page" href="api/users" class="nav-link render_page">
                        <i class="nav-icon fas fa-users"></i>
                        <p data-i18n="Users">
                            Users
                        </p>
                    </a>
                </li>
                @endif

                @if($is_customer === false)
                <li class="nav-item">
                    <a id="render_end_users" href="api/users/enduser" class="nav-link render_page">
                        <i class="nav-icon fas fa-user"></i> <!-- circle -->
                        <p data-i18n="End Users">
                            End Users
                        </p>
                    </a>
                </li>
                @endif

                @if($is_customer === false)
                <li class="nav-item">
                    <a href="api/getcertificaterequest" class="nav-link render_page">
                        <i class="nav-icon fas fa-share-square"></i>
                       <!--  <i class="nav-icon fas fa-caret-right"></i> -->
                        <p data-i18n="Calibration Requests">Calibration Requests</p>
                    </a>
                </li>
                @endif

                @if($is_admin == true || $is_staff == true)
                <li class="nav-item">
                    <a id="render_certificate_type_duration" href="api/certificate_type_durations" class="nav-link render_page">
                        <i class="nav-icon fas fa-file"></i> <!-- circle -->
                        <p data-i18n="Certificate Type Durations">
                            Certificate Type Durations
                        </p>
                    </a>
                </li>
                @endif
                @if($is_admin === true || $is_staff== true || $is_agent === true || $is_oem === true)
                <li class="nav-item">
                    <a id="render_certificate_type_duration" href="api/port_preference" class="nav-link render_page">
                        <i class="nav-icon fas fa-file"></i> <!-- circle -->
                        <p data-i18n="Port Preference">
                            Port Preference
                        </p>
                    </a>
                    <!-- 
                    <a id="render_certificate_type_duration" href="api/certificate_type_durations" class="nav-link render_page">
                       
                    </a> -->
                </li>
                @endif


                <!-- <li class="nav-item">
                    <a id="render_users_page" href="api/register_device_list" class="nav-link render_page">
                        <i class="nav-icon fas fa-tablet"></i>
                        <p>
                            Registered Devices
                        </p>
                    </a>
                </li> -->
                

                @if($device_write === true)
                <li class="nav-item has-treeview ">
                    <a id="render_merlinimport_page" href="api/merlins" class="nav-link render_page">
                        <i class="nav-icon fas fa-exchange-alt"></i>
                        <p>
                            <span data-i18n="Merlin Import">Merlin Import</span>
                        </p>
                    </a>
                </li>
                @endif

                @if($device_read === true && ($is_admin == true || $is_staff == true))
                <li class="nav-item">
                    <a id="render_devices_page" href="api/devices" class="nav-link render_page">
                        <i class="nav-icon fas fa-box-open"></i>
                        @if($is_admin == true || $is_staff == true)
                            <p><span data-i18n="Part List">Part List</span> <span data-i18n="(Merlin)">(Merlin)</span></p>
                        @else
                            <p data-i18n="Part List">Part List</p>
                        @endif
                    </a>
                </li>
                @endif

                @if($device_read === true && ($is_admin == true || $is_staff == true))
                <li class="nav-item">
                    <a id="render_customers_page" href="api/customers" class="nav-link render_page">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            <span data-i18n="Customers List">Customers List</span> <span data-i18n="(Merlin)">(Merlin)</span>
                        </p>
                    </a>
                </li>
                @endif

                @if($device_read === true || $calibration_check_write === true  || $calibration_certificate_write === true)
                <li class="nav-item">
                    <a id="render_salesorder_page" href="api/sales" class="nav-link render_page">
                        <i class="nav-icon fas fa-credit-card"></i>
                        @if($is_admin == true || $is_staff == true)
                            <p><span data-i18n="Sales Order List">Sales Order List</span> <span data-i18n="(Merlin)">(Merlin)</span></p>
                        @else
                            <p data-i18n="Sales Order List">Sales Order List</p>
                        @endif
                    </a>
                </li>
                @endif

                @if($is_admin == true || $is_staff == true)
                <li class="nav-item">
                    <a id="render_device_form_submission_page" href="api/form-builder/my-submissions/X/id" class="nav-link render_page">
                        <i class="nav-icon fas fa-bell"></i>
                        <p data-i18n="Form Submission">
                           Form Submission
                        </p>
                    </a>
                </li>
                @endif
                @if($is_admin == true || $is_staff == true)
                <li class="nav-item">
                    <a id="render_device_list_certificate_page" href="api/listcertificate" class="nav-link render_page">
                        <i class="nav-icon fas fa-bell"></i>
                        <p data-i18n="Device List - Certificates">
                           Device List - Certificates
                        </p>
                    </a>
                </li>
                @endif

                <li class="nav-item">
                    <a id="render_vessel_devices_page" href="api/deviceList" class="nav-link render_page">
                        <i class="nav-icon fas fa-bell"></i>
                        <p data-i18n="Registered Device List">
                            Registered Device List
                        </p>
                    </a>
                </li>

                @if($customers === true)
                <li class="nav-item">
                    <a id="render_devices_certificate_list_page" href="api/certificateslist" class="nav-link render_page">
                        <i class="nav-icon fas fa-bell"></i>
                        <p data-i18n="Certificate List">
                            Certificate List
                        </p>
                    </a>
                </li>
                @endif

                @if($is_admin == true || $is_staff == true)
                <li class="nav-item">
                    <a id="render_vessel_list" href="api/vesselList" class="nav-link render_page">
                        <i class="nav-icon fas fa-bell"></i>
                        <p data-i18n="Vessel List">
                            Vessel List
                        </p>
                    </a>
                </li>
                @endif

                @if($notification_read === true || $notification_write === true)
                <li class="nav-item">
                    <a href="api/notification" class="nav-link render_page">
                        <i class="nav-icon fas fa-bell"></i>
                        @if($notification_write === true)
                            <p data-i18n="Notification Management">
                                Notification Management
                            </p>
                        @else
                            <p data-i18n="Notifications">
                                Notifications
                            </p>
                        @endif
                    </a>
                </li>
                @endif

                @if($template_read === true || $template_write === true)
                <li class="nav-item has-treeview menu-open">
                    <a href="#" class="nav-link ">
                        <i class="nav-icon fa fa-th-list"></i>
                        <p>
                           <span data-i18n="Create Template"> Create Template </span>
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="form-builder/forms" class="render_page nav-link" id="form_template_builder">
                                <i class="nav-icon  fas fa-caret-right"></i>
                                <p data-i18n="Form Template List">Form Template List</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="api/certificatetemplatelist" id="sidebarcertificatelistmenu" class="render_page nav-link">
                                <i class="nav-icon  fas fa-caret-right"></i>
                                <p data-i18n="Certificate Template List">Certificate Template List</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif

                <!-- @if($calibration_certificate_read === true || $calibration_check_read === true)
                <li class="nav-item has-treeview menu-open">
                    <a href="#" class="nav-link ">
                        <i class="nav-icon fas fa-certificate"></i>
                        <p>
                            Calibration Certificate
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon  fas fa-caret-right"></i>
                                <p>Certificate List</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon  fas fa-caret-right"></i>
                                <p>Create Certificate</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif -->

                <!-- <li class="nav-item">
                    <a id="sidebar_device_list" href="api/deviceList" class="nav-link render_page">
                        <i class="nav-icon fas fa-bell"></i>
                        <p>
                         Devices List
                        </p>
                    </a>
                </li> -->
                
                @if($customers === true)
                <li class="nav-item">
                    <a id="render_registration_page" href="api/customers/deviceRegister" class="nav-link render_page">
                        <i class="nav-icon fas fa-bell"></i>
                        <p data-i18n="Device Registration">
                            Device Registration
                        </p>
                    </a>
                </li>
                @endif

                @if($report_read === true || $report_write === true)
                 <!-- <li class="nav-item">
                    <a id="render_vessel_devices_page" href="api/customers/deviceRegister" class="nav-link render_page">
                        <i class="nav-icon fas fa-bell"></i>
                        <p>
                            Register Vessel Devices
                        </p>
                    </a>
                </li> -->
                <li class="nav-item has-treeview menu-open"> <!-- menu-open -->
                    <a href="#" class="nav-link">
                        <i class="nav-icon far fa-list-alt"></i>
                        <p>
                            <span data-i18n="Reports">Reports</span>
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">  <!-- style="display: none;" -->
                        <li class="nav-item">
                            <a href="api/reports/completedcalibration" class="nav-link render_page">
                                <i class=" nav-icon  fas fa-caret-right"></i>
                                <p data-i18n="Completed Calibration">Completed Calibration</p>
                            </a>
                        </li>

                        @if($is_admin == true || $is_staff == true)
                        <li class="nav-item">
                            <a href="api/reports/expiredcalibration" class="nav-link render_page">
                                <i class="nav-icon  fas fa-caret-right"></i>
                                <p data-i18n="Expired Calibrations">Expired Calibrations</p>
                            </a>
                        </li>
                         <li class="nav-item">
                            <a href="api/reports/agentkits" id="Calibration_Kits_Used" class="nav-link render_page">
                                <i class="nav-icon  fas fa-caret-right"></i>
                                <p data-i18n="Calibration Kits Used">Calibration Kits Used</p>
                            </a>
                        </li>
                        @endif

                        <li class="nav-item">
                            <a href="api/reports/calibrationdue" class="nav-link render_page">
                                <i class="nav-icon  fas fa-caret-right"></i>
                                <p data-i18n="Re-calibrations Due">Re-calibrations Due</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="api/reports/customerlist" class="nav-link render_page">
                                <i class="nav-icon  fas fa-caret-right"></i>
                                <p data-i18n="End User List">End User List</p>
                            </a>
                        </li>

                        @if($is_admin == true || $is_staff == true)

                        <li class="nav-item">
                            <a href="api/reports/completedcalibrationscountbyregion" class="nav-link render_page">
                                <i class="nav-icon  fas fa-caret-right"></i>
                                <p data-i18n="Calibration by Region/Agent">Calibration by Region/Agent</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="api/reports/registeredproduct" class="nav-link render_page">
                                <i class="nav-icon  fas fa-caret-right"></i>
                                <p data-i18n="Registered Devices">Registered Devices</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="api/reports/registeredDevice" class="nav-link render_page">
                                <i class="nav-icon  fas fa-caret-right"></i>
                                <p data-i18n="Calibration Check Certificates">Calibration Check Certificates</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="api/reports/unregisterdevicelist" class="nav-link render_page">
                                <i class="nav-icon  fas fa-caret-right"></i>
                                <p data-i18n="Un-Registered Device List">Un-Registered Device List</p>
                            </a>
                        </li>

                        @endif

                    </ul>
                </li>
                @endif

               <!--  <li class="nav-item has-treeview ">
                    <a href="#" class="nav-link ">
                          <i class="nav-icon fas fa-cog"></i>
                          <p>
                              Settings
                          </p>
                    </a>
                </li> -->

               <!--  <li class="nav-item has-treeview ">
                    <a href="#" class="nav-link ">
                        <i class="nav-icon fas fa-info-circle"></i>
                        <p data-i18n="Help">
                            Help
                        </p>
                    </a>
                </li> -->


                 <!--  <li class="nav-item has-treeview menu-open">
                      <a href="#" class="nav-link active">
                          <i class="nav-icon fas fa-tachometer-alt"></i>
                          <p>
                              Dashboard
                              <i class="right fas fa-angle-left"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="./index2.html" class="nav-link ">
                                  <i class="nav-icon  fas fa-caret-right"></i>
                                  <p>Admin Portal</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="./index4.html" class="nav-link active">
                                  <i class="nav-icon  fas fa-caret-right"></i>
                                  <p>Admin Portal1</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="./index.html" class="nav-link ">
                                  <i class="nav-icon  fas fa-caret-right"></i>
                                  <p> Agent Portal</p>
                              </a>
                          </li>

                          <li class="nav-item">
                              <a href="./index3.html" class="nav-link">
                                  <i class="nav-icon  fas fa-caret-right"></i>
                                  <p>Customer Portal</p>
                              </a>
                          </li>
                      </ul>
                  </li> -->

                 <!--  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-box-open"></i>
                          <p>
                              Manage Devices
                          </p>
                      </a>
                  </li> -->

                 <!--  <li class="nav-item has-treeview">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-users"></i>
                          <p>
                              Manage Users
                              <i class="right fas fa-angle-left"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="#" class="nav-link active">
                                  <i class="nav-icon  fas fa-caret-right"></i>
                                  <p>Admin Staff</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="#" class="nav-link ">
                                  <i class="nav-icon  fas fa-caret-right"></i>
                                  <p>Agents</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="#" class="nav-link">
                                  <i class="nav-icon  fas fa-caret-right"></i>
                                  <p>Customers</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="#" class="nav-link">
                                  <i class="nav-icon  fas fa-caret-right"></i>
                                  <p>OEM</p>
                              </a>
                          </li>

                      </ul>
                  </li> -->


                  <!-- <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-unlock"></i>
                          <p>
                              Roles &amp; Permissions
                          </p>
                      </a>
                  </li> -->

                  <!-- <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fa fa-th-list"></i>
                          <p>
                              Create Template
                          </p>
                      </a>
                  </li> -->
                 <!--  <li class="nav-item has-treeview ">
                      <a href="#" class="nav-link ">
                          <i class="nav-icon fas fa-certificate"></i>
                          <p>
                              Calibration Certificate
                              <i class="right fas fa-angle-left"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="certificatelist.html" class="nav-link active">
                                  <i class="nav-icon  fas fa-caret-right"></i>
                                  <p>Certificate List</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="#" class="nav-link">
                                  <i class="nav-icon  fas fa-caret-right"></i>
                                  <p>Create Certificate</p>
                              </a>
                          </li>
                      </ul>
                  </li> -->
                  <!-- <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-bell"></i>
                          <p>
                              Event Notification
                          </p>
                      </a>
                  </li> -->
                  <!-- <li class="nav-item has-treeview ">
                      <a href="#" class="nav-link ">
                         
                          <i class="nav-icon fas fa-exchange-alt"></i>
                          <p>
                              Merlin Import
                              
                          </p>
                      </a>

                  </li> -->

                  <!-- <li class="nav-item has-treeview">
                      <a href="#" class="nav-link">
                          <i class="nav-icon far fa-list-alt"></i>
                          <p>
                              Reports
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview" style="display: none;">

                          <li class="nav-item">
                              <a href="#" class="nav-link">
                                  <i class=" nav-icon  fas fa-caret-right"></i>
                                  <p>Completed Calibration</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="#" class="nav-link">
                                  <i class="nav-icon  fas fa-caret-right"></i>
                                  <p>Expired Calibrations</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="#" class="nav-link">
                                  <i class="nav-icon  fas fa-caret-right"></i>
                                  <p>Re-calibrations Due</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="#" class="nav-link">
                                  <i class="nav-icon  fas fa-caret-right"></i>
                                  <p>Customer List</p>
                              </a>
                          </li>

                      </ul>
                  </li> -->

                <!-- <li class="nav-item has-treeview ">
                      <a href="#" class="nav-link ">
                          <i class="nav-icon fas fa-cog"></i>
                          <p>
                              Settings
                          </p>
                      </a>
                </li> -->

              </ul>
          </nav>
      </nav>
      <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
