<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>Support : HOKA Administration System</title>
   <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{asset('themes/frontend/assets/app_js/jquery.toaster.js')}}"></script> -->
    <style type="text/css">
        .header {
          font-family: Arial, Helvetica, sans-serif;
          font-size: 24px;
          font-style: normal;
        }
        .text1 {
          font-family: Arial, Helvetica, sans-serif;
          font-size: 12px;
          font-style: normal;
          font-weight: normal;
          color: #999999;
        }
        .subheader {
          font-family: Arial, Helvetica, sans-serif;
          font-size: 16px;
          font-weight: bold;
          color: #999999;
        }
        .description {
          font-family: Arial, Helvetica, sans-serif;
          font-size: 12px;
          font-style: normal;
          font-weight: bold;
        }
         .toaster .alert-danger {
            color: #a94442;
            background-color: #f2dede;
            border-color: #ebccd1;
        }

        .toaster  .alert-success {
            color: #3c763d;
            background-color: #dff0d8;
            border-color: #d6e9c6;
        }
    </style>

    <link rel="shortcut icon" href="{{asset('themes/images/hoka_logo.png')}}" />

    <script>
        
        function appStoreClick() {
            alert('Appen er kun tilgjengelig for Android. Vi lanserer for Apple snart');
            //$.toaster({ priority: 'danger', title: 'Error', message: 'Hi' });
        }

    </script>

</head>

<body>
<table width="43%"  border="0" align="center">
  <tr>
    <th colspan="6" scope="row"><span class="header">Supportside for Claims by Run </span></th>
  </tr>
  <tr>
    <th colspan="6" class="text1" scope="row">Dette er supportside for reklamasjon til Run AS. Denne l&oslash;sningen er kun for godkjente og registrerte forhandlere.<br>
Brukernavn opg passord i appen er det samme som du bruker ved etterbestilling p&aring; Runs forhandlerside.</th>
  </tr>
  <tr align="right">
    <th colspan="6" valign="middle" scope="row"><div align="left"></div></th>
  </tr>

  <tr align="center">
    <th colspan="6" valign="middle" scope="row">
        <span class="description">Last ned appen</span><br>
        <img src="{{asset('/themes/support/play_store.png')}}" width="269" height="100" style="cursor:pointer;">
        <img onclick="appStoreClick();" src="{{asset('/themes/support/app_store.png')}}" width="269" height="100" style="cursor:pointer;">
    </th>
  </tr>

  <tr align="center" valign="middle">
    <th colspan="6" class="subheader" scope="row">Glemt brukernavn eller passord?</th>
  </tr>
  <tr align="center" valign="middle">
    <th colspan="6" scope="row">
      <!-- <a href="mailto:claims@run.no" class="text1">Send oss en e-post</a> -->
      Kontakt oss på 62 52 03 20 
    </th>
  </tr>
  <tr>
    <th colspan="6" scope="row">&nbsp;</th>
  </tr>
  <tr>
    <th width="15%" scope="row"><img src="{{asset('/themes/support/1.png')}}" width="150" height="317" border="1"></th>
    <td width="15%"><img src="{{asset('/themes/support/2.png')}}" width="150" height="317" border="1"></td>
    <td width="15%"><img src="{{asset('/themes/support/3.png')}}" width="150" height="317" border="1"></td>
    <td width="15%"><img src="{{asset('/themes/support/4.png')}}" width="150" height="317" border="1"></td>
    <td width="21%"><img src="{{asset('/themes/support/32.png')}}" width="150" height="317" border="1"></td>
    <td width="19%"><img src="{{asset('/themes/support/31.png')}}" width="150" height="317" border="1"></td>
  </tr>
  <tr align="center" valign="top">
    <th scope="row">&nbsp;</th>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr align="center" valign="top" class="description">
    <th scope="row">Logg inn med ditt brukernavn og passord fra Run AS</th>
    <td>Klikk p&aring; &quot;Registrer reklamasjon&quot; for &aring; starte</td>
    <td>Finn frem skoen, og ha pl&oslash;sen godt synlig.</td>
    <td>Bilde av merkelappen m&aring; v&aelig;re godt leselig.</td>
    <td>Hvis informasjonen er godt synlig vil alle data v&aelig;re riktig fylt inn.</td>
    <td>Appen sjekker mot tilgjengelige modeller.</td>
  </tr>
  <tr align="center" valign="top">
    <th scope="row">&nbsp;</th>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr align="center" valign="top" class="text1">
    <th scope="row"><p>      Dette er det samme brukernavn og passord du logger inn for bestilling </p>
    </th>
    <td>Appen vil n&aring; guide deg igjennom n&oslash;dvendige steg for at vi skal ha tilstrekkelig informasjon </td>
    <td><p>Kameraet m&aring; kunne ta et tydelig bilde av merkelappen p&aring; innsiden </p>
      <p>Klikk p&aring; kamera eller bilde for &aring; ta bilde </p></td>
    <td> Denne vil skannes og leses av appen. Klikk p&aring; avhukning/godkjenn </td>
    <td>      Du m&aring;
    angi st&oslash;rrelse </td>
    <td><p> Hvis det er avlest uriktig informasjon, vil du bli varslet om dette og du kan skrive inn data selv.</p>
    </td>
  </tr>
  <tr>
    <th scope="row">&nbsp;</th>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th scope="row"><img src="{{asset('/themes/support/5.png')}}" width="150" height="317" border="1"></th>
    <td><img src="{{asset('/themes/support/6.png')}}" width="150" height="317" border="1"></td>
    <td><img src="{{asset('/themes/support/8.png')}}" width="150" height="317" border="1"></td>
    <td><img src="{{asset('/themes/support/9.png')}}" width="150" height="317" border="1"></td>
    <td><img src="{{asset('/themes/support/92.png')}}" width="150" height="317" border="1"></td>
    <td><img src="{{asset('/themes/support/93.png')}}" width="150" height="317" border="1"></td>
  </tr>
  <tr align="center" valign="top" class="description">
    <th scope="row">Ta bilde av skaden du reklamerer p&aring;. </th>
    <td>Bildet lagres i appen og sendes med som dokumentasjon.</td>
    <td>Appen guider deg gjennom n&oslash;dvendige steg for dokumentasjon</td>
    <td>Helhetsbilde er siste p&aring;krevde bilde er.</td>
    <td>Vi m&aring; vite kj&oslash;psdato for skoen. </td>
    <td>N&oslash;dvendig informasjon sendes inn til oss. Husk &aring; huke av.</td>
  </tr>
  <tr align="center" valign="top">
    <th scope="row">&nbsp;</th>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr align="center" valign="top" class="text1">
    <th scope="row">    Dette er n&oslash;dvendig for &aring; kunne vurdere reklamasjonen</th>
    <td> Dette gjelder alle bildene du tar </td>
    <td><p>.</p>
    </td>
    <td><p>      Du kan legge ved flere bilder hvis du mener det er n&oslash;dvendig.</p>
    </td>
    <td><p>Du kan ogs&aring; angi egen referanse under dette punktet </p>
    </td>
    <td>    Du vil f&aring; et eget referansenummer per skadesak. </td>
  </tr>
  <tr>
    <th scope="row">&nbsp;</th>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th colspan="6" class="text1" scope="row">Run AS<br>
      2318 Hamar<br>
      Hertzbergsgate 34<br>
    Org.nr: 999 132 790</th>
  </tr>
</table>
</body>
</html>
