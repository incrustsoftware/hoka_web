<!doctype html>
<html lang="en">
<head>
<title>Complaint Details</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="{{asset('themes/frontend/assets/css/jquery-ui.css')}}">
<link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/ti-icons/css/themify-icons.css')}}">
<link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/css/vendor.bundle.base.css')}}">
<link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/font-awesome/css/font-awesome.min.css')}}">
<!-- endinject -->
<link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/simple-line-icons/css/simple-line-icons.css')}}">
<!-- Plugin css for this page -->
<!-- Plugin css for this page -->
<link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}">
<!-- End plugin css for this page -->

<!-- End plugin css for this page -->
<link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/owl-carousel-2/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/owl-carousel-2/owl.theme.default.min.css')}}">
<!-- inject:css -->
<link rel="stylesheet" href="{{asset('themes/frontend/assets/css/horizontal-layout-light/style.css')}}">
<!-- endinject -->
<!-- <link rel="shortcut icon" href="{{asset('themes/frontend/assets/images/favicon.png')}}" /> -->
<link rel="shortcut icon" href="{{asset('themes/images/hoka_logo.png')}}" />

<!-- <script src="{{asset('themes/frontend/assets/js/jquery-3.3.1.js')}}"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<style type="text/css">
    .sidebar .nav .nav-item.active > .nav-link i, .sidebar .nav .nav-item.active > .nav-link .menu-title, .sidebar .nav .nav-item.active > .nav-link .menu-arrow {
        color: #fff;
    }
    .toaster .alert-danger {
        color: #a94442;
        background-color: #f2dede;
        border-color: #ebccd1;
    }

    .toaster  .alert-success {
        color: #3c763d;
        background-color: #dff0d8;
        border-color: #d6e9c6;
    }
</style>
</head>
<body>
    <div class="container-scroller">

        <!-- partial:partials/_navbar.html -->
        <div class="horizontal-menu">
            <nav class="navbar top-navbar col-lg-12 col-12 p-0 header_background">
                <div class="container header_new">
                    <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                        <a class="logo_image" href="javascript:void(0);"><img src="{{asset('themes/images/hoka_logo.png')}}" alt="logo"/></a>
                    </div>

                    
                    <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
                        <ul class="navbar-nav navbar-nav-right firstHeadNavRight">

                        </ul>
                    </div>
                </div>
            </nav>
        </div>

        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <div class="main-panel_old">
                <section class="container dashboard_page">
                    

                @if(isset($complaint))
                    <div class="card" style="margin-top: 50px;">
                        <div class="card-body">

                            <div class="row" >
                                <div class="col-12">
                                    <h4 style="margin-bottom:20px;">Complaint Details</h4>
                                </div>
                            </div>

                            <div class="row text-grid">
                                <div class="col-4">
                                    <div class="text-block">
                                        <div>{{$complaint['gender']}} {{$complaint['model_name']}} {{$complaint['version']}}</div>
                                        <div>{{$complaint['model_number']}} {{$complaint['color_code']}}</div>
                                        <div>{{$complaint['manufacturer_code']}}</div>
                                        <div>{{$complaint['size']}}</div>
                                    </div>
                                </div>

                                <div class="col-4">
                                    <div class="text-block">
                                        <div>{{$complaint['dealer_name']}}</div>
                                        <div>{{$complaint['address1']}}</div>
                                        <div>{{$complaint['zip_code']}} {{$complaint['city']}}</div>
                                    </div>
                                </div>

                                <div class="col-4">
                                    <div class="text-block">
                                        <div>e-post: {{$complaint['complaint_email']}}</div>
                                        <div>status: {!!html_entity_decode($status)!!}</div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="complaint_slider owl-carousel owl-theme" id="complaint_slider">

                                    @foreach ($complaint['images'] as $images)
                                        <div class="row item">
                                            <div class="col-xs-12 col-md-3"></div>
                                            <div class="col-xs-12 col-md-6">
                                                <img class="slider-image" src="{{asset($images['s3_product_image'])}}">
                                            </div>
                                            <div class="col-xs-12 col-md-3"></div>
                                        </div>
                                    @endforeach
                
                                <!-- <div class="row">
                                        <div class="col-3"></div>
                                        <div class="col-6">
                                            <img class="slider-image" src="{{asset('uploads/product_complaint_images/image1.jpg')}}">
                                        </div>
                                        <div class="col-3"></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-3"></div>
                                        <div class="col-6">
                                            <img class="slider-image" src="{{asset('uploads/product_complaint_images/image2.jpg')}}">
                                        </div>
                                        <div class="col-3"></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-3"></div>
                                        <div class="col-6">
                                            <img class="slider-image" src="{{asset('uploads/product_complaint_images/image3.jpg')}}">
                                        </div>
                                        <div class="col-3"></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-3"></div>
                                        <div class="col-6">
                                            <img class="slider-image" src="{{asset('uploads/product_complaint_images/image4.jpg')}}">
                                        </div>
                                        <div class="col-3"></div>
                                    </div> -->
                                      
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                @else
                    <div class="card item" style="margin-top: 50px;">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <span style="font-size:25px;">{{$message}}</span>
                                </div>
                            </div>
                        </div>
                    </div>    
                @endif

                </section>
            </div>
          <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->

    <script src="{{asset('themes/frontend/assets/js/jquery-ui.js')}}"></script>
    <script src="{{asset('themes/frontend/assets/vendors/js/vendor.bundle.base.js')}}"></script>

    <script src="{{asset('themes/frontend/assets/vendors/owl-carousel-2/owl.carousel.min.js')}}"></script>
    <script src="{{asset('themes/frontend/assets/js/hoverable-collapse.js')}}"></script>
    <script src="{{asset('themes/frontend/assets/js/owl-carousel.js')}}"></script>
    <script src="{{asset('themes/frontend/assets/vendors/moment/moment.min.js')}}"></script>
    <script src="{{asset('themes/frontend/assets/js/owl-carousel.js')}}"></script>
    <script src="{{asset('themes/frontend/assets/app_js/loadingoverlay.js')}}"></script>
    <script src="{{asset('themes/frontend/assets/app_js/jquery.toaster.js')}}"></script>
    <script src="{{asset('themes/frontend/assets/app_js/jquery.exzoom.js')}}"></script>
    <!-- End custom js for this page-->

    <script type="text/javascript">
        
        $(document).ready(function(){
            $('.complaint_slider').owlCarousel({
              loop: false,
              items: 1,
              margin: 10,
              nav: true,
              autoplay: false,
              autoplayTimeout: 8500,
              //navigationText : ["prev","next"],
              navText: ["<i class='ti-angle-left'></i>", "<i class='ti-angle-right'></i>"],
              responsive: {
                    600: {
                      items: 1
                    }
                }
            });
        });

    </script>

</body>
</html>
