@extends('layout.app')
@section('title', 'Dashboard')
@section('content')

<!-- Custom css for this page-->
<link rel="stylesheet" href="{{asset('themes/frontend/assets/app_css/dashboard.css')}}">
<!-- <link rel="stylesheet" href="{{asset('themes/frontend/assets/app_css/drift-basic.css')}}"> -->
<link rel="stylesheet" href="{{asset('themes/frontend/assets/app_css/jquery.magnify.css')}}">

<section class="entry_form_content_display container dashboard_page">

    <!-- Date range picker for new complaints -->
    <div class="custom_card m_t_10">
        <div class="card-body">
            <div class="row">
                <div class="col-xs-12 col-md-2">
                    <label for="date_range_picker" class="m_t_15">New Complaints</label>
                    <!-- Nye raklamasjoner -->
                </div>

                <div class="col-xs-12 col-md-6 stretch-card" id="date_range_picker">
                    <div class="input-group new_complaint_picker input-daterange d-flex align-items-center">

                        <label class="input-group-btn" for="from_date">
                            <span class="btn btn-default daterange_btn">
                                <span class="fa fa-calendar fa_celender_icon" aria-hidden="true"></span>
                            </span>
                        </label>
    

                        <input id="from_date" data-date-format="dd-mm-yyyy" type="text" class="form-control" placeholder="From date" autocomplete="off">

                        <div class="daterange_devider"></div>

                        <label class="input-group-btn" for="to_date">
                            <span class="btn btn-default daterange_btn">
                                <span class="fa fa-calendar fa_celender_icon" aria-hidden="true"></span>
                            </span>
                        </label>

                        <input id="to_date" data-date-format="dd-mm-yyyy" type="text" class="form-control" placeholder="To date" autocomplete="off">
                    </div>
                </div>

                <div class="col-xs-12 col-md-4 text-right" id="gererate_complaint_report">
                    <div class="generate_report1_grid ">
                        <button type="button" class="btn btn-primary btn-fw custom_button" onclick="generateNewComplaintReport();" title="Generate Report">Generate report</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Complaints slider -->
    <div class="custom_card m_t_10" id="complaint_slider_box">
        <div class="card-body">
            <div class="hide text-center" id="no_complaints_found" style="opacity: 0.8;">
                Complaints not found
            </div>
            <div class="complaint_slider owl-carousel owl-theme" id="complaint_slider">
                <!-- 
                <div onclick="viewProductDetails(1, this);" title="View Complaint Details">
                    <div class="row">
                        <div class="col-12">
                            <div class="row image-grid">
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image1.jpg')}}">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image2.jpg')}}">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image3.jpg')}}">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image4.jpg')}}">
                                </div>
                            </div>
                            <div class="row text-grid">
                                <div class="col-12">
                                    <div class="text-block">
                                        <div>M Napali</div>
                                        <div>1091609 BIAG</div>
                                        <div>F27218E</div>
                                    </div>
                                    <div class="text-block">
                                        <div>Naygard Sport AS</div>
                                        <div>Lilleasgata 4</div>
                                        <div>3340 AMOT</div>
                                    </div>
                                    <div class="text-block">
                                        <div>e-post: nygard@sport1.no</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div onclick="viewProductDetails(2, this);" title="View Complaint Details">
                    <div class="row">
                        <div class="col-12">
                            <div class="row image-grid">
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image1.jpg')}}">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image2.jpg')}}">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image3.jpg')}}">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image4.jpg')}}">
                                </div>
                            </div>
                            <div class="row text-grid">
                                <div class="col-12">
                                    <div class="text-block">
                                        <div>M Napali</div>
                                        <div>1091609 BIAG</div>
                                        <div>F27218E</div>
                                    </div>
                                    <div class="text-block">
                                        <div>Naygard Sport AS</div>
                                        <div>Lilleasgata 4</div>
                                        <div>3340 AMOT</div>
                                    </div>
                                    <div class="text-block">
                                        <div>e-post: nygard@sport1.no</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div onclick="viewProductDetails(3, this);" title="View Complaint Details">
                    <div class="row">
                        <div class="col-12">
                            <div class="row image-grid">
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image1.jpg')}}">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image2.jpg')}}">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image3.jpg')}}">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image4.jpg')}}">
                                </div>
                            </div>
                            <div class="row text-grid">
                                <div class="col-12">
                                    <div class="text-block">
                                        <div>M Napali</div>
                                        <div>1091609 BIAG</div>
                                        <div>F27218E</div>
                                    </div>
                                    <div class="text-block">
                                        <div>Naygard Sport AS</div>
                                        <div>Lilleasgata 4</div>
                                        <div>3340 AMOT</div>
                                    </div>
                                    <div class="text-block">
                                        <div>e-post: nygard@sport1.no</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div onclick="viewProductDetails(4, this);" title="View Complaint Details">
                    <div class="row">
                        <div class="col-12">
                            <div class="row image-grid">
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image1.jpg')}}">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image2.jpg')}}">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image3.jpg')}}">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image4.jpg')}}">
                                </div>
                            </div>
                            <div class="row text-grid">
                                <div class="col-12">
                                    <div class="text-block">
                                        <div>M Napali</div>
                                        <div>1091609 BIAG</div>
                                        <div>F27218E</div>
                                    </div>
                                    <div class="text-block">
                                        <div>Naygard Sport AS</div>
                                        <div>Lilleasgata 4</div>
                                        <div>3340 AMOT</div>
                                    </div>
                                    <div class="text-block">
                                        <div>e-post: nygard@sport1.no</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div onclick="viewProductDetails(5, this);" title="View Complaint Details">
                    <div class="row">
                        <div class="col-12">
                            <div class="row image-grid">
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image1.jpg')}}">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image2.jpg')}}">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image3.jpg')}}">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image4.jpg')}}">
                                </div>
                            </div>
                            <div class="row text-grid">
                                <div class="col-12">
                                    <div class="text-block">
                                        <div>M Napali</div>
                                        <div>1091609 BIAG</div>
                                        <div>F27218E</div>
                                    </div>
                                    <div class="text-block">
                                        <div>Naygard Sport AS</div>
                                        <div>Lilleasgata 4</div>
                                        <div>3340 AMOT</div>
                                    </div>
                                    <div class="text-block">
                                        <div>e-post: nygard@sport1.no</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
              
            </div>
        </div>
    </div>

    <!-- Complaint graph -->
    <div class="custom_card m_t_10">
        <div class="card-body">
            <!-- Graph date range -->
            <div class="row m_b_35">
                <div class="col-xs-12 col-md-2">
                    <label for="date_range_picker" class="m_t_15">Complaint Graph</label>
                </div>

                <div class="col-xs-12 col-md-6 stretch-card" id="date_range_picker">
                    <div class="input-group graph_date_range_picker input-daterange d-flex align-items-center">
                        <label class="input-group-btn" for="graph_from_date">
                            <span class="btn btn-default daterange_btn">
                                <span class="fa fa-calendar fa_celender_icon" aria-hidden="true"></span>
                            </span>
                        </label>
                        <input id="graph_from_date" data-date-format="dd-mm-yyyy"  type="text" class="form-control" placeholder="From date" autocomplete="off">

                        <div class="daterange_devider"></div>

                        <label class="input-group-btn" for="graph_to_date">
                            <span class="btn btn-default daterange_btn">
                                <span class="fa fa-calendar fa_celender_icon" aria-hidden="true"></span>
                            </span>
                        </label>
                        <input id="graph_to_date" data-date-format="dd-mm-yyyy"  type="text" class="form-control" placeholder="To date" autocomplete="off">


                    </div>
                </div>

                <div class="col-xs-12 col-md-4 text-right" id="view_graph_button">
                    <div class="show_complaint_graph ">
                        <button type="button" class="btn btn-primary btn-fw custom_button" onclick="getMultiLineComplaintGraph();" title="View Graph">View Graph</button>
                    </div>
                </div>
            </div>

            <!-- Graph block -->
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card" id="complaint_graph">
                    <!-- <h4 class="card-title">Multi line chart</h4> -->
                    <canvas id="linechart-multi"></canvas>
                </div>
            </div>
        </div>
    </div>

    <!-- Close cases report -->
    <div class="custom_card m_t_10">
        <div class="card-body">
            <!-- Date range picker for close cases-->
            <div class="row">
                <div class="col-xs-6 col-md-2">
                    <label for="close_cases_date_range_picker" class="m_t_15">Closed Cases</label>
                    <!-- Avsluttede saker -->
                </div>

                <div class="col-xs-6 col-md-6 stretch-card" id="close_cases_date_range_picker">
                    <div class="input-group close_cases_picker input-daterange d-flex align-items-center">

                        <label class="input-group-btn" for="close_cases_from_date">
                            <span class="btn btn-default daterange_btn">
                                <span class="fa fa-calendar fa_celender_icon" aria-hidden="true"></span>
                            </span>
                        </label>
                        <input id="close_cases_from_date" data-date-format="dd-mm-yyyy" type="text" class="form-control" value="2012-04-05" placeholder="From date" autocomplete="off">

                        <div class="daterange_devider"></div>

                        <label class="input-group-btn" for="close_cases_to_date">
                            <span class="btn btn-default daterange_btn">
                                <span class="fa fa-calendar fa_celender_icon" aria-hidden="true"></span>
                            </span>
                        </label>
                        <input id="close_cases_to_date" data-date-format="dd-mm-yyyy" type="text" class="form-control" value="2012-04-19" placeholder="To date" autocomplete="off">
                    </div>
                </div>

                <div class="col-xs-6 col-md-3">
                    <div class="generate_report2_grid text-center" id="generate_close_report_button">
                        <button type="button" class="btn btn-primary btn-fw custom_button" onclick="generateCloseCasesReport();" title="Generate Report">Generate report</button>

                         <i class="fa fa-file-pdf-o pdf_icon d-md-none d-lg-none d-xl-none" onclick="closeCasesPDF();" aria-hidden="true" title="Generate Report In Pdf Format" style="    margin-left: 50px;position: absolute;"></i>
                    </div>
                </div>

                <div class="col-xs-6 col-md-1 text-right d-none d-sm-none d-md-block d-lg-block d-xl-block" id="generate_close_pdf_button">
                    <i class="fa fa-file-pdf-o pdf_icon" onclick="closeCasesPDF();" aria-hidden="true" title="Generate Report In Pdf Format"></i>
                </div>
            </div>

            <!-- Datatable for close cases report -->
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive m_t_35">
                        <!-- <label class="badge badge-success">Closed</label>
                        <label class="badge badge-info">On hold</label>
                        <label class="badge badge-danger">Pending</label> -->
                        <table id="close_cases" class="table">
                            <thead>
                                <tr>
                                    <th>Complaint ID</th>
                                    <th>Customer ID</th>
                                    <th>Forhandler</th>
                                    <th>Link til reklamasjonen</th>
                                    <th>Modell</th>
                                    <th>Produksjon</th>
                                    <th>Reason</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>2300900</td>
                                    <td>101511</td>
                                    <td>Sport 1 Vinje</td>
                                    <td><a target="__blank" href="https://app.hoka.no/reklamasjon&returnID=101511">https://app.hoka.no/reklamasjon&returnID=101511</td>
                                    <td>1091609</td>
                                   
                                    <td>F27218E1</td>
                                    <td>Hi</td>
                                    <td>
                                        <i class="fa fa-check fa_right_check" aria-hidden="true"></i>
                                        <!-- <i class="fa fa-times" aria-hidden="true"></i> -->
                                    </td>
                                </tr>

                                <tr>
                                    <td>2300900</td>
                                    <td>101512</td>
                                    <td>Hank Sport</td>
                                    <td><a target="__blank" href="https://app.hoka.no/reklamasjon&returnID=101512">https://app.hoka.no/reklamasjon&returnID=101512</td>
                                    <td>5453948</td>
                                   
                                    <td>F28985G2</td>
                                    <td>Hi</td>
                                    <td>
                                        <i class="fa fa-check fa_right_check" aria-hidden="true"></i>
                                    </td>
                                </tr>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Popup for view complaint data -->
    <div class="modal fade" id="complaint_modal_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <i id="popup_back_button" title="back" class="fa fa-arrow-circle-left radio_button_back" onclick="showRadioGrid();"></i>

                    <i id="not_accept_back_button" title="back" class="fa fa-arrow-circle-left radio_button_back hide" onclick="notAcceptBackButton();"></i>

                    <h5 class="modal-title" id="complaint_number"> 102547</h5>
                    <!-- Reklamasjon -->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0px;">

                    <input type="hidden" id="_popup_complaint_id" value="">
                    <input type="hidden" id="_popup_customer_id" value="">
                    <input type="hidden" id="_company_name" value="">
                    <input type="hidden" id="_complaint_number" value="">
                    <input type="hidden" id="_complaint_received_date" value="">

                    <div class="row text-grid" style="padding-left: 0px;padding-bottom: 10px;">
                        <div class="col-6" id="complaint_popup_first_block">
                            <div class="text-block">
                                <div><label class="label_margin_bottom_0px">Forhandler</label></div>
                                <div id="popup_forhandler_name">Naygard Sport AS</div>
                            </div>
                        </div>
                        <div class="col-5">
                            <div class="text-block" id="popup_product_complaint_details">
                                <!-- <div>M Napali</div>
                                <div>1091609 BIAG</div>
                                <div>F27218E</div>
                                <div>9.5</div> -->
                            </div>
                        </div>
                        <div class="col-1">
                            <div id="edit_icon_block">
                                <i class="fa fa-edit" title="Edit Complaint"></i>
                            </div>
                        </div>
                    </div>
                    
                   <!--  <div class="row">
                        <div class="col-12">
                            <div class="exzoomWrapper">
                               
                            </div>
                        </div>
                    </div> -->

                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="row image-grid" style="padding:0px;" id="popup_complaint_images">
                               <!--  <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image1.jpg')}}">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image2.jpg')}}">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image3.jpg')}}">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="{{asset('uploads/product_complaint_images/image4.jpg')}}">
                                </div> -->
                            </div>
                        </div>
                    </div>

                    <div id="zoom_view">
                    </div>

                    <div class="row m_t_5 hide" id="radio_button_grid">
                        <div class="col-md-12">
                            <div class="form-group" id="invoices_data">
                                <!-- <div class="form-check">
                                    <label class="form-check-label radio_label">
                                        <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios1" value="">
                                        InviceNo:91021, Line ID:64866, OrderNo:49257, ArticleNo:FW1091609BIAG09, NetPrice:999, Discount1:48.5
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label radio_label">
                                        <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios2" value="option2">
                                        InviceNo:91021, Line ID:64866, OrderNo:49257, ArticleNo:FW1091609BIAG09, NetPrice:999, Discount1:48.5
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label radio_label">
                                        <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios3" value="option2">
                                        InviceNo:91021, Line ID:64866, OrderNo:49257, ArticleNo:FW1091609BIAG09, NetPrice:999, Discount1:48.5
                                    </label>
                                </div> -->
                            </div>
                        </div>
                    </div>

                    <div class="row m_t_15 hide" id="accepted_message_box">
                        <div class="col-md-12">
                            <textarea class="form-control" id="complaint_messagebox" rows="5" style="padding: 10px;">Hei,&#13;&#10;&#13;&#10;vi har behandlet reklamasjonssaken og vil kredittere order 49257&#13;&#10;&#13;&#10;med vennlig hilsen</textarea>
                        </div>
                    </div>

                    <div class="row m_t_15 hide" id="notaccepted_message_box">
                        <div class="col-md-12">
                            <textarea class="form-control" id="complaint_messagebox2" rows="9" style="padding: 10px;">Hei,&#13;&#10;&#13;&#10;vi kan ikke behandle klagesaken.&#13;&#10;&#13;&#10;med beste ønsker</textarea>
                        </div>
                    </div>

                </div>
                <div class="modal-footer" style="display: initial;justify-content: initial;">
                    <div class="row">
                        <div class="col-md-12 text-right" id="complaint_popup_footer"> 
                            <button id="accepted_btn" type="button" class="btn btn-success btn-rounded btn-sm" title="Accepted" onclick="accepted(1);">Accepted<i class="ti-arrow-circle-right ml-1 arrow_icon"></i></button> <!-- submitInvoiceRequest -->
                            <button id="not_accepted_btn" type="button" class="btn btn-danger btn-rounded btn-sm" title="Not Accepted" onclick="notAccepted(1);">Not Accepted<i class="ti-arrow-circle-right ml-1 arrow_icon"></i></button>
                        </div>

                        <div class="col-md-12 text-right" id="complaint_popup_footer2" style="margin-top: 10px;"> 
                            <button id="accepted_btn2" type="button" class="btn btn-success btn-rounded btn-sm" title="Accepted" onclick="accepted(2);">Accepted <i class="ti-arrow-circle-right ml-1 arrow_icon"></i><div style="text-align: left;">Sport 1</div></button> <!-- submitInvoiceRequest -->
                            <button id="not_accepted_btn2" type="button" class="btn btn-danger btn-rounded btn-sm" title="Not Accepted" onclick="notAccepted(2);">Not Accepted<i class="ti-arrow-circle-right ml-1 arrow_icon"></i><div style="text-align: left;">Sport 1</div></button>
                        </div>

                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <!-- Edit complaint data -->
     <!-- Popup for view complaint data -->
    <div class="modal fade" id="update_complaint" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h5 class="modal-title">Edit Complaint</h5>
                    <!-- Reklamasjon -->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="edit_complaint_id" value="0">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group required">
                                <label for="model_name_input">Model Name</label>
                                <input type="text" class="form-control" id="model_name_input" placeholder="Model Name" autocomplete="off">
                            </div>
                            <div class="form-group required">
                                <label for="model_number_input">Model Number</label>
                                <input type="text" class="form-control" id="model_number_input" placeholder="Model Number" autocomplete="off">
                            </div>
                            <div class="form-group required">
                                <label for="color_code_input">Color Code</label>
                                <input type="text" class="form-control" id="color_code_input" placeholder="Color Code" autocomplete="off">
                            </div>
                            <div class="form-group required">
                                <label for="manufacturer_code_input">Manufacturer Code</label>
                                <input type="text" class="form-control" id="manufacturer_code_input" placeholder="Manufacturer Code" autocomplete="off">
                            </div>
                           <!--  <div class="form-group required">
                                <label for="claim_type">Claim Type</label>
                                 <select class="form-control" id="claim_type">
                                    <option value="">Select claim type</option>
                                    <option value="CREDITED">CREDITED</option>
                                    <option value="NEWSHOES">NEWSHOES</option>
                                </select>
                            </div> -->

                             <div class="form-group required">
                                <label for="size_input">Size</label>
                                <input type="text" class="form-control" id="size_input" placeholder="Size" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group required">
                                <label for="version_input">Claims Reason</label>
                                <select class="form-control" id="claims_reason">
                                    <option>Select claims reason</option>
                                </select>
                            </div>
                            <div class="form-group required">
                                <label for="version_input">Version</label>
                                <input type="text" class="form-control" id="version_input" placeholder="Version" autocomplete="off">
                            </div>
                            <div class="form-group required">
                                <label for="gender_input">Gender</label>
                                <input type="text" class="form-control" id="gender_input" placeholder="Gender" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="customer_reference_number">Customer Reference Number</label>
                                <input type="text" class="form-control" id="customer_reference_number" placeholder="Customer Reference Number" autocomplete="off">
                            </div>
                            <div class="form-group required">
                                <label for="gender_input">Comment from customer (Free Text)</label>
                                <textarea type="text" rows="4" class="form-control" id="free_text_input" placeholder="Comment from customer" autocomplete="off"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="display: initial;justify-content: initial;">
                    <div class="row">
                        <div class="col-md-12 text-center"> 
                            <button type="button" class="btn btn-success btn-rounded btn-sm" title="Save" style="width: 100px;" onclick="updateComplaintData();">Save</button> 
                            <button type="button" class="btn btn-danger btn-rounded btn-sm" title="Cancel" data-dismiss="modal" style="width: 100px;">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--modal body-->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md-custom" role="document">
            <div class="modal-content">
                <!-- <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" title="Close">
                    <span class="glyphicon glyphicon-remove"></span></button>
                </div> -->
                <div class="modal-body">

                    <!-- carousel body-->
                    <div id="myGallery" class="carousel slide" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner">

                            <div class="item active">
                                <img src="http://hoka.incrustsoftware.com/uploads/product_complaint_images/f574146eaec740fbd1a9b67809352c98.png" alt="item1">
                                <div class="carousel-caption"></div>
                            </div>



                            <div class="item">
                                <img src="http://hoka.incrustsoftware.com/uploads/product_complaint_images/2d45387823562687719b5700b369d02c.png" alt="item2">
                                <div class="carousel-caption">
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- Previous and Next buttons-->
                    <a class="left carousel-control" href="#myGallery" role="button" data-slide="prev">
                    <i class='fa fa-angle-left'></i></a> 
                    <a class="right carousel-control" href="#myGallery" role="button" data-slide="next">
                    <i class='fa fa-angle-right'></i></a>
                </div>

            </div>
        </div>
    </div>
                   
</section>

<!-- Custom js for this page-->
<!-- <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script> -->
<script src="{{asset('themes/frontend/assets/app_js/dashboard.js')}}"></script>
<script src="{{asset('themes/frontend/assets/app_js/FileSaver.js') }}"></script>
<script src="{{asset('themes/frontend/assets/app_js/pdfmake.min.js') }}"></script>
<script src="{{asset('themes/frontend/assets/app_js/vfs_fonts.js') }}"></script>
<script src="{{asset('themes/frontend/assets/app_js/js-image-zoom.js') }}"></script>
<script src="{{asset('themes/frontend/assets/app_js/jquery.magnify.js') }}"></script>
<!-- <script src="{{asset('themes/frontend/assets/app_js/Drift.js') }}"></script> -->
<!-- <script src="https://unpkg.com/js-image-zoom/js-image-zoom.js"></script> -->



@endsection