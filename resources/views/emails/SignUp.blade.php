@component('mail::message')
# Welcome

Dear {{$data['name']}},

Your new {{ config('app.name') }} Account has been created. Welcome to the {{ config('app.name') }} community!

From now on, Please log in to your account using your email address and your password. We have sent an email with login link to your email address.In order to complete the login process, Please use below details - 

Email : {{$data['email']}}<br>
Password : {{$data['password']}}

@component('mail::button', ['url' => $data['url']])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
