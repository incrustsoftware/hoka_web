@component('mail::message')
# Forgot password

Dear {{$data['name']}},

We have sent an email with login link to your email address.In order to complete the forgot password process, Please use below details - 

New Password : {{$data['password']}}

@component('mail::button', ['url' => $data['url']])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
