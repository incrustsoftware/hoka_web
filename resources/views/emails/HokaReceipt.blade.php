@component('mail::message')
# Complaint Receipt

Hei,

Det er kommet en reklamasjon fra {{$data['dealer_name']}} med reklamasjonsnummer {{$data['complaint_number']}} på følgende sko:
{{$data['model_name']}}, med varenummer {{$data['model_number']}} kjøpt den {{$data['purchase_date']}}.


Med vennlig hilsen


Reklamasjonssystemet for Run AS

Thanks,<br>
{{ config('app.name') }}
@endcomponent
