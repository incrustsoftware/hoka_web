@component('mail::message')
# Complaint Not Accepted

{{$data['message']}}.

@component('mail::button', ['url' => $data['url']])
View Complaint
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
