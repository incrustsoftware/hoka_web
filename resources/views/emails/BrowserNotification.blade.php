@component('mail::message')
# Notification Alert

Dear {{$data['name']}},

{{$data['message']}}.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
