@component('mail::message')
# Complaint Receipt

Hei, {{$data['dealer_name']}}

Vi bekrefter å ha mottatt reklamasjon med reklamasjonsnummer {{$data['complaint_number']}} for følgende sko:
{{$data['model_name']}}
Med varenummer {{$data['model_number']}}
Kjøpt av kunden den {{$data['purchase_date']}}.

I tråd med våre retningslinjer vil vi gå igjennom denne, og vil oppdatere deg etter behandling.

<b>OBS!</b> Dette er kun en kvittering på mottak av reklamasjon.

Med vennlig hilsen

Thanks,<br>
{{ config('app.name') }}
@endcomponent
