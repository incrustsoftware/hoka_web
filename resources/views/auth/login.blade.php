<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>HOKA Administration System</title>
  <!-- plugins:css -->

  <link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/ti-icons/css/themify-icons.css')}}">
  <link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/css/vendor.bundle.base.css')}}">
  <link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/font-awesome/css/font-awesome.min.css')}}">

  <!-- endinject -->
  <!-- Plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
    <link rel="stylesheet" href="{{asset('themes/frontend/assets/css/horizontal-layout-light/style.css')}}">
  <!-- endinject -->
   <!--  <link rel="stylesheet" href="{{asset('themes/frontend/assets/images/favicon.png')}}"> -->
   <link rel="shortcut icon" href="{{asset('themes/images/hoka_logo.png')}}" />
  <style type="text/css">
    .field-icon {
  float: right;
  margin-left: -25px;
  margin-top: -25px;
  position: relative;
  z-index: 2;
}

.fa-fw {
    width: 1.28571429em;
    text-align: center;
}

.fa {
    display: inline-block;
    font: normal normal normal 14px/1 FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
    
  </style>
</head>

<body>
<section>
  
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="main-panel" style="background-image:url({{asset('themes/images/login-bg-img.jpg')}})">
        <div class ="logo_image">
          <a href="#">
            <img src="{{asset('/themes/images/hoka_logo.png')}}">
          </a>
        </div>
        <div class="content-wrapper d-flex align-items-center auth px-0">
            
            <input type="hidden" id="base_path" value="{{ url('/') }}">
            <input type="hidden" id="dashboard_url" value="{{ url('/dashboard') }}">
          <div class="row w-100 mx-0">
            <div class="col-lg-5 mx-auto">
               
              <div class="auth-form-light text-left py-5 px-4 px-sm-5" >
                <h5>Admin Innlogging</h5>
                <form class="pt-3" action="{{ url('/api/login') }}" 
                  method="GET" id="user_login_form" name="user_login_form" novalidate>
                  <div class="form-group login_form_group">
                    <div class="input-group">
                      <div class="input-group-prepend bg-transparent">
                        <span class="input-group-text border-right-0">
                          <i class="ti-user text-primary"></i>
                        </span>
                      </div>
                      <input type="text" class="form-control form-control-lg border-left-0" id="username" name="email" placeholder="e-post">
                    </div>
                    <p style="font-size: 14px; color: red; margin-top: 5px" id="usernameErr"></p>
                  </div>
                  <div class="form-group login_form_group">
                    <div class="input-group">
                      <div class="input-group-prepend bg-transparent">
                        <span class="input-group-text border-right-0">
                          <i class="ti-lock text-primary"></i>
                        </span>
                      </div>
                      <input type="password" class="form-control form-control-lg border-left-0" id="password" name="password" placeholder="Password">
                    </div>
                    <p style="font-size: 14px; color: red; margin-top: 5px" id="passwordErr"></p>
                  </div>
                  <div class="errorMsg">
                  
                  </div>
                  <div class = "frgt_pw_link">
                    <!-- <a data-toggle="modal" href="#myModal1">Glemt passord?</a> -->
                  </div>
                  <div class="row roleSelectionButtonRow">
                   
                    <div class="col-lg-6">
                      <input class="btn btn-block btn-primary" type="button" value="Support" style="border-radius: 10px; margin-bottom: 10px;" onclick="support_view();"> 
                    </div>
                    <div class="col-lg-6"> 
                      <input class="btn btn-block btn-primary" type="submit" value="Logg inn" style="border-radius: 10px; margin-bottom: 10px;"> 
                    </div>
                    <div class="col-lg-12">
                         <div id="error_message_block" style="padding-top: 14px;"></div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
</section>


    </div>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="{{asset('themes/frontend/assets/js/jquery-3.3.1.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/js/jquery.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/js/bootstrap_3_4_1.min.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/vendors/js/vendor.bundle.base.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/vendors/chart.js/Chart.min.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/vendors/datatables.net/jquery.dataTables.js')}}"></script>
  <script src="{{asset('themes/frontend/assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>

  <!-- inject:js -->

 <script src="{{asset('themes/frontend/assets/js/off-canvas.js')}}"></script>
 <script src="{{asset('themes/frontend/assets/js/hoverable-collapse.js')}}"></script>
 <script src="{{asset('themes/frontend/assets/js/off-canvas.js')}}"></script>
 <script src="{{asset('themes/frontend/assets/js/template.js')}}"></script>
 <script src="{{asset('themes/frontend/assets/js/settings.js')}}"></script>
 <script src="{{asset('themes/frontend/assets/js/todolist.js')}}"></script>
 <script src="{{asset('themes/frontend/assets/js/todolist.js')}}"></script>
 <script src="{{asset('themes/frontend/assets/js/dashboard.js')}}"></script>
 <script src="{{asset('themes/frontend/assets/js/data-table.js')}}"></script>
 <script src="{{asset('themes/frontend/assets/app_js/loadingoverlay.js')}}"></script>
 <script src="{{asset('themes/frontend/assets/app_js/jquery.cookie.js')}}"></script>
 <script src="{{asset('themes/frontend/assets/app_js/jquery.toaster.js')}}"></script>

</body>

</html>



<script language="javascript">
    var _accessToken;
    var _user_id;
    $(document).ready(function () {
        //User login
        var user_login_form = $("#user_login_form");
        user_login_form.submit(function(e) {
            e.preventDefault();

            $("#error_message_block").html('');

            var email = $("#username").val();
            var password = $('#password').val();
            if (email.length < 1 ) {
                $('#usernameErr').text('Vennligst skriv inn e-postadressen');
            } else {
                $('#usernameErr').text('');
            }

            if (password.length < 1 ) {
                $('#passwordErr').text('Vennligst skriv inn passordet');
            } else {
                $('#passwordErr').text('');
            }

            if ((password.length > 1) && (email.length > 1)) {
                url = user_login_form.attr("action");
                data = user_login_form.serialize() + '&device_type=web';
                $.LoadingOverlay("show");
                $.ajax({
                    type:"POST",
                    url:url,
                    data:data,
                    success: function(response) {
                        console.log('User login - '+JSON.stringify(response));
                        $.LoadingOverlay("hide");
                        if (response.Status) {
                            var data = response.Data;
                            var accessToken = data.token.accessToken;
                            var userData = data.user;
                            var web_token = data.web_token;
                            var user_token = data.user_token;
                            var username = userData.firstname+' '+userData.lastname;
                            localStorage.setItem('token', accessToken);
                            localStorage.setItem('user_id', userData.id);
                            localStorage.setItem('username', username);
                            localStorage.setItem('email', userData.email);
                            localStorage.setItem('role_id', userData.role_id);
                           
                            // var profileimage = userData.profile_picture;
                            // if (profileimage != null && profileimage != '') {
                            //     localStorage.setItem('profileimage', profileimage);
                            // }

                            // if ($('#remember').is(':checked')) {
                            //     $.cookie("accessToken", accessToken, { expires: 7 });
                            // } else {
                            //     $.removeCookie("accessToken");
                            //     //$.cookie("accessToken", access_token, { expires: cookieExp });
                            // }

                            var dash_url = $("#dashboard_url").val();
                            window.location.href = dash_url+'/'+user_token+'/'+web_token;
                        }
                    },
                    error: function(xhr, error) {
                        $.LoadingOverlay("hide");
                        manageErrorResponse(xhr, error);
                    }
                });
            } 
        });

    });

    $(".toggle-password").click(function() {
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
          $(this).removeClass("fa fa-fw fa-eye-slash field-icon toggle-password");
          $(this).addClass("fa fa-fw fa-eye field-icon toggle-password");
          input.attr("type", "text");
        } else {
          $(this).removeClass("fa fa-fw fa-eye field-icon toggle-password");
          $(this).addClass("fa fa-fw fa-eye-slash field-icon toggle-password");
          input.attr("type", "password");
        }
      });

    function validateLogin() {
      var email = $("#username").val();
      var password = $('#password').val();
      console.log(email);
      console.log(password);
      if (email.length < 1 ) {
        $('#usernameErr').text('Vennligst skriv inn e-postadressen');
      }
      else{
        $('#usernameErr').text('');
      }

      if (password.length < 1 ) {
        $('#passwordErr').text('Vennligst skriv inn passordet');
      }
      else{
        $('#passwordErr').text('');
      }

      if ((password.length > 1) && (email.length > 1)) {
        return true;
      }
      
      return false;
    }

    function manageErrorResponse(xhr, error, option) {
        // function manageErrorResponse(xhr, error, option = ['ss']) {
        var option = option || [];

        if (xhr.responseJSON.Message) {
            errors = xhr.responseJSON.Message;
        } else {
            errors = error.Message;
        }

        //$.toaster({ priority: 'danger', title: 'Error', message: errors });

        var error_html = '';
        if (Array.isArray(errors)) {
            $.each(errors, function( index, value ) {
                error_html += bindErrorHtml(value);
            });
        } else {
            error_html = bindErrorHtml(errors);
        }

        if (option['error_selector']) {
            $("#"+option['error_selector']).html(error_html);
        } else {
            $("#error_message_block").html(error_html);
        }
    }

    //Bind error messages.
    function bindErrorHtml(error) {
        html = '<div class="alert alert-danger alert-dismissible" style="background-color: #ffdada; !important;">'+
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>'+
                        '<strong>Error: </strong>' +error+
            '</div>';

        return html;
    }

    function support_view() {
        var base_path = $("#base_path").val();
        window.location.href = base_path+'/support';
    }

   
</script>