<?php
    
    if(isset($data)) {
        $status = $data['status'];
        $message = $data['message'];
        $token = $data['token'];
        $success = '';
        if ($status == true) {
            $success = 'login-page';
        }
    } else {
        $status = false;
        $message = 'Unauthorized access.';
        $success = '';
    }
?>

@if($status == true)
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Password Reset | Rivertrace Connected</title>
        <link rel="icon" sizes="16x16" href="{{asset('riv.jpg')}}">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css') }}">

        <!--Common css -->
        <link rel="stylesheet" href="{{asset('css/common.css')}}">

        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- icheck bootstrap -->
        <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css') }}">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

        <style>
           /* .alert-dismissible {
                padding-right: 0px;
            }*/

            .alert {
                border-radius: 0px;
            }

            .register_super_admin  .input-group .form-control {
                border: 1px solid #ced4da;
            }
        </style>
    </head>

    <body class="hold-transition login-page">
       
        <div class="login-box">
            <div class="login-logo">
                <a href="javascript:void(0);" style="color:white;"><b>Reset Password</b></a>
            </div>

            <input type="hidden" id="login_link" value="{{url('/login')}}">
            <input type="hidden" id="password_reset_url" value="{{url('/')}}">
            
             <div class="card">

                <div id="error_message_block"></div>
                <div id="success_message_block"></div>

                <div class="card-body" style="display: block;">
                    <form method="post" action="api/reset/password" accept-charset="UTF-8" id="reset_password_form" name="reset_password_form" class="form-horizontal">

                        <input type="hidden" value="PUT" name="_method" readonly>
                        <input type="hidden" value="{{$token}}" name="token" readonly>

                        <!-- <div class="form-group ">
                            <label for="old_password" class="col-sm-4 col-md-3 offset-3 control-label">Old Password</label>
                            <div class="col-sm-12 col-md-6 offset-3">
                                <input class="form-control" name="old_password" type="password" id="old_password" value="" minlength="1" maxlength="255" required="true" placeholder="Old password">
                                
                            </div>
                        </div> -->

                        <div class="form-group ">
                            <label for="new_password" class="col-md-12 control-label">New Password</label>
                            <div class="col-md-12">
                                <input class="form-control" name="new_password" type="password" id="new_password" value="" minlength="1" maxlength="255" required="true" placeholder="New password">
                                
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="confirm_password" class="col-md-12 control-label">Confirm New Password</label>
                            <div class="col-md-12">
                                <input class="form-control" name="confirm_password" type="password" id="confirm_password" value="" minlength="1" maxlength="255" required="true" placeholder="Confirm new password">
                                
                            </div>
                        </div>

                        <div class="col-md-12 text-center">
                            <input class="btn btn-primary custom_btn" type="submit" value="Reset">
                            <!-- <input class="btn btn-danger custom_btn" type="button" value="Cancel" onclick="clearChangePasswordForm();"> -->
                        </div>
                        <div class="col-md-12" id="login_btn">
                            <u><a href="{{url('/login')}}">Login</a></u>
                        </div>
                    <form>
                </div>
            </div>
        </div>
        <!-- /.login-box -->
      

        <!-- jQuery -->
        <script src="{{asset('plugins/jquery/jquery.min.js') }}"></script>
        <!-- Bootstrap 4 -->
        <script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <!-- AdminLTE App -->
        <script src="{{asset('dist/js/adminlte.min.js') }}"></script>

         <!-- jquery cookie -->
        <script src="{{asset('js/jquery.cookie.js') }}"></script>

        <!-- jquery loader -->
        <script src="{{asset('app-js/loadingoverlay.js') }}"></script>

        <script src="{{asset('app-js/login.js') }}"></script>
        
        <script src="{{asset('app-js/project.js') }}"></script>

    </body>

    </html>
@else
    <div style="padding-top:40px;"><center><h3>{{$message}}</h3></center></div>
@endif