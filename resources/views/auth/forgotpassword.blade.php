
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Forgot Password | Rivertrace Connected</title>
    <link rel="icon" sizes="16x16" href="{{asset('riv.jpg')}}">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css') }}">

    <!--Common css -->
    <link rel="stylesheet" href="{{asset('css/common.css')}}">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition login-page" style="background-color: #e5f7fd;">
    <div class="login-box">
        <div class="login-logo" style="height: 40px;">
            <!-- <a href="javascript:void(0)" style="color: white;"><b>RIVERTRACE CONNECTED</b></a> -->
            <!-- <a href="javascript:void(0);" style="color:white;"><img style="width:80%;" src="{{ asset('dist/img/Rivertrace_connected_rev_orange.png') }}"></a> -->
        </div>
        <!-- /.login-logo -->
        <div class="card" style="box-shadow: 0 0 2rem 0 rgba(136, 152, 170, .3) !important;">

            <div id="error_message_block"></div>
            <div id="success_message_block"></div>
            <a href="javascript:void(0);" style="color:white;"><img style="width:100%;" src="{{ asset('dist/img/rivertrace_connected_rev_orang.jpg') }}"></a>
            <div class="card-body login-card-body" style=" border-radius: 6px;">
                
                <p class="login-box-msg">You forgot your password? Here you can easily retrieve a new password.</p>

                <form action="api/passwordreset/request" method="post" accept-charset="UTF-8" id="forgot_password_form" class="form-horizontal">
                    <div class="input-group mb-3">
                        <input type="email" name="email" class="form-control" placeholder="Email">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary btn-block">Forgot Password</button>
                        </div>
                        <!-- /.col -->
                    </div>
                 </form>

                <p class="mt-3 mb-1">
                    <a href="{{route('login')}}">Login</a>
                </p>
                <p class="mb-0">
                    <a href="{{route('register')}}" class="text-center">New Customer Registration</a>
                </p>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->

    <!-- jQuery -->
    <script src="{{asset('plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap 4 -->
    <script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- jquery cookie -->
    <script src="{{asset('js/jquery.cookie.js') }}"></script>

    <!-- AdminLTE App -->
    <script src="{{asset('dist/js/adminlte.min.js') }}"></script>

     <!-- jquery loader -->
    <script src="{{asset('app-js/loadingoverlay.js') }}"></script>

    <script src="{{asset('app-js/login.js') }}"></script>

    <script src="{{asset('app-js/project.js') }}"></script>

</body>

</html>