<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Register | Rivertrace Connected</title>
    <link rel="icon" sizes="16x16" href="{{asset('riv.jpg')}}">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

   
    <style>
        .confirmation-message {
            display: none;
            margin: auto;
        }
        
        .card {
            min-height: 100px;
            -webkit-transition: min-height 1s ease-in;
            -moz-transition: min-height 1s ease-in;
            -ms-transition: min-height 1s ease-in;
            -o-transition: min-height 1s ease-in;
            transition: min-height 1s ease-in;
        }
    </style>

</head>

<body class="hold-transition register-page" style="background-color: #e5f7fd;">
    <div class="register-box">
        <div class="register-logo" style="height: 10px;">
            <!-- <a href="javascript:void(0);" style="color: white;"><b>RIVERTRACE CONNECTED</b></a> -->
            <!-- <a href="javascript:void(0);" style="color:white;"><img style="width:80%;" src="{{ asset('dist/img/Rivertrace_connected_rev_orange.png') }}"></a> -->
        </div>
        <div id="error_message_block"></div>
        <div id="success_message_block"></div>
        <a href="javascript:void(0);" style="color:white;"><img style="width:100%;" src="{{ asset('dist/img/rivertrace_connected_rev_orang.jpg') }}"></a>
        <div class="card" style="box-shadow: 0 0 2rem 0 rgba(136, 152, 170, .3) !important;">
            <div class="card-body register-card-body" style=" border-radius: 6px;">
                <p class="login-box-msg calibration-hide">New Customer Registration</p>

                <form action="api/registered" method="post" class="calibration-hide" accept-charset="UTF-8" id="user_register_form" >
                   <!--  <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Full name">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div> -->
                    <div class="input-group mb-3">
                        <input type="email" name="email" class="form-control" placeholder="Email">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="input-group mb-3">
                        <div id="register_recaptcha" class="g-recaptcha" data-sitekey="{{env('SITE_KEY')}}"></div>
                    </div> -->
                   
                    <div class="row">
                        <div class="col-8">
                            <div class="icheck-primary">
                                <input type="checkbox" id="agreeTerms" name="terms" value="agree" required>
                                <label for="agreeTerms">
               I agree to the <a href="privacy/terms_condition">terms</a>
              </label>
                            </div>
                        </div>
                        
                        <div class="col-4">
                            <!-- title="Please verify captcha for customer registration" disabled-->

                            <button id="register_button" type="submit" class="btn btn-primary btn-block btnregister">Register</button>
                        </div>
                       
                    </div>
                </form>

                <!-- <div class="social-auth-links text-center">
                    <p>- OR -</p>
                    <a href="#" class="btn btn-block btn-primary">
                      <i class="fab fa-facebook mr-2"></i>
                      Sign up using Facebook
                    </a>
                    <a href="#" class="btn btn-block btn-danger">
                      <i class="fab fa-google-plus mr-2"></i>
                      Sign up using Google+
                    </a>
                </div> -->

                <a href="{{route('login')}}" class="text-center calibration-hide">I already have an Account</a>


                <p class="confirmation-message  calibration-show">
                We have sent you a registration link on email, please ensure you check your spam folder <br/> <a href="{{route('login')}}" class="text-center calibration-show">Go to login screen</a></p>
            </div>
            <!-- /.form-box -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.register-box -->

    <!-- jQuery -->
    <script src="{{asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('dist/js/adminlte.min.js') }}"></script>

     <!-- jquery cookie -->
    <script src="{{asset('js/jquery.cookie.js') }}"></script>

    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
    async defer>
    </script>

    <!-- jquery loader -->
    <script src="{{asset('app-js/loadingoverlay.js') }}"></script>

    <script src="{{asset('app-js/login.js') }}"></script>

    <script src="{{asset('app-js/project.js') }}"></script>
    
    <script>
        $(document).ready(function() {
            $('.btnregister').click(function() {
                // $('.calibration-hide').fadeOut(500);
                // $('.confirmation-message').fadeIn(500);
                // $('.card').css('min-height', '0px');
            });
        });
    </script>
    <script type="text/javascript">

        function getCaptchaVerifyCallback(response) {
            $("#register_button").removeAttr('disabled');
            $("#register_button").attr('title','Click here to customer registration');
        }

        var onloadCallback = function() {
            //grecaptcha.render('login_recaptcha');
            grecaptcha.render('register_recaptcha', {
              'callback' : getCaptchaVerifyCallback,
              'theme' : 'light'
            });
        };

    </script>
</body>

</html>