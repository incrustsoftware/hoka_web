<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Register | Rivertrace Connected</title>
    <link rel="icon" sizes="16x16" href="{{asset('riv.jpg')}}">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

   
    <style>
        .confirmation-message {
            display: none;
            margin: auto;
        }
        
        .card {
            min-height: 100px;
            -webkit-transition: min-height 1s ease-in;
            -moz-transition: min-height 1s ease-in;
            -ms-transition: min-height 1s ease-in;
            -o-transition: min-height 1s ease-in;
            transition: min-height 1s ease-in;
        }
    </style>

</head>

<body class="hold-transition register-page" style="background-color: #e5f7fd;">
    <div class="register-box">
        <div class="register-logo" style="height: 40px;">
           <!--  <a href="javascript:void(0);" style="color: white;"><b>RIVERTRACE CONNECTED</b></a> -->
           <!-- <a href="javascript:void(0);" style="color:white;"><img style="width:80%;margin-top: 60px;" src="{{ asset('dist/img/Rivertrace_connected_rev_orange.png') }}"></a> -->
        </div>
        <div id="error_message_block"></div>
        <div id="success_message_block"></div>
        <a href="javascript:void(0);" style="color:white;"><img style="width:100%;" src="{{ asset('dist/img/rivertrace_connected_rev_orang.jpg') }}"></a>
        <div class="card" style="box-shadow: 0 0 2rem 0 rgba(136, 152, 170, .3) !important;">
           
          <div class="card-body register-card-body" style=" border-radius: 6px;">
                <?php if(!isset($msg)){?>
                <p class="login-box-msg calibration-hide">Register a new Account</p>

                <form action="{{route('customer.registration')}}" method="post" class="calibration-hide" accept-charset="UTF-8" id="user_registeration_form" >
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="firstname" placeholder="First name">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="lastname" placeholder="Last name">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <?php $decoded_token =explode("&&",base64_decode($token)) ; ?>
                    <div class="input-group mb-3">
                        <input type="email" name="" class="form-control" placeholder="Email" value="<?php echo $decoded_token[0];?>" disabled >
                        <input type="hidden" name="email" value="<?php echo $decoded_token[0];?>" >
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="token" value="<?php echo $token; ?>">
                    <input type="hidden" name="account" value="0">
                    <input type="hidden" name="timestamp" value="<?php echo $decoded_token[1];?>">
                 
                    <div class="input-group mb-3">
                        <input type="text" class="form-control"  name="phonenumber" placeholder="Phone Number">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-phone"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control"  name="shipowner" placeholder="Ship Owner">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control"  name="shipmanagementcompany" placeholder="Ship Management Company">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-edit"></span>
                            </div>
                        </div>
                    </div>
                    <div>
                        <label>
                            <small>
                                <b>Note : End-user can add ship owner name and ship management name.
                                </b>
                            </small>
                        </label>
                    </div>
                    
                    <div class="row">
                        <div class="col-8">
                            <div class="icheck-primary">
                                <input type="checkbox" id="agreeTerms" name="terms" value="agree" required>
                                <label for="agreeTerms">
                                   I agree to the <a href="../privacy/terms_condition" target="_blank">terms</a>
                                  </label>
                            </div>
                        </div>
                        
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block btnregister">Register</button>
                        </div>
                       
                    </div>
                </form>

                <a href="{{route('login')}}" class="text-center calibration-hide">I already have an Account</a>


                <p class="confirmation-message  calibration-show">
                    Register a new Account<br/>We have sent you a confirmation email, please ensure you check your spam folder <br/> <a href="{{route('login')}}" class="text-center calibration-show">Go to login screen</a></p>
                <?php 
                    }else{ 
                ?>
           
                    <!-- <a href="{{route('login')}}" class="text-center calibration-hide">I already have an Account</a> --><br>
                     <p class="text-center calibration-hide">Your Link has been expired ! Please <a href="{{route('register')}}" class="text-center calibration-hide">try again</a></p>
                <?php } ?>
            </div>
           
          
            <!-- /.form-box -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.register-box -->

    <!-- jQuery -->
    <script src="{{asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('dist/js/adminlte.min.js') }}"></script>

     <!-- jquery cookie -->
    <script src="{{asset('js/jquery.cookie.js') }}"></script>

    <!-- jquery loader -->
    <script src="{{asset('app-js/loadingoverlay.js') }}"></script>

    <script src="{{asset('app-js/login.js') }}"></script>

    <script src="{{asset('app-js/project.js') }}"></script>
    
    <script>
        $(document).ready(function() {
            // $('.btnregister').click(function() {
            //     $('.calibration-hide').fadeOut(500);
            //     $('.confirmation-message').fadeIn(500);
            //     $('.card').css('min-height', '0px');
            // });
        });
    </script>
</body>

</html>