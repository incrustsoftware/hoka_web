<!doctype html>
<html lang="en">
<head>
<title>Hoka : Logout</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="{{asset('themes/frontend/assets/css/jquery-ui.css')}}">
<link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/ti-icons/css/themify-icons.css')}}">
<link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/css/vendor.bundle.base.css')}}">
<link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/font-awesome/css/font-awesome.min.css')}}">
<!-- endinject -->
<link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/simple-line-icons/css/simple-line-icons.css')}}">
<!-- Plugin css for this page -->
<!-- Plugin css for this page -->
<link rel="stylesheet" href="{{asset('themes/frontend/assets/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}">
<!-- End plugin css for this page -->

<link rel="stylesheet" href="{{asset('themes/frontend/assets/css/horizontal-layout-light/style.css')}}">
<!-- endinject -->
<!-- <link rel="shortcut icon" href="{{asset('themes/frontend/assets/images/favicon.png')}}" /> -->
<link rel="shortcut icon" href="{{asset('themes/images/hoka_logo.png')}}" />

<!-- <script src="{{asset('themes/frontend/assets/js/jquery-3.3.1.js')}}"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="{{asset('themes/frontend/assets/app_js/common.js')}}"></script>

</head>
<body>
   
    <input id="login_url" type="hidden" value="{{route('login')}}">
    <input id="base_path" type="hidden"  value="{{ url('/') }}">

    <script src="{{asset('themes/frontend/assets/js/jquery-ui.js')}}"></script>
    <script src="{{asset('themes/frontend/assets/vendors/js/vendor.bundle.base.js')}}"></script>
    <script src="{{asset('themes/frontend/assets/app_js/loadingoverlay.js')}}"></script>
    <script src="{{asset('themes/frontend/assets/app_js/jquery.toaster.js')}}"></script>
    <script src="{{asset('themes/frontend/assets/app_js/jquery.exzoom.js')}}"></script>
    <!-- End custom js for this page-->

    <script type="text/javascript">
        
        $(document).ready(function() {

            $.LoadingOverlay("show");
            setTimeout(function(){  logout(); }, 2000);
           
        });

        function logout() {
            

            request_url = getBaseUrl()+'/api/users/logout';
            login_url = $("#login_url").val();

            $.ajax({
                type:"GET",
                url:request_url,
                dataType: "json",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Authorization', getBearerToken());
                },
                success: function(response) {
                    //console.log('User logout - '+JSON.stringify(response));
                    $.LoadingOverlay("hide");
                    localStorage.clear();
                    //$.removeCookie("accessToken");
                    if (response.Status) {
                        window.location.href = login_url;
                    }
                },
                error: function(xhr, error) {
                    $.LoadingOverlay("hide");
                    localStorage.clear();
                    //$.removeCookie("accessToken");
                    window.location.href = login_url;
                }
            });
        }

    </script>

</body>
</html>
