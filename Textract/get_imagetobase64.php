<?php

$error = null;
$name = null;
if(isset($_FILES['image']) && $_FILES['image']['size'] > 0) {
	$file_name = $_FILES['image']['name'];
	$file_size = $_FILES['image']['size'];
	$file_tmp = $_FILES['image']['tmp_name'];
 	$file_type = $_FILES['image']['type'];
	$file_ext = strtolower(end(explode('.',$_FILES['image']['name'])));
	  
	$extensions = array("jpeg","jpg","png");
	  
	if(in_array($file_ext, $extensions)=== false) {
	   $error = "extension not allowed, please choose a JPEG or PNG file.";
	}

	// if($file_size > 2097152) {
	//     $error = 'File size must be excately 2 MB';
	// }

	$uploaddir = '/images/';
	$uploadfile = $uploaddir . basename($_FILES['image']['name']);

	if (!file_exists(__DIR__.'/images')) {
	    mkdir(__DIR__.'/images', 0777, true);
	}

	if ($error == null) {
		$name = __DIR__.'/images/'.strtotime("now"). $_FILES["image"]['name'];
		if (move_uploaded_file($_FILES['image']['tmp_name'], $name)) {
		    //echo "Uploaded";
		} else {
		   //echo "File not uploaded";
		}
    }
} else {
	$error = 'Please upload image file';
}

if ($error != null) {
	$response['Status'] = false;
	$response['Message'] = $error;

	echo json_encode($response);

	return;
}

// $type = pathinfo($name, PATHINFO_EXTENSION);
// $data = file_get_contents($name);
// $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

$data = file_get_contents($name);
$base64 = base64_encode($data);
if ($name != null) {
	unlink($name);
}

echo $base64;

?>
