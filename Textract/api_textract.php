<?php

$response['Status'] = true;
$response['Data'] = '';
$response['Message'] = '';
$base64image = null;

try {
	$json = file_get_contents('php://input');
	$data = json_decode($json, true); 
	if (isset($data['image']) && $data['image'] != '') {
		$base64image = $data['image'];
	} else {
		$response['Status'] = false;
		$response['Message'] = 'Please provide the base 64 image data.';
		echo json_encode($response);
		http_response_code(400);
		return;
	}
} catch (Exception $ex) {
	$error = $ex->getMessage();
	$response['Status'] = false;
	$response['Message'] = $error;
	echo json_encode($response);
	http_response_code(400);
	return;	
}

if (!file_exists(__DIR__.'/images')) {
    mkdir(__DIR__.'/images', 0777, true);
}

$name = __DIR__.'/images/'.strtotime("now").'.png';
$image_base64 = base64_decode($base64image);
file_put_contents($name, $image_base64);

require 'aws/aws-autoloader.php';

use Aws\Credentials\CredentialProvider;
use Aws\Textract\TextractClient;
use Aws\Exception\AwsException;

try {
	$client = new TextractClient([
	    'region' => 'ap-south-1',
		'version' => '2018-06-27',
		'http' => [
			'verify' => 'C:\wamp64\bin\php\php7.2.25\cacert.pem'
		],
		'credentials' => [
	        'key'    => 'AKIATLZU3TIGAXZIEQ5G',
	        'secret' => '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'
		]
	]);

	$filename = $name;
	$file = fopen($filename, "rb");
	$contents = fread($file, filesize($filename));
	fclose($file);
	$options = [
	    'Document' => [
			'Bytes' => $contents
	    ],
	    'FeatureTypes' => ['FORMS'], // REQUIRED
	];
	$result = $client->analyzeDocument($options);

	$blocks = $result['Blocks'];
	$lines = [];
	$words = [];
	//$confidence = 0;
	foreach ($blocks as $key => $value) {
		if (isset($value['BlockType']) && $value['BlockType']) {
			$blockType = $value['BlockType'];
			if (isset($value['Text']) && $value['Text']) {
				$text = $value['Text'];
				$conf = $value['Confidence'];
				if ($blockType == 'WORD') {
					array_push($words, $text);
				} else if ($blockType == 'LINE') {
					array_push($lines, $text);
					//$confidence = $confidence + $conf;
				}
			}
		}
	}

	$accuracy = null;//$confidence/count($lines);

	$textdata['Accuracy'] = $accuracy;
	$textdata['Line'] = $lines;
	$textdata['Word'] = $words;

	$response['Status'] = true;
	$response['Data'] = $textdata;
	$response['Code'] = 200;
	$response['Message'] = 'Image data read successfully.';

	if ($name != null) {
		unlink($name);
	}

	echo json_encode($response);
	http_response_code(200);

} catch (AwsException $e) {
	$error = $e->getMessage();
	$response['Status'] = false;
	$response['Message'] = $error;
	$response['Code'] = 400;

	if ($name != null) {
		unlink($name);
	}

	echo json_encode($response);
	http_response_code(200);
}

?>
