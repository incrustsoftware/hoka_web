<?php

$error = null;
$name = null;
if(isset($_FILES['image']) && $_FILES['image']['size'] > 0) {
	$file_name = $_FILES['image']['name'];
	$file_size = $_FILES['image']['size'];
	$file_tmp = $_FILES['image']['tmp_name'];
 	$file_type = $_FILES['image']['type'];
	
	$exp = explode('.',$_FILES['image']['name']);
	
	$file_ext = strtolower(end($exp));
	  
	$extensions = array("jpeg","jpg","png");
	  
	if(in_array($file_ext, $extensions)=== false) {
	   $error = "extension not allowed, please choose a JPEG or PNG file.";
	}

	// if($file_size > 2097152) {
	//     $error = 'File size must be excately 2 MB';
	// }

	$uploaddir = '/images/';
	$uploadfile = $uploaddir . basename($_FILES['image']['name']);

	if (!file_exists(__DIR__.'/images')) {
	    mkdir(__DIR__.'/images', 0777, true);
	}

	if ($error == null) {
		$name = __DIR__.'/images/'.strtotime("now"). $_FILES["image"]['name'];
		if (move_uploaded_file($_FILES['image']['tmp_name'], $name)) {
		    //echo "Uploaded";
		} else {
		   //echo "File not uploaded";
		}
    }
} else {
	$error = 'Please upload image file';
}

if ($error != null) {
	$response['Status'] = false;
	$response['Message'] = $error;

	echo json_encode($response);

	return;
}


require 'aws/aws-autoloader.php';

use Aws\Credentials\CredentialProvider;
use Aws\Textract\TextractClient;
use Aws\Exception\AwsException;

//Incrust
//Textract Access key ID : AKIATLZU3TIGAXZIEQ5G
//Textract Secret access key : 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

try {
	$client = new TextractClient([
	    'region' => 'ap-south-1',
		'version' => '2018-06-27',
		'http' => [
			'verify' => 'C:\wamp64\bin\php\php7.2.25\cacert.pem'
		],
		//'ssl.certificate_authority' => 'C:\wamp64\bin\php\php7.2.25\cacert.pem',
		'credentials' => [
	        'key'    => 'AKIATLZU3TIGAXZIEQ5G',
	        'secret' => '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'
		]
	]);

	// The file in this project.
	//$filename = "aws_cli_text_document.jpg";
	$filename = $name;//"download.png";
	$file = fopen($filename, "rb");
	$contents = fread($file, filesize($filename));
	fclose($file);
	$options = [
	    'Document' => [
			'Bytes' => $contents
	    ],
	    'FeatureTypes' => ['FORMS'], // REQUIRED
	];
	$result = $client->analyzeDocument($options);

	$blocks = $result['Blocks'];
	$lines = [];
	$words = [];
	//$confAdd = [];
	$confidence = 0;
	foreach ($blocks as $key => $value) {


		if (isset($value['BlockType']) && $value['BlockType']) {
			$blockType = $value['BlockType'];
			if (isset($value['Text']) && $value['Text']) {
				$text = $value['Text'];
				$conf = $value['Confidence'];
				if ($blockType == 'WORD') {
					array_push($words, $text);
				} else if ($blockType == 'LINE') {
					array_push($lines, $text);
					//array_push($confAdd, $conf);
					$confidence = $confidence + $conf;
				}
			}
		}
	}


	$accuracy = $confidence/count($lines);
	$response['Status'] = true;
	$response['Line'] = $lines;
	$response['Word'] = $words;
	$response['Accuracy'] = $accuracy;
	//$response['confAdd'] = $confAdd;
	$response['Message'] = 'Textract data selected successfully.';

	if ($name != null) {
		unlink($name);
	}

	echo json_encode($response);

} catch (AwsException $e) {
	$error = $e->getMessage();
	$response['Status'] = false;
	$response['Message'] = $error;

	if ($name != null) {
		unlink($name);
	}

	echo json_encode($response);
}

?>
