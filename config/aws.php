<?php 

return [
    "ocr" => [
    	'key' => env('AWS_TEXTRACT_KEY',''),
    	'secret' => env('AWS_TEXTRACT_SECRET',''),
    ]
];