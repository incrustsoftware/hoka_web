<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Messages
    |--------------------------------------------------------------------------
    |
    | This file is for storing the messages for error and success response.
    |
     */

    'ErrorMessages' => [
        'invalid_login' => 'Please enter valid email or password.',
        'account_block' => 'Your account is blocked.',
        'user_not_exist' => "We can't find a user with that email.",
        'user_not_found' => 'Users not found.',
        'old_password_error' => 'Provide valid old password.',
    ],

    'SucessMessages' => [
        'login_success' => 'User login successfully.',
        'password_reset' => 'Password reset email has been send successfully.',
        'logout_user' => 'User logout successfully.',
        'user_create' => 'User created successfully.',
        'user_show' => 'User selected successfully.',
        'password_success' => 'Password changed successfully.'
    ],

];
