$(document).ready(function()
{
   $('[data-toggle="tooltip"]').tooltip();
    setInterval('update_clock()',1000);
    // setInterval('get_notification()',10000);
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    }, 15000);
    
    greeting();
    que_send();
    groupEdit();
    verify_phone();
    notification();

    data_to_modal();
    data_edit_modal();
    data_delete_modal();
    
    loadWeather('Vernon Hills, Illinois',''); //@params location, woeid
    
    $('.messages-menu').on('click',function(){
        $('#badge').html('');
    });

    $('.btn-reset').on('click',function(){
        $('form')[0].reset();
       // $('#avatar_load').attr('src','');
       // $('#avatar_input').val('');
    });

    $('#btn-rmv-pic').on('click',function(){
        $('#avatar_load').attr('src','');
        $('#avatar_input').val('');
        $('#add_image').val('0');
    });

    $('#btn-ack').on('click', function() {
        var id = $(this).data('id');
        var name = $(this).data('name');
        $.ajax({
            type:'POST',
            url:'dashboard/ack_alert', 
            cache: false,
            data: {id : id, name : name},           // To unable request pages to be cached
            beforeSend: function(){
                $('#ack-msg').html('<i class="fa fa-spinner fa-spin"></i>');
            },
            success: function(d){
                $('#ack-msg').html('');
                if(d == true){
                    $('#ack-msg').text('Acknowledgement has been made. Thank you.');
                }else{
                    $('#ack-msg').text('Acknowledgement couldn\'t be made. Sorry');
                }
                window.setTimeout(function() {
                    $('#ack-msg').html('');  
                }, 6000);
            }
        });
        return false;
    });

    $('#btn-print').on('click',function(){
        var contents = $("#div-details").html();
        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute", "top": "-1000000px" });
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>KittyShield</title>');
        frameDoc.document.write('</head><body>');
        //Append the external CSS file.
        frameDoc.document.write('<link rel="stylesheet" href="https://kittyshield.co/assets/css/bootstrap.min.css" type="text/css">');
        frameDoc.document.write('<link rel="stylesheet" href="https://kittyshield.co/assets/font-awesome/css/font-awesome.min.css" type="text/css">');

        frameDoc.document.write('<link rel="stylesheet" href="https://kittyshield.co/assets/css/animate.min.css" type="text/css">');

        frameDoc.document.write('<link rel="stylesheet" href="https://kittyshield.co/assets/css/adminlte.min.css" type="text/css">'); 
        frameDoc.document.write('<link rel="stylesheet" href="https://kittyshield.co/assets/css/style.css" type="text/css">');
        frameDoc.document.write('<link rel="stylesheet" href="https://kittyshield.co/assets/css/skin-black.css" type="text/css">');
        //Append the DIV contents.
        var name    = $(this).data('name');
        var gender  = $(this).data('gender');
        var breed   = $(this).data('breed');
        var micro   = $(this).data('micro');
        var number  = $(this).data('num');
        frameDoc.document.write('<div class="row text-center"><div class="col-lg-12"><h3>'+name+'</h3><h5><strong >Microchip brand:</strong> <i class="legend">'+micro+'</i> <strong>Gender:</strong> <i>'+gender+'</i></h5><h5><strong>Microchip No:</strong> <i class="legend">'+number+'</i>  <strong>Breed:</strong> <i>'+breed+'</i></h5></div>')
        frameDoc.document.write(contents);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
        
    });
    
    /*$('#quesCat').on('show.bs.modal' , function(e){
        $('.modal-title').text(e.relatedTarget.dataset.name);
        // $('#ques_tabs a:first').tab('show');

        var cat = e.relatedTarget.dataset.cat;
    /*       var que = '1';

        get_form_on_show('1',cat,$('#water-inner'));

       $('#btn-fwp-form').on('click', function(e){
            e.preventDefault();
            var data = $('#fwp-form').serialize();
            send_details(data,$('#water-inner'));
        });*

        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            // $('.modal-title').text($(this).text()); 
           
            var target = $(e.target).attr('href');
                if( target == '#personality'){
                    get_form_on_show('2',cat,$('#person-inner'));
                    $('#btn-per-form').on('click', function(e){

                        var data = $('#per-form').serialize();
                        send_details(data,$('#person-inner'));
                    });
                }else if( target == '#elimination'){
                    get_form_on_show('3',cat,$('#eliminate-inner'));
                     $('#litter-check input[name="litter_type"]').click(function () {
                             var $this = $(this);
                             if ($this.is(':checked')) {
                                 // $('#shcompany').hide();
                                 alert('asvsas');
                             } else {
                                 $('#shcompany').show();
                             }
                         });
                    if($('#litter-check input[name="litter_type"]').is(':checked')){
                        $('#litter-box input[name="litter_type"]').show();
                        alert('ss00');
                    }else{
                        $('#litter-box').hide();
                    }

                    $('#btn-eli-form').on('click', function(e){

                        var data = $('#eli-form').serialize();
                        send_details(data,$('#eliminate-inner'));
                    });

                }else if( target == '#exercise'){
                    get_form_on_show('4',cat,$('#exercise-inner'));

                    $('#btn-exe-form').on('click', function(e){

                        var data = $('#exe-form').serialize();
                        send_details(data,$('#exercise-inner'));
                    });
                
                }else if( target == '#napping'){
                    get_form_on_show('5',cat,$('#napping-inner'));
                    $('#btn-nap-form').on('click', function(e){

                        var data = $('#nap-form').serialize();
                        send_details(data,$('#napping-inner'));
                    });

                }else if( target == '#medical'){
                    get_form_on_show('6',cat,$('#medical-inner'));
                    $('#btn-med-form').on('click', function(e){

                        var data = $('#med-form').serialize();
                        send_details(data,$('#medical-inner'));
                    });

                }else if( target == '#emergency'){
                    get_form_on_show('7',cat,$('#emergency-inner'));
                    $('#btn-eme-form').on('click', function(e){

                        var data = $('#eme-form').serialize();
                        send_details(data,$('#emergency-inner'));
                    });
                
                }else if( target == '#home'){
                    get_form_on_show('8',cat,$('#home-inner'));
                    $('#btn-hme-form').on('click', function(e){

                        var data = $('#hme-form').serialize();
                        send_details(data,$('#home-inner'));
                    });

                }else if(target == '#water'){
                    get_form_on_show('1',cat,$('#water-inner'));

                    $('#btn-fwp-form').on('click', function(e){
                        var data = $('#fwp-form').serialize();
                  
                        // data += "&cat="+encodeURIComponent(cat) + "&question="+encodeURIComponent(que);
                        // data.push({question : que, cat : cat});
                        send_details(data,$('#water-inner'));
                    });
                }
        });

       /* $('.tab-change').on('click', function () {
                e.preventDefault();
            // alert($(this).data('value'));
            que = $(this).data('value');
            var div =$(this).data('div');
            var d = document.getElementById(div);
            get_form_on_change(que,cat,d);

            $('.modal-title').text($(this).text()); 
            $('#btn-fwp-form').on('click', function(e){
                var data = $('#fwp-form').serialize();
          
                // data += "&cat="+encodeURIComponent(cat) + "&question="+encodeURIComponent(que);
                // data.push({question : que, cat : cat});
                send_details(data,$('#water-inner'));
            });
        });*

    });
    
    $('#quesCat').on('hide.bs.modal' , function(e){
        $(this).removeData('bs.modal');
        
        $('a[data-toggle="tab"]').on('hide.bs.tab', function(e){
            $(this).removeData('bs.modal');
       })
    });
    */

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        
        if($('#med-check').is(':checked')){
            $("#med-box").show();
        }
        $("#med-check").change(function () {
            //check if its checked. If checked move inside and check for others value
            if (this.checked && this.value === "other") {
                //add a text box next to it
                $("#med-box").show();
            } 
            else if (!this.checked && this.value === "other") {
                //remove if unchecked
                $("#med-box").hide();
            }
        });

        if($('#litter-check').is(':checked')){
            $("#litter-box").show();
        }
        $("#litter-check").change(function () {
            //check if its checked. If checked move inside and check for others value
            if (this.checked && this.value === "other") {
                //add a text box next to it
                $("#litter-box").show();
            } 
            else if (!this.checked && this.value === "other") {
                //remove if unchecked
                $("#litter-box").hide();
            }
        });
    });

    $('.message-delete').on('click', function(e) {
        if(confirm('Proceed to delete this message?')){
            message_delete($(this).data('id'));
        }
    });
    
    $('#message-delete_all').on('click', function(e) {
        if(confirm('Delete all messages. Proceed?')){
            message_delete_all();
        }
    });

    $('.delete-guardian').on('click', function() {
        if(confirm('This action will remove all data of care giver. Still proceed?')){
            guardian_delete($(this).data('id'),$(this).data('image'));
        }
    });
  //  coupon_yearly
    $( ".apply_coupon_yearly" ).on("click", function() {
    
        var formControl = $(this).parent('form');
        console.log(formControl);
        if($('.coupon_yearly').val() == '') 
        {
            $('.coupon_msg').text('');
            if($("input[name='bill']:checked").val() == 'monthly') {
                $(".discount-of-amount-monthly").val($('#default_monthly_amount').val());
            } else {
                $(".discount-of-amount-yearly").val($('#default_yearly_amount').val());
            }
            return false;
        }
        var amount= $("input[name='bill']:checked").data("camount");

                    
         $.ajax({
            type: 'POST',
            url: '/payment/coupon_check',
            cache: false,
            data: {code: $('.coupon_yearly').val(),amount:amount},
            beforeSend: function(){
                $('.coupon_msg').text('');
                $('.coupon_msg').addClass('fa fa-spinner fa-spin');
            },
            success: function(data){
                if(data.status=="success")
                {
                   
                 $(".coupon_msg").text(data.message);
                 $('#discount_of_amount').val(data.total_discount_amount);
                // $('.stripe-button').attr('data-amount', 1234); 

                 $(".discount_offmsg").text(data.displaymsg);
                
                if($("input[name='bill']:checked").val() == 'monthly') {
                    $(".discount-of-amount-monthly").val(data.total_discount_amount);
                } else {
                    $(".discount-of-amount-yearly").val(data.total_discount_amount);
                }

                }else{
                   
                 $('.coupon_msg').text(data.message);
                 
                }
            },
            complete: function(){
                $('.fa-spin').removeClass('fa fa-spinner fa-spin');
            }
        });
        return false;
    });

   
    $( ".apply_coupon" ).on("click", function() {
       
        var formControl = $(this).parent('form');
        console.log(formControl);
        if($('.coupon').val() == '') 
        {
            $('.coupon_msg').text('');
            if($("input[name='bill']:checked").val() == 'monthly') {
                $(".discount-of-amount-monthly").val($('#default_monthly_amount').val());
            } else {
                $(".discount-of-amount-yearly").val($('#default_yearly_amount').val());
            }
            return false;
        }
        var amount= $("input[name='bill']:checked").data("camount");

                    
         $.ajax({
            type: 'POST',
            url: '/payment/coupon_check',
            cache: false,
            data: {code: $('.coupon').val(),amount:amount},
            beforeSend: function(){
                $('.coupon_msg').text('');
                $('.coupon_msg').addClass('fa fa-spinner fa-spin');
            },
            success: function(data){
                if(data.status=="success")
                {
                   
                 $(".coupon_msg").text(data.message);
                 $('#discount_of_amount').val(data.total_discount_amount);
                // $('.stripe-button').attr('data-amount', 1234); 

                 $(".discount_offmsg").text(data.displaymsg);
                
                if($("input[name='bill']:checked").val() == 'monthly') {
                    $(".discount-of-amount-monthly").val(data.total_discount_amount);
                } else {
                    $(".discount-of-amount-yearly").val(data.total_discount_amount);
                }

                }else{
                   
                 $('.coupon_msg').text(data.message);
                 
                }
            },
            complete: function(){
                $('.fa-spin').removeClass('fa fa-spinner fa-spin');
            }
        });
        return false;
    });

    // Custom pay button click event handled
    var stripeInterval, payBtnAction;
    $('.pay-with-card-btn').on('click', function(){
        var discounted_amount, stride_data_description, script_render_control;
        payBtnAction = $(this);
        payBtnAction.attr('disabled', 'disabled');
        var stripe_publishable_key = $('#stripe_publishable_key').val();
        if(payBtnAction.attr('data-item') == 'monthly') {
            stride_data_description = $('.stripe-data-description-monthly').val();
            discounted_amount = $('.discount-of-amount-monthly').val();
            script_render_control = $('.checkout-button-script-monthly');
        } else {
            stride_data_description = $('.stripe-data-description-yearly').val();
            discounted_amount = $('.discount-of-amount-yearly').val();
            script_render_control = $('.checkout-button-script-yearly');
        }
        
        var scriptTag = '<script src="https://checkout.stripe.com/checkout.js" class="stripe-button"'+
                            'data-key="' + stripe_publishable_key + '"'+
                            'data-description="' + stride_data_description + '"'+
                            'data-amount="'+discounted_amount+'"'+
                            'data-billing-address="true"'+
                            'data-shipping-address="false"'+
                            'data-name="KittyShield"'+
                            'data-image="./themes/frontend/images/KittyShield.png"'+
                            'data-locale="auto">'+
                          '</script>';
        script_render_control.html('');
        script_render_control.html(scriptTag);
        stripeInterval = setInterval(stripeIntervalFunction, 1000);
    });

    var stripeIntervalFunction = function() {
        if(payBtnAction.attr('data-item') == 'monthly') {
            if($('.checkout-button-script-monthly').html() != '') {
                $('form[id=bill_month] .stripe-button-el').trigger('click');
                clearInterval(stripeInterval);
                payBtnAction.removeAttr('disabled');
            }
        } else {
            if($('.checkout-button-script-yearly').html() != '') {
                $('form[id=bill_year] .stripe-button-el').trigger('click');
                clearInterval(stripeInterval);
                payBtnAction.removeAttr('disabled');
            }
        }
    }


    // $('bill_month').show();
    // paypal_month();
    // paypal_year();

    var radio = $("input[type='radio']:checked").val();
    if(radio == 'monthly'){
        $('#paypal-button-mth').show();
        $('#paypal-button-yr').hide();

        $('#bill_month').show();
        $('#bill_year').hide();
    }else{
        $('#paypal-button-yr').show();
        $('#paypal-button-mth').hide();

        $('#bill_year').show();
        $('#bill_month').hide();
    }

    $('input:radio[name="bill"]').on('click change', function(e) {
        var type = $('input[name="bill"]:checked').val();
        if(type == 'monthly'){
            $('#bill_month').show();
            $('#bill_year').hide();

            $('#paypal-button-mth').show();
            $('#paypal-button-yr').hide();
           
        }else{
            $('#bill_year').show();
            $('#bill_month').hide(); 

            $('#paypal-button-yr').show();
            $('#paypal-button-mth').hide();
        }    
    });
});

function verify_phone() {
    $('#verify-phone').on('click',function(e){
        var spin = $(this).data('spin');
        $.ajax({
            type: 'POST',
            url: '/setting/get_verify_code',
            cache: false,
            beforeSend: function(){
                $('#verify-phone').html(spin+' Verify');
            },
            success: function(data){
                if(data != false){
                    $("#verify").val(data);
                    $('#verifyModal').modal('show');
                }
            },
            complete: function(){
                $('#verify-phone').html('Verify');
            }
        });
         return false;
    });

    $('#verifyModal').on('shown.bs.modal', function(e){
        e.preventDefault();

        $('#btn-verify').on('click',function(e){
            var verification = $('#verify').val();
            var code    = $('#vcode').val();
            $.ajax({
                type: 'POST',
                url: '/setting/do_verify',
                cache: false,
                data: {verify : verification, code: code},
                beforeSend: function(){
                    $('#progress').show();
                },
                success: function(data){
                    if(data){
                       $('#verifyModal').modal('hide');
                       location.reload();
                    }else{
                        $('#msg').text("Error verifying number! Please try again in the next 10 minutes");
                        $('#msg').show();
                    }
                },
                complete: function(){
                    $('#progress').hide();
                }
            });
            return false;
        });

        $('#btn-vcancel').on('click',function(e){
            var verification = $('#verify').val();
            $.ajax({
                type: 'POST',
                url: '/setting/cancel_verify',
                cache: false,
                data: {verification : verification},
                beforeSend: function(){
                    $('#progress').show();
                },
                success: function(data){
                   $('#verifyModal').modal('hide');
                    console.log(data);
                },
                complete: function(){
                    $('#progress').hide();
                }
            });
            return false;
        });
    });

    $('#verifyModal').on('hidden.bs.modal' , function(e){
        $(this).removeData('bs.modal');
        
        $('input[class="form-control"]').val('');
    });
}

function notification() {
    $('#modalNotify').on('show.bs.modal', function(e){
        var notice = e.relatedTarget.dataset.type;
        var title = e.relatedTarget.dataset.title;
        var id = e.relatedTarget.dataset.id;
        var status = e.relatedTarget.dataset.status;
        var user = e.relatedTarget.dataset.user;
        var tag = $(e.relatedTarget);
        // var message = 
        // 1-danger,2-info,3-warning,4-success
        var notice_msg = '';
        if(notice == '1'){
                                 
            notice_msg = '<i class="fa  fa-bolt" style="color: #f44336;"></i> ';

        }else if(notice == '2'){
            notice_msg = '<i class="fa fa-exclamation-circle" style="color: #2196F3;"></i> ';

        }else if(notice == '3'){
            notice_msg = '<i class="fa fa-warning"  style="color: #FFC107;"></i> ';

        }else if(notice == '4'){
            notice_msg = '<i class="fa fa-check" style="color: #4CAF50;"></i> ';

        }else{
            notice_msg = '<i class="fa fa-bell" style="color: #4E342E;"></i> ';
        }
        $('.modal-title').html(notice_msg+title);
        $('#md-message').html(e.relatedTarget.dataset.message);    
        $('#modalNotify').on('shown.bs.modal', function(){

        if(status == 0){
            $.ajax({
                type: 'GET',
                url: '/dashboard/read_notice/'+id+'/'+user,
                cache: false,
                success: function(data){
                    // $('#notice').html();
                    if(data){
                        tag.attr('class','bs-a');
                    }
                    console.log(data);
                }
            });
             return false;
        }  
        });  
    });   
}

function que_send() {
   $('#btn-fwp-form').on('click', function(e){
        var data = $('#fwp-form').serialize();
        send_details(data,$('#water-inner'));
    });

    $('#btn-per-form').on('click', function(e){
        var data = $('#per-form').serialize();
        send_details(data,$('#person-inner'));
    });


    $('#btn-eli-form').on('click', function(e){

        $("#litter-check").change(function () {
            //check if its checked. If checked move inside and check for others value
            if (!this.checked && this.value === "other") {
                //remove if unchecked
                $("#litter-box").val('');
            }
        });
        var data = $('#eli-form').serialize();
        send_details(data,$('#eliminate-inner'));
    });

    $('#btn-exe-form').on('click', function(e){

        var data = $('#exe-form').serialize();
        send_details(data,$('#exercise-inner'));
    });

    $('#btn-nap-form').on('click', function(e){

        var data = $('#nap-form').serialize();
        send_details(data,$('#napping-inner'));
    });

    $('#btn-med-form').on('click', function(e){
        $("#med-check").change(function () {
            //check if its checked. If checked move inside and check for others value
            if (!this.checked && this.value === "other") {
                //remove if unchecked
                $("#med-box").val('');
            }
        });
        var data = $('#med-form').serialize();
        send_details(data,$('#medical-inner'));
    });

    $('#btn-eme-form').on('click', function(e){

        var data = $('#eme-form').serialize();
        send_details(data,$('#emergency-inner'));
    });

    $('#btn-hme-form').on('click', function(e){

        var data = $('#hme-form').serialize();
        send_details(data,$('#home-inner'));
    });
     $('#btn-otherinfo-form').on('click', function(e){

        var data = $('#otherinfo-form').serialize();
        send_details(data,$('#otherinfo-inner'));
    });
}

function send_details(argument, div) {
    $.ajax({
        type: 'POST',
        url: '../../cat/add_details',
        cache: false,
        data: {data : argument},
        beforeSend: function(){
            $('.loading').show();
        },
        success: function(data){
            // $(div).html(data);
            // console.log(data);
        },
        complete: function(){
            $('.loading').hide();
        }
    });
     return false;
 }

function paypal_month(){
      paypal.Button.render({

            env: 'sandbox', // sandbox | production

            style: {
            label: 'paypal',
            size:  'responsive',    // small | medium | large | responsive
            shape: 'rect',     // pill | rect
            color: 'blue',     // gold | blue | silver | black
            tagline: false    
            },

            // PayPal Client IDs - replace with your own
            // Create a PayPal app: https://developer.paypal.com/developer/applications/create
            client: {
                sandbox:    'AQOcqweUODaPNotdNxrXms-s5ODzoQDwnlIf8N2rT9qZJlzkqedAR47xvU5AdcO4i2S3lPPojzVbxMhq',
                production: '<insert production client id>'
            },

            // Show the buyer a 'Pay Now' button in the checkout flow
            commit: true,

            // payment() is called when the button is clicked
            payment: function(data, actions) {

                // Make a call to the REST api to create the payment
                return actions.payment.create({
                    payment: {
                        transactions: [
                            {
                                amount: { total: '2.99', currency: 'USD' }
                            }
                        ]
                    }
                });
            },

            // onAuthorize() is called when the buyer approves the payment
            onAuthorize: function(data, actions) {

                // Make a call to the REST api to execute the payment
                return actions.payment.execute().then(function() {
                    // window.alert(data + actions);
                    make_payment(data,actions,0);  
                });
            }

        }, '#paypal-button-mth');
}

function paypal_year(){
      paypal.Button.render({

            env: 'sandbox', // sandbox | production

            style: {
            label: 'paypal',
            size:  'responsive',    // small | medium | large | responsive
            shape: 'rect',     // pill | rect
            color: 'blue',     // gold | blue | silver | black
            tagline: false    
            },

            // PayPal Client IDs - replace with your own
            // Create a PayPal app: https://developer.paypal.com/developer/applications/create
            client: {
                sandbox:    'AQOcqweUODaPNotdNxrXms-s5ODzoQDwnlIf8N2rT9qZJlzkqedAR47xvU5AdcO4i2S3lPPojzVbxMhq',
                production: '<insert production client id>'
            },

            // Show the buyer a 'Pay Now' button in the checkout flow
            commit: true,

            // payment() is called when the button is clicked
            payment: function(data, actions) {

                // Make a call to the REST api to create the payment
                return actions.payment.create({
                    payment: {
                        transactions: [
                            {
                                amount: { total: '29.99', currency: 'USD' }
                            }
                        ]
                    }
                });
            },

            // onAuthorize() is called when the buyer approves the payment
            onAuthorize: function(data, actions) {

                // Make a call to the REST api to execute the payment
                return actions.payment.execute().then(function() {
                    // window.alert(data + actions);
                    make_payment(data,actions,1);

                });
            }

        }, '#paypal-button-yr');
}

function make_payment(data, action, sub) 
{
    // console.log(data, action);
            // return
    $.ajax({
        type: 'POST',
        url: 'payment/make_paypal_payment',
        cache: false,
        data: {token : data.paymentToken, id : data.payerID, payment : data.paymentID, subscription : sub},
        beforeSend: function(){
            $('.loading').show();
        },
        success: function(d){
            if(data){
                location.reload();
            }
        },
        complete: function(){
            $('.loading').hide();
        }
    });
    return false;
}

function get_notification()
{
    $.ajax({
        type: 'GET',
        url: '/registry/dashboard/get_notice',
        cache: false,
        success: function(data){
            console.log(data);
            if(data != false){
                $.each(data, function(key,value) {
                // alert(value.com);
                });
            }
        }
    });
    return false;
}

function update_clock()
{
    var currentTime = new Date ( );
    var currentHours = currentTime.getHours ( );
    var currentMinutes = currentTime.getMinutes ( );
    var currentSeconds = currentTime.getSeconds ( );

    currentHours = ( currentHours < 10 ? "0" : "" ) + currentHours;
    currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
    currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;
    //var currentHours = (currentHours+24-2)%24; 
    var currentHours = (currentHours)%24;
    var mid='AM';
    if(currentHours==0){ //At 00 hours we need to show 12 am
        currentHours=12;
    }
    else if(currentHours>12)
    {
        currentHours=currentHours%12;
        mid='PM';
    }
    
    var currentTimeString = currentHours + " : " + currentMinutes + " : " + currentSeconds + " "+mid;

    $(".timer").html('<span class="fa fa-clock-o"></span> ' + currentTimeString);
}

function guardian_delete(id, image) 
{
    $.ajax({
        type: 'POST',
        url: 'guardian/delete_guardian',
        cache: false,
        data: {id : id, image : image},
        beforeSend: function(){
            $('.loading').show();
        },
        success: function(arg){
            $('.loading').hide();
            if(arg){
                location.reload();
            }else{
                alert('Error processing request');
            }
        },
        complete: function(){
            $('.loading').hide();
        }
    });
    return false;
}

function message_delete(args) 
{
    $.ajax({
        type: 'POST',
        url: 'service/delete_message',
        cache: false,
        data: {id : args},
        beforeSend: function(){
            $('.loading').show();
        },
        success: function(arg){
            $('.loading').hide();
            if(arg['status']){
                location.reload();
            }else{
                alert(arg['message']);
            }
        },
        complete: function(){
            $('.loading').hide();
        }
    });
    return false;
}

function message_delete_all() 
{
    $.ajax({
        type: 'POST',
        url: 'service/delete_message_all',
        cache: false,
        beforeSend: function(){
            $('.loading').show();
        },
        success: function(arg){
            $('.loading').hide();
            if(arg['status']){
                location.reload();
            }else{
                alert(arg['message']);
            }
        },
        complete: function(){
            $('.loading').hide();
        }
    });
    return false;
}

function readUrl(input, id) 
{
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(id).attr('src', e.target.result);
            $('#add_image').val('1');
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function groupEdit()
{
    $('#groupEdit').on('show.bs.modal' , function(e){
        $('.modal-body #group_name').val(e.relatedTarget.dataset.name);
        $('.modal-body #group_description').val(e.relatedTarget.dataset.desc);
        $('.modal-body #editGroupForm').attr("action", e.relatedTarget.dataset.url);
    });
}

function data_to_modal()
{
    $('#catDetail').on('show.bs.modal', function(e){
        var cat_array =JSON.parse(e.relatedTarget.dataset.cat);
        $('.modal-title').text(cat_array['name']);
        $('#cat-nick').text(cat_array['nickname']);
        $('#cat-micro').text(cat_array['micro_no']);
        $('#cat-age').text(cat_array['age']);        
        $('#cat-gender').text(cat_array['gender']);
        $('#cat-breed').text(cat_array['nickname']);
        $('#cat-coloring').text(cat_array['coloring']);
        $('#cat-neutered').text((cat_array['neutered']=='1')? 'Yes' : 'No');
        $('#cat-food').text(cat_array['food']);
        $('#cat-extra').text(cat_array['extra']);        
    });
}

function data_delete_modal()
{
    $('#deleteCat').on('show.bs.modal', function(e){
    $('.modal-title').text(e.relatedTarget.dataset.name);
    $('#delete-id').val(e.relatedTarget.dataset.edit);

    $('#btn-delete').click(function(){
        var id = $('#delete-id').val();
        var spin = $('#btn-delete').data('spin');
        $.ajax({
            type:'POST',
            url:'cat/delete_cat', 
            cache: false,
            data: {id : id},           // To unable request pages to be cached
            beforeSend: function(){
                $('#btn-delete').html(spin);
                $('#btn-disable').prop("disabled", true);
            },
            success: function(d){
                console.log(d);
                $('#btn-delete').html('Yes');
                if(d['status']){
                    $('#deleteCat').modal('hide');
                    location.reload();
                }else{
                    $('#ajax-message').text(d['message']);
                    $('#btn-disable').prop("disabled", false);
                }
            },
            error:function(xhr,status,msg){
                $('#btn-delete').html('Yes');
                $('#btn-disable').prop("disabled", false);
            }
            });
        return false;
        });
    });
    $('#deleteCat').on('hidden.bs.modal', function(){
        $('#ajax-message').text('');
    });
}

function data_edit_modal()
{
    var src = $('#edit_image').attr('src');
    $('#updateCat').on('show.bs.modal', function(e){
        var cat_array =JSON.parse(e.relatedTarget.dataset.edit);
        $('#edit_cat_name').val(cat_array['name']);
        $('#edit_nickname').val(cat_array['nickname']);
        $('#edit_micro_no').val(cat_array['micro_no']);
        $('#edit_age').val(cat_array['age']);        
        $('select[name="edit_gender"]').find('option[value="'+cat_array['gender']+'"]').attr("selected",true);
        $('#edit_breed').val(cat_array['breed']);
        $('#edit_coloring').val(cat_array['coloring']);
        $('select[name="edit_neutered"]').find('option[value="'+cat_array['neutered']+'"]').attr("selected",true);
        $('#edit_food').html(cat_array['food']);
        $('#edit_extra').html(cat_array['extra']); 
        $('#edit_id').val(cat_array['id']);
        var image = src + cat_array['image'];
        $('#edit_image').attr('src',image);
    });
    $('#updateCat').on('hidden.bs.modal', function(e){
        $('#edit_image').attr('src',src);  
        $('input').val(''); 
         $(this).removeData('bs.modal');    
    });
}

function greeting()
{
    var thehours = new Date().getHours();
    var themessage;
    var morning = ('<img src="assets/img/morning.png" class="greeting-icon"/> Good morning');
    var afternoon = ('<img src="assets/img/afternoon.png" class="greeting-icon"/> Good afternoon');
    var evening = ('<img src="assets/img/evening.png" class="greeting-icon"/> Good evening');
    var night = ('<img src="assets/img/night.png" class="greeting-icon"/> Good night');
    
    if (thehours >= 0 && thehours < 12) {
        themessage = morning; 
    } else if (thehours >= 12 && thehours < 17) {
        themessage = afternoon;
    } else if (thehours >= 17 && thehours < 21) {
        themessage = evening;
    
    } else if (thehours >= 21 && thehours < 24) {
        themessage = night;
    }
    $('.greeting').html(themessage);
}

/*function get_location(){
    var latitudeAndLongitude=document.getElementById("latitudeAndLongitude"),
    location={
        latitude:'',
        longitude:''
    };

    if (navigator.geolocation){
      navigator.geolocation.getCurrentPosition(showPosition);
    }
    else{
      // latitudeAndLongitude.innerHTML="Geolocation is not supported by this browser.";
      console.log('not supported');
    }

    function showPosition(position){ 
        location.latitude=position.coords.latitude;
        location.longitude=position.coords.longitude;
        // latitudeAndLongitude.innerHTML="Latitude: " + position.coords.latitude + 
        // "<br>Longitude: " + position.coords.longitude; 
        var geocoder = new google.maps.Geocoder();
        var latLng = new google.maps.LatLng(location.latitude, location.longitude);

     if (geocoder) {
        geocoder.geocode({ 'latLng': latLng}, function (results, status) {
           if (status == google.maps.GeocoderStatus.OK) {
             console.log(results[0].formatted_address); 
             // $('#address').html('Address:'+results[0].formatted_address);
           }
           else {
            // $('#address').html('Geocoding failed: '+status);
            console.log("Geocoding failed: " + status);
           }
        }); //geocoder.geocode()
      }      
    } //showPosition
}
*/
  // Docs at http://simpleweatherjs.com

/* Does your browser support geolocation? */
if ("geolocation" in navigator) {
  $('.js-geolocation').show(); 
} else {
  $('.js-geolocation').hide();
}

/* Where in the world are you? */
$('.js-geolocation').on('click', function() {
  navigator.geolocation.getCurrentPosition(function(position) {
    loadWeather(position.coords.latitude+','+position.coords.longitude); //load weather using your lat/lng coordinates
  });
});

/* 
* Test Locations
* Austin lat/long: 30.2676,-97.74298
* Austin WOEID: 2357536
*/

function loadWeather(location, woeid) {
  $.simpleWeather({
    location: location,
    woeid: woeid,
    unit: 'f',
    success: function(weather) {
      html = '<h4><i class="i icon-'+weather.code+'"></i> '+weather.temp+'&deg;'+weather.units.temp+'/'+weather.alt.temp+'&deg;C</h4>';
      html += '<ul><li>'+weather.city+', '+weather.region+'</li>';
      html += '<li class="currently">'+weather.currently+'</li></ul>';
      
      $("#weather").html(html);
    },
    error: function(error) {
      $("#weather").html('<p>'+error+'</p>');
    }
  });
}

/*Added cat code */


     $('input[name="age"]').keyup(function(e)
    {
          if (/\D/g.test(this.value))
          {
            // Filter non-digits from input value.
            this.value = this.value.replace(/\D/g, '');
          }
  }); 

    $('input[name="cat_name"]').keyup(function(e)
    {
         if (this.value.match(/[^a-zA-Z0-9 ]/g))
          {
            this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');

          }
   }); 

    $('input[name="nickname"]').keyup(function(e)
    {
         if (this.value.match(/[^a-zA-Z0-9 ]/g))
          {
            this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');

          }
   });

    $('input[name="breed"]').keyup(function(e)
    {
         if (this.value.match(/[^a-zA-Z0-9 ]/g))
          {
            this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');

          }
   });  


    $('#addcatbtn').on('click',function(e)
       {
            e.preventDefault();
            var age     = $("#age").val();
            var catname =$("#cat_name").val();
            var catbreed =$("#breed").val();
            var imgVal = $('#avatar_input').val();
            var ext = $('#avatar_input').val().split('.').pop().toLowerCase();
            
            if(imgVal=='')
            {
              
                 $("#error_catimage").show(); 
                 $("#error_catimage").text("Please upload image");
                 setTimeout(function(){
                   $('#error_catimage').hide();
                }, 3000);
                return false; 

            }else if($.inArray(ext, ['png','jpg','jpeg']) == -1){
                 $("#error_catimage").show();
                  $("#error_catimage").text("Please select png,jpg ,jpeg format images");
                setTimeout(function(){
                   $('#error_catimage').hide();
                }, 3000);
                
                 return false;
            }

             if(catname=='')
             {
               $("#error_catname").show(); 
               $("#error_catname").text("Please enter cat name");
                setTimeout(function(){
                   $('#error_catname').hide();
                }, 3000);
               return false;
             }

            if(age=='')
             {
               $("#error_age").show(); 
               $("#error_age").text("Age field requires numbers only");
                setTimeout(function(){
                   $('#error_age').hide();
                }, 3000);
               return false;
             }

             if(catbreed=='')
             {
               $("#error_catbreed").show(); 
               $("#error_catbreed").text("Please enter cat breed");
                setTimeout(function(){
                   $('#error_catbreed').hide();
                }, 3000);
               return false;
             }

            if(age!=='' && catname!=='' && catbreed!=='' && imgVal!==''){
                  $("#catform").submit();
            } 
        }); 

/*edit cat */

  $('input[name="edit_age"]').keyup(function(e)
    {
          if (/\D/g.test(this.value))
          {
            // Filter non-digits from input value.
            this.value = this.value.replace(/\D/g, '');
          }
  }); 

    $('input[name="edit_cat_name"]').keyup(function(e)
    {
         if (this.value.match(/[^a-zA-Z0-9 ]/g))
          {
            this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');

          }
   }); 

    $('input[name="edit_nickname"]').keyup(function(e)
    {
         if (this.value.match(/[^a-zA-Z0-9 ]/g))
          {
            this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');

          }
   });

    $('input[name="edit_breed"]').keyup(function(e)
    {
         if (this.value.match(/[^a-zA-Z0-9 ]/g))
          {
            this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');

          }
   });  
    
     $('#editcatbtn').on('click',function(e)
       {
            e.preventDefault();
            var editage     = $("#edit_age").val();
            var editcatname =$("#edit_cat_name").val();
            var editcatbreed =$("#edit_breed").val();
           
             if(editcatname=='')
             {
               $("#error_editcatname").show(); 
               $("#error_editcatname").text("Please enter cat name");
                setTimeout(function(){
                   $('#error_editcatname').hide();
                }, 3000);
               return false;
             }

            if(editage=='')
             {
               $("#error_editcatage").show(); 
               $("#error_editcatage").text("Age field requires numbers only");
                setTimeout(function(){
                   $('#error_editcatage').hide();
                }, 3000);
               return false;
             }

             if(editcatbreed=='')
             {
               $("#error_editcatbreed").show(); 
               $("#error_editcatbreed").text("Please enter cat breed");
                setTimeout(function(){
                   $('#error_editcatbreed').hide();
                }, 3000);
               return false;
             }

            if(editage!=='' && editcatname!=='' && editcatbreed!==''){
                  $("#editcatform").submit();
            } 
        }); 

/*end here */

 /*Add or edit guardian information */

     $('input[name="phone"]').keyup(function(e)
    {
          if (/\D/g.test(this.value))
          {
            // Filter non-digits from input value.
            this.value = this.value.replace(/\D/g, '');
          }
  }); 

   $('input[name="first_name"]').keyup(function(e)
    {
         if (this.value.match(/[^a-zA-Z0-9 ]/g))
          {
            this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');

          }
   });

    $('input[name="last_name"]').keyup(function(e)
    {
         if (this.value.match(/[^a-zA-Z0-9 ]/g))
          {
            this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');

          }
   });

   $('input[name="city"]').keyup(function(e)
    {
         if (this.value.match(/[^a-zA-Z0-9 ]/g))
          {
            this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');

          }
   });
    
    
   $(".edit_guardian").on('click',function(e){
   
      e.preventDefault();

      var editid=$(this).data("editid");

        $.ajax({
        type: 'POST',
        url: 'guardian/get_edit_guardian_details',
        cache: false,
        data: {id :editid},
        dataType:'json',
        success: function(data){
           
            $("#editguardianform #edit_first_name").val(data.first_name);
            $("#editguardianform #edit_last_name").val(data.last_name);
            $("#editguardianform #edit_phone").val(data.phone);
            $("#editguardianform #edit_identity").val(data.email);
            $("#editguardianform #edit_address1").val(data.address1);
            $("#editguardianform #edit_address2").val(data.address2);
            $("#editguardianform #edit_city").val(data.city);
            $("#editguardianform #edit_guardian_id").val(data.id);
                     
        }
        
    });

   }); 


   $("#addbtn").on('click',function(e){

    e.preventDefault();

            var fname =$("#first_name").val();
            var lname =$("#last_name").val();
            var phone =$("#phone").val();
            var emailidentity =$("#identity").val();
         
             if(fname=='')
             {
               $("#error_addname").show(); 
               $("#error_addname").text("Please enter first name");
                setTimeout(function(){
                   $('#error_addname').hide();
                }, 3000);
               return false;
             }

             if(lname=='')
             {
               $("#error_addlastname").show(); 
               $("#error_addlastname").text("Please enter last name");
                setTimeout(function(){
                   $('#error_addlastname').hide();
                }, 3000);
               return false;
             }

              if(phone=='')
             {
               $("#error_addphone").show(); 
               $("#error_addphone").text("Please enter phone number");
                setTimeout(function(){
                   $('#error_addphone').hide();
                }, 3000);
               return false;
             }

             if (!validateEmail(emailidentity))
              {
                 $("#error_addemail").show(); 
                 $("#error_addemail").text("Please enter valid email id");
                  setTimeout(function(){
                   $('#error_addemail').hide();
                }, 3000);
                 return false;
              }

               if(fname!=='' && lname!=='' && phone!=='' && emailidentity!=='')
              {
                             
                $("#addGuardian").hide();
                $(".modal-backdrop").hide();
                $("#addGuardianform").submit();
                setTimeout(function(){
                   alert("Guardian information saved successfully");
                }, 3000);
                
                
              }  

   });

   $("#editbtn").on('click',function(e){

            e.preventDefault();
            var editfname =$("#edit_first_name").val();
            var editlname =$("#edit_last_name").val();
            var edit_phone =$("#edit_phone").val();
            var edit_identity =$("#edit_identity").val();
         
             if(editfname=='')
             {
               $("#error_editname").show(); 
               $("#error_editname").text("Please enter first name");
                setTimeout(function(){
                   $('#error_editname').hide();
                }, 3000);
               return false;
             }

             if(editlname=='')
             {
               $("#error_editlastname").show(); 
               $("#error_editlastname").text("Please enter last name");
                setTimeout(function(){
                   $('#error_editlastname').hide();
                }, 3000);
               return false;
             }

              if(edit_phone=='')
             {
               $("#error_editphonenumber").show(); 
               $("#error_editphonenumber").text("Please enter phone number");
                setTimeout(function(){
                   $('#error_editphonenumber').hide();
                }, 3000);
               return false;
             }

             if (!validateEmail(edit_identity))
              {
                 $("#error_editemailid").show(); 
                 $("#error_editemailid").text("Please enter valid email id");
                  setTimeout(function(){
                   $('#error_editemailid').hide();
                }, 3000);
                 return false;
              }

              if(editfname!=='' && editlname!=='' && edit_phone!=='' && edit_identity!=='')
              {
                $("#editGuardian").hide();
                $(".modal-backdrop").hide();
                var data1 = $("#editguardianform").serialize();
                 $.ajax({
                        type: 'POST',
                        url: 'guardian/edit_guardian_data',
                        cache: false,
                        data: data1,
                        dataType:'json',
                        success: function(data)
                        {  
                            $("#editGuardian").hide();
                            $(".modal-backdrop").hide(); 
                           if(data.status=="success")
                           {
                            alert(data.message);
                            location.reload();
                           }
                            else
                            {
                             alert(data.message);
                            location.reload();
                                         
                            }

                        }
                        
                    });
              }

            
   });


  function validateEmail(email)
  {
      var re = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/;
      return re.test(String(email).toLowerCase());
  }

$("#editcardupdate").on('click',function(e){

   e.preventDefault();
  $.ajax({
    url: "setting/credit_card_update",
    type: "POST",
    dataType: "json",
    contentType: false,
    processData: false,
   
    success: function ($data) {
        if($data.status='success')
        {
          
           $("#creditcardModal").hide();
           $(".modal-backdrop").hide(); 
          alert("Customer service agent will be in touch with you to update your billing information");
                 
        }
        else {
             alert("something went wrong when sending mail");
             
        }
    }
});
     
  });
/*end here */

/*function get_form_on_show(que,cat,div) {
    $.ajax({
        type: 'get',
        url: 'cat/get_question_category',
        cache: false,
        data: {que : que , cat : cat},
        beforeSend: function(){
            $('.loading').show();
        },
        success: function(data){
            $(div).html(data);
            console.log(data);
        },
        complete: function(){
            $('.loading').hide();
        }
    });
     return false;
}*/


/* $('#table-btn-prev').click(function(){
    var data = $('#table-btn-prev').data('date');
    show_table(data);
});

$('#table-btn-next').click(function(){
    var v = $('#table-btn-next').data('date');
    show_table(v);
});

function show_table(s){
    $.ajax({
        type:'POST',
        url:'setting/show',
        cache: false,           // To unable request pages to be cached
        data: {date : s}, 
        beforeSend: function(){
           $('.loading').show();
        },
        success: function(d){
            $('#table-data').html(d['table']);
            $('#table-btn-next').data('date', d['next']);
            $('#table-btn-prev').data('date', d['prev']);
        },
        error: function (xhr,status, error) {
            // alert(error);
        },
        complete:function(){
            $('.loading').hide();
        }
    });
    return false;
}

function upadate_schedule(update, date) {
    var id = $('#hidden_id').val();
     $.ajax({
        type:'POST',
        url:'setting/update',
        cache: false,
        data: {value: update, id : id},           // To unable request pages to be cached
        beforeSend: function(){
           $('.loading').show();
        },
        success: function(d){
            show_table(date);
        },
        complete:function(){
            $('.loading').hide();
        }
    });
    return false;
}*/

//setting cancel_subscription_form
$(".password_text").on("keyup", function(){ $(".error_meass").text(' ');  });
$("#cancel_subscription_form").on("submit",function(e){  
  e.preventDefault();
  $.ajax({
    url: "setting/cancel_subscription",
    type: "POST",
    dataType: "json",
    contentType: false,
    processData: false,
    data: new FormData(this),
    success: function ($data) {
        if($data.status='success')
        {
           // alert("Account deactivated! To get back to us please activate it using the link sent to your mail");
            alert("Your subscription  canceled.You can login again");
           /* var strconfirm = confirm("Your subscription  canceled.You can login again");
            if (strconfirm == true) {
                return true;
                window.location.href = 'welcome';
            }*/
            window.location.href = 'welcome';
           // window.location.href = 'auth/logout';
           
        }
        else if($data.status='failed'){
            $(".error_meass").text('wrong password please try again');
        }
    }
});
});



  



