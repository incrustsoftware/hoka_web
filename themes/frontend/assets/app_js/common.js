var _accessToken;
var _userId;
var REQUEST_URL;
$(document).ready(function () {

    REQUEST_URL = getBaseUrl();
    if (checkSession() === false) {
        logout();
    }

    $(document).click(function() {
        if (checkSession() === false) {
            logout();
        }
    });
});

function checkSession() {
    if (!localStorage.getItem('token')) {
        return false; 
    }

    return true;
}

function getBaseUrl() {
    return $("#base_path").val();
}

function getBearerToken() {
    _accessToken = localStorage.getItem('token');

    return 'Bearer '+_accessToken;
}

function getUserId() {
    _userId = localStorage.getItem('user_id');

    return _userId;
}

function manageErrorResponse(xhr, error, option) {
    // function manageErrorResponse(xhr, error, option = ['ss']) {
    var option = option || [];

    if (xhr.status == 401) {
        logout();
    }

    if (xhr.responseJSON.Message) {
        errors = xhr.responseJSON.Message;
    } else {
        errors = error.Message;
    }

    if (Array.isArray(errors)) {
        $.each(errors, function( index, value) {
            $.toaster({ priority: 'danger', title: 'Error', message: value });
        });
    } else {
        $.toaster({ priority: 'danger', title: 'Error', message: errors });
    }


    // var error_html = '';
    // if (Array.isArray(errors)) {
    //     $.each(errors, function( index, value ) {
    //         error_html += bindErrorHtml(value);
    //     });
    // } else {
    //     error_html = bindErrorHtml(errors);
    // }

    // if (option['error_selector']) {
    //     $("#"+option['error_selector']).html(error_html);
    // } else {
    //     $("#error_message_block").html(error_html);
    // }
}

//Bind error messages.
function bindErrorHtml(error) {
    html = '<div class="alert alert-danger alert-dismissible" style="background-color: #ffdada; !important;">'+
                '<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>'+
                    '<strong>Error: </strong>' +error+
        '</div>';

    return html;
}