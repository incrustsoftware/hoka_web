//Global variables
var from_date;
var to_date;
var base_path;
var owlCarousel = null;
var global_complaint_order_invice = null;
var global_selected_invoicerecord = null;
var close_cases_table = null;
var current_slider_visibility;
var _Invoice_API_Error = 0;
var _common_login_url = 'https://claims.run.no'; //Server
//var _common_login_url = 'https://localhost/Hoka/hoka_web/'; //Local

/*
 * Screen resize event.
*/
$(window).resize(function(){
    //Run dynamic screen size changes
    mediaQueries(0);
});

/*
 * Document ready event.
*/
$(document).ready(function() {

    base_path = getBaseUrl();
    to_date = new Date();
    from_date = new Date();
    from_date.setDate(from_date.getDate() - 1);

    if (!localStorage.getItem('token')) {
        localStorage.clear();
        //login_url = $("#login_url").val();
        window.location.href = _common_login_url;
    }

    //Run dynamic screen size changes
    mediaQueries(1);

    //Get claims reason
    getClaimsReason();

    //Daterange picker for new complaint
    if($('.new_complaint_picker').length) {
        $('.new_complaint_picker input').each(function() {
          $(this).datepicker('clearDates');
        });
        $('.new_complaint_picker').datepicker({
            format: {
                toDisplay: 'yyyy-mm-dd',
                toValue: 'yyyy-mm-dd'
            },
        });
        $("#from_date").datepicker("update", from_date);
        $("#to_date").datepicker("update", to_date);

        generateNewComplaintReport();
    }

    // Initialize the multi line graph date range picker
    if($('.graph_date_range_picker').length) {
        $('.graph_date_range_picker input').each(function() {
          $(this).datepicker('clearDates');
        });
        $('.graph_date_range_picker').datepicker({
            format: {
                toDisplay: 'yyyy-mm-dd',
                toValue: 'yyyy-mm-dd'
            },
        });
        
        //Get the graph details with default 1 year data.
        getMultiLineComplaintGraph();
    }

    //Daterange picker for close cases
    if($('.close_cases_picker').length) {

        $('.close_cases_picker input').each(function() {
          $(this).datepicker('clearDates');
        });

        $('.close_cases_picker').datepicker({
            format: {
                toDisplay: 'yyyy-mm-dd',
                toValue: 'yyyy-mm-dd'
            },
        });

        close_cases_from_date = new Date();
        close_cases_from_date.setDate(close_cases_from_date.getDate() - 7);

        $("#close_cases_from_date").datepicker("update", close_cases_from_date);
        $("#close_cases_to_date").datepicker("update", to_date);

        if (checkSession() === true) {
            generateCloseCasesReport();
        } 
    }

    //Close cases datatable
   //close_cases_table = $('#close_cases').DataTable({
        // "dom": '<"bottom"i>rt<"top"flp><"clear">',
        // "aLengthMenu": [
        //     [5, 10, 15, 25, -1],
        //     [5, 10, 15, 25, "All"]
        // ],
        // "iDisplayLength": 10,
        // "language": {
        //     search: ""
        // },
    //});

    $('#close_cases').each(function() {
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.removeClass('form-control-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.removeClass('form-control-sm');
    });

    $('input[type=radio][name=optionsRadios]').change(function(event) {
        $("#radio_button_grid").addClass("hide");
        $("#accepted_message_box").removeClass("hide");
        $("#popup_back_button").removeClass("hide");

        $("#accepted_btn").removeAttr("disabled");
        $("#not_accepted_btn").removeAttr("disabled");
    });
});

/*
 * Generate new complaint report.
*/
function generateNewComplaintReport() {
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();

    var request = {};
    request.from_date = from_date;
    request.to_date = to_date;
    request.user_id = getUserId();

    $.LoadingOverlay("show");
    $.ajax({
        type:"GET",
        url:REQUEST_URL+'/api/dashboard/slider/get',
        data:request,
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', getBearerToken());
        },
        success: function(response) {
            $.LoadingOverlay("hide");
            //console.log('Dashboard Slider - '+JSON.stringify(response));
            if (response.Status) {
                if (response.Data.length > 0) {
                    $("#no_complaints_found").addClass("hide");
                    $("#complaint_slider").removeClass("hide");
                    bindSliderData(response.Data);
                } else {
                    $("#complaint_slider").addClass("hide");
                    $("#no_complaints_found").removeClass("hide");
                }
            }
        },
        error: function(xhr, error) {
            $.LoadingOverlay("hide");
            manageErrorResponse(xhr, error);
        }
    });
}

/*
 * Bind slider data.
*/
function bindSliderData(complaints) {
    var html = '';
    $("#complaint_slider").html('');
    $.each(complaints, function(index, complaint) {

        var details = complaint.product_details.split(/\n/);
        var detail_str = '';
        $.each(details, function(key, text) {
            detail_str += '<div>'+text+'</div>';
        });

        html += '<div class="item" onclick="viewProductDetails('+complaint.id+', this);" title="View Complaint Details">'+
                    '<div class="row">'+
                        '<div class="col-12">'+
                            '<div class="row image-grid">';


        var images = '';

        var complaint_image_length = complaint.images.length;
        var plus_images = complaint_image_length - 4;
        $.each(complaint.images, function(key, image) {
            if (key < 4) {
               if (key == 3 && complaint_image_length > 4) {
                    images += '<div class="col-6">'+
                            '<div class="thumbnail more_image_count_thumbnail">'+
                                '<img class="slider-image1 portrait" src="'+image.s3_product_image+'" style="opacity: 0.4;">'+
                                '<div class="more_image_count">+'+plus_images+'</div>'+
                            '</div>'+
                        '</div>';
                } else {
                    images += '<div class="col-6">'+
                            '<div class="thumbnail">'+
                                '<img class="slider-image1 portrait" src="'+image.s3_product_image+'">'+
                            '</div>'+
                        '</div>';
                }
            }

        });

        html += images+
                    '</div>'+  //Image grid complete
                    '<div class="row text-grid">'+
                        '<div class="col-12">'+
                            '<div class="text-block product_details">'+
                                //'<div class="product_details">'+detail_str+'</div>'+
                                '<div>'+complaint.gender+' '+complaint.model_name+' '+complaint.version+'</div>'+
                                '<div>'+complaint.model_number+' '+complaint.color_code+'</div>'+
                                '<div>'+complaint.manufacturer_code+'</div>'+
                                '<div>'+complaint.size+'</div>'+
                            '</div>'+
                            '<div class="text-block complaint_user_details">'+
                                '<div>'+complaint.dealer_name+'</div>'+
                                '<div>'+complaint.address1+'</div>'+
                                '<div>'+complaint.zip_code+' '+complaint.city+'</div>'+
                            '</div>'+
                            '<div class="text-block">'+
                                // '<div>claim type: '+complaint.claim_type+'</div>'+
                                '<div>purchase date: '+complaint.purchase_date+'</div>'+
                            '</div>'+
                            '<div class="text-block">'+
                                '<div>e-post: '+complaint.complaint_email+'</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    //'<button type="button" onclick="editComplaint('+complaint.id+', this);" class="btn btn-success btn-sm" title="Edit">Edit</button> '+
                    '</div></div></div>';
    });

    // console.log('Slider Data : ');
    // console.log(html);

    $("#complaint_slider").html(html);

    setTimeout(function(){

        var image_width = $(".more_image_count_thumbnail").innerWidth();
        left = (image_width/2) - 15;
        if (left > 25) {
            $(".more_image_count").css("left", left);
        }
        //alert(left);
     }, 1000);

    //Complaint slider
    if ($('.complaint_slider').length) {
        $('.complaint_slider').removeClass('owl-hidden');
        $('.complaint_slider').owlCarousel('destroy'); 
        $('.complaint_slider').owlCarousel({
          loop: false,
          items: 4,
          margin: 10,
          nav: true,
          autoplay: false,
          autoplayTimeout: 8500,
          //navigationText : ["prev","next"],
          navText: ["<i class='ti-angle-left'></i>", "<i class='ti-angle-right'></i>"],
          responsive: {
                0:{
                    items:1
                },
                768:{
                    items:3
                },
                1000:{
                    items:4
                }
            }
        });
    }  
}

/*
 * Display/View complaint details in popup.
*/
function viewProductDetails(id, visibility) {

    current_slider_visibility = visibility;
    _Invoice_API_Error = 0;

    $("#complaint_popup_footer2").hide();
    $("#accepted_message_box").addClass("hide");
    $("#popup_back_button").addClass("hide");
    $("#radio_button_grid").addClass("hide");
    $("#notaccepted_message_box").addClass("hide");
    $("#not_accept_back_button").addClass('hide');
    
    $('input[type=radio][name=optionsRadios]').prop('checked', false);
    $("#not_accepted_btn").removeClass("hide");
    $("#not_accepted_btn2").removeClass("hide");
    $("#accepted_btn").show();
    $("#accepted_btn2").show();
    $("#accepted_btn").removeAttr("disabled", "disabled");
    $("#not_accepted_btn").removeAttr("disabled", "disabled");
    

    var request = {};
    request.complaint_id = id;
    request.user_id = getUserId();

    $.LoadingOverlay("show");
    $.ajax({
        type:"GET",
        url:REQUEST_URL+'/api/dashboard/popup/get',
        data:request,
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', getBearerToken());
        },
        success: function(response) {
            $.LoadingOverlay("hide");
            //console.log('Dashboard Slider - '+JSON.stringify(response));
            if (response.Status) {
                bindComplaintPopupData(response.Data, 'bind');
            }
        },
        error: function(xhr, error) {
            $.LoadingOverlay("hide");
            manageErrorResponse(xhr, error);
        }
    });
}

/*
 * Bind complaint popup details.
*/
function bindComplaintPopupData_old(complaint) {

    $("#popup_forhandler_name").text(complaint.dealer_name);
    $("#complaint_number").text('Complaint '+complaint.id);
    $("#_popup_complaint_id").val(complaint.id);

    var details = complaint.product_details.split(/\n/);
    var detail_str = '';
    $.each(details, function(key, text) {
        detail_str += '<div>'+text+'</div>';
    });

    $("#popup_product_complaint_details").html('<div class="product_details">'+detail_str+'</div>');

    var images = '';
    // $.each(complaint.images, function(key, image) {
    //     images += '<div class="col-6">'+
    //                 '<div class="thumbnail">'+
    //                     '<img class="slider-popup-image1 portrait" src="'+base_path+'/'+image.product_image+'">'+
    //                 '</div>'+
    //             '</div>';
    // });

     $.each(complaint.images, function(key, image) {
        images +=   '<li>'+
                        '<img src="'+image.s3_product_image+'" /></li>'
                    '</li>';
    });

    var slider_html = '<div class="exzoom exzoomParent" id="exzoom">'+
                        '<div class="exzoom_nav"></div>'+
                        '<p class="exzoom_btn">'+
                           ' <a href="javascript:void(0);" class="exzoom_prev_btn"> < </a>'+
                            '<a href="javascript:void(0);" class="exzoom_next_btn"> > </a>'+
                        '</p>'+
                        '<div class="exzoom_img_box">'+
                            '<ul class="exzoom_img_ul" id="popup_complaint_images">'+
                               
                            '</ul>'+
                        '</div>'+
                    '</div>';

    $('.exzoomWrapper').empty();
    $('.exzoomWrapper').append(slider_html);
    $('.exzoomWrapper').find("#popup_complaint_images").html(images);
    $('#complaint_modal_popup').modal('show');

    $('.exzoomWrapper').find("#exzoom").exzoom({
        autoPlay: false
    });
    $('.exzoomWrapper').find("#exzoom").removeClass('hidden');

    //https://www.jqueryscript.net/slider/Product-Carousel-Magnifying-Effect-exzoom.html    
}

/*
 * Bind complaint popup details.
*/
function bindComplaintPopupData(complaint, flag) {

    $("#popup_forhandler_name").text(complaint.dealer_name);
    $("#complaint_number").text('Complaint '+complaint.complaint_number);
    $("#_popup_complaint_id").val(complaint.id);
    $("#_company_name").val(complaint.company);
    $("#_complaint_number").val(complaint.complaint_number);
    $("#_complaint_received_date").val(complaint.received_date);

    comp_id = complaint.id;

    // var details = complaint.product_details.split(/\n/);
    // var detail_str = '';
    // $.each(details, function(key, text) {
    //     detail_str += '<div>'+text+'</div>';
    // });

    var detail_str = '<div>'+complaint.gender+' '+complaint.model_name+' '+complaint.version+'</div>'+
    '<div>'+complaint.model_number+' '+complaint.color_code+'</div>'+
    '<div>'+complaint.manufacturer_code+'</div>'+
    '<div>'+complaint.size+'</div>';

    $("#popup_product_complaint_details").html('<div class="product_details">'+detail_str+'</div>');

    var images = '';
    var complaint_image_length = complaint.images.length;
    var plus_images = complaint_image_length - 4;
    $.each(complaint.images, function(key, image) {

        var dyn_id = 'pupup_image_'+comp_id+key;
        var par_dyn_id = 'thumbnail_pupup_image_'+comp_id+key;
        if (key == 3 && complaint_image_length > 4) {
            images += '<div class="col-6">'+
                    '<div class="thumbnail more_image_count_popup_thumbnail" id="'+par_dyn_id+'">'+ //#myGallery
                        '<a data-magnify="gallery" data-caption="Complaint images" href="'+image.s3_product_image+'"><img title="Click to view complaint image" id="'+dyn_id+'" class="slider-popup-image1 portrait" src="'+image.s3_product_image+'" style="opacity: 0.4;"></a>'+ //data-toggle="modal" data-target="#myModal"
                        '<div class="more_image_count_popup">+'+plus_images+'</div>'+
                    '</div>'+
                '</div>';
        } else {

            if (key < 4) {
                images += '<div class="col-6">'+
                        '<div class="thumbnail" id="'+par_dyn_id+'">'+ //#myGallery
                            '<a data-magnify="gallery" data-caption="Complaint images" href="'+image.s3_product_image+'"><img title="Click to view complaint image" id="'+dyn_id+'" class="slider-popup-image1 portrait" src="'+image.s3_product_image+'"></a>'+ //data-toggle="modal" data-target="#myModal"
                        '</div>'+
                    '</div>';
            } else {
                images += '<div class="col-6" style="display:none;">'+
                        '<div class="thumbnail" id="'+par_dyn_id+'">'+ //#myGallery
                            '<a data-magnify="gallery" data-caption="Complaint images" href="'+image.s3_product_image+'"><img title="Click to view complaint image" id="'+dyn_id+'" class="slider-popup-image1 portrait" src="'+image.s3_product_image+'"></a>'+ //data-toggle="modal" data-target="#myModal"
                        '</div>'+
                    '</div>';
            }
        }
       
    });

    var edit_complaint = ' <i class="fa fa-edit" title="Edit Complaint" onclick="editComplaint('+complaint.id+', this);"></i>';    
    
    $("#edit_icon_block").html(edit_complaint);
    $("#popup_complaint_images").html(images);

    setTimeout(function(){ 
        var image_width = $(".more_image_count_popup_thumbnail").innerWidth();
        left = (image_width/2) - 15;
        if (left > 25) {
            $(".more_image_count_popup").css("left", left);
        }
     }, 1000);

    if (flag == 'bind') {
        $('#complaint_modal_popup').modal('show');
    }

    if (complaint.is_sport == 1) {
        _Invoice_API_Error = 1;
        $("#complaint_popup_footer2").show();
    }

    // $(".thumbnail img").mouseover(function(){
    //     var id = $(this).attr('id');
    //     var image_zoom_selector = 'thumbnail_'+id;
    //     complaintImageZoom(image_zoom_selector, this);
        
    // });

    //imageMagnify();

    //driftZoom();
}

function imageMagnify() {
    var options = {};
    options.draggable = false;
    options.resizable = true;
    options.movable = false;
    options.keyboard = true;
    options.title = false;
    // options.modalWidth = 320;
    // options.modalHeight = 320;
    options.fixedContent = true;
    options.initMaximized = true;
    options.initMaximized = 0.02;
    options.ratioThreshold = 0.01;
    options.minRatio = 0.05;
    options.maxRatio = 16;
    options.headerToolbar = ['close'];
    options.footerToolbar = ['zoomIn','zoomOut','prev','fullscreen','next','actualSize','rotateRight'];
    $('[data-magnify=gallery]').magnify(options);
}

/*
 * Initialize the image zoom plugin.
*/
function complaintImageZoom(selector, thisele) {
    // new Drift(document.querySelector('.drift-demo-trigger:first-child'), {
    //     paneContainer: document.querySelector('.detail'),
    //     inlinePane: 900,
    //     inlineOffsetY: -85,
    //     containInline: true,
    //     hoverBoundingBox: true
    // });

    var options = {
        width: 100,
        //height:100,
        //zoomWidth: 500,
        fillContainer: true,
        zoomContainer:document.getElementById('zoom_view'),
        //offset: {vertical: 0, horizontal: 10},
        //scale: 3,
        //zoomPosition: 'left'
    };

    if($(window).width() > 767) {
         options.zoomPosition = 'right';
    } else {
         options.zoomPosition = 'bottom';
    }

   

    //$(".thumbnail img:first-child");

    window.imageZoom = new ImageZoom(document.getElementById(selector), options);
    //https://www.cssscript.com/zoom-image-on-hover/

    $(thisele).removeAttr('style');
}

/*
 * Show invoices block to accept the complaint.
*/
function showRadioGrid() {

    if (_Invoice_API_Error == 0) {
        $("#accepted_message_box").addClass("hide");
        $("#popup_back_button").addClass("hide");
        $("#radio_button_grid").removeClass("hide");
        $("#accepted_btn").attr("disabled", "disabled");
        $("#not_accepted_btn").attr("disabled", "disabled");
        $('input[type=radio][name=optionsRadios]').prop('checked', false);
    } else {
        $("#popup_back_button").addClass("hide");
        $("#accepted_message_box").addClass("hide");
        $("#not_accepted_btn").removeClass("hide");
        $("#not_accepted_btn2").removeClass("hide");
        $("#accepted_btn2").show();
        $("#accepted_btn").show();
        $("#complaint_popup_footer2").show();
    }
}

/*
 * This method is used to call the invoice and complaint accept API.
*/
function accepted(id) {

    //_Invoice_API_Error = 1;

    if (_Invoice_API_Error == 0) {
        if ($("#not_accepted_btn").hasClass("hide")) {
            // Submit invoice accepted request
            submitInvoiceRequest();
        } else {
            //Get invoices data
            $("#complaint_messagebox").html('');
            getInvoicesData();
        }
    } else if (_Invoice_API_Error == 1) {

        //alert(_Invoice_API_Error);

        if ($("#not_accepted_btn").hasClass("hide")) {

            submitInvoiceErrorRequest();
        } else {
            $("#not_accepted_btn").addClass("hide");
            $("#not_accepted_btn2").addClass("hide");

            if (id == 1) {
                $("#accepted_btn2").hide();
            } else {
                $("#accepted_btn").hide();
            }

            $("#complaint_messagebox").html('');
            var box = 'Hei,&#13;&#10;&#13;&#10;vi har behandlet reklamasjonssaken &#13;&#10;&#13;&#10;med vennlig hilsen';
            $("#complaint_messagebox").html(box);
            $("#accepted_message_box").removeClass("hide");
            $("#popup_back_button").removeClass("hide");
        }
    } else {

    }

}

/*
 * This method is used to accept the complaint.
*/
function submitInvoiceRequest() {
    popup_complaint_id = $("#_popup_complaint_id").val();
    popup_customer_id = $("#_popup_customer_id").val();
    complaint_messagebox = $("#complaint_messagebox").val();

    var request = {};

    if (_Invoice_API_Error == 0) {
        var InvoiceLines = global_selected_invoicerecord.InvoiceLines[0];
        request.invoice_no = global_selected_invoicerecord.InvoiceNo;
        request.line_id = InvoiceLines.LineID;
        request.order_no = InvoiceLines.OrderNo;
        request.article_no = InvoiceLines.ArticleNo;
        request.quantity = InvoiceLines.Quantity;
        request.net_price = InvoiceLines.NetPrice;
        request.discount1 = InvoiceLines.Discount1;
        request.discount2 = InvoiceLines.Discount2;
        request.discount3 = InvoiceLines.Discount3;
        request.credit_note_endpoint = InvoiceLines.CreditnoteEndpoint;
        request.credit_note_body = InvoiceLines.CreditnoteBody;
        request.invoice_date = global_selected_invoicerecord.InvoiceDate;
        request.json_data = JSON.stringify(global_selected_invoicerecord);
    }

    request.product_complaint_data_id = popup_complaint_id;
    request.customer_id = popup_customer_id;
    request.accept_message = complaint_messagebox;
    request.is_active = 1;
    request.is_delete = 0;
    request.complaint_status = 1;
    //request.invoice_api_error = _Invoice_API_Error;
    request.user_id = getUserId();

    $.LoadingOverlay("show");
    $.ajax({
        type:"POST",
        url:REQUEST_URL+'/api/dashboard/popup/accept',
        data:request,
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', getBearerToken());
        },
        success: function(response) {
            $.LoadingOverlay("hide");
            //console.log('Dashboard Popup Accept - '+JSON.stringify(response));
            if (response.Status) {
                $.toaster({ priority: 'success', title: 'Success', message: response.Message });
                $('#complaint_modal_popup').modal('hide');
                generateNewComplaintReport();
                generateCloseCasesReport();
            }
        },
        error: function(xhr, error) {
            $.LoadingOverlay("hide");
            manageErrorResponse(xhr, error);
        }
    });
}

/*
 * This method is used to accept the complaint.
*/
function submitInvoiceErrorRequest() {
    popup_complaint_id = $("#_popup_complaint_id").val();
    complaint_messagebox = $("#complaint_messagebox").val();

    var request = {};

    request.product_complaint_data_id = popup_complaint_id;
    request.accept_message = complaint_messagebox;
    request.is_active = 1;
    request.is_delete = 0;
    request.complaint_status = 1;
    //request.invoice_api_error = _Invoice_API_Error;
    request.user_id = getUserId();

    $.LoadingOverlay("show");
    $.ajax({
        type:"POST",
        url:REQUEST_URL+'/api/dashboard/invoicerrror/accept',
        data:request,
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', getBearerToken());
        },
        success: function(response) {
            $.LoadingOverlay("hide");
            //console.log('Dashboard Popup Accept - '+JSON.stringify(response));
            if (response.Status) {
                $.toaster({ priority: 'success', title: 'Success', message: response.Message });
                $('#complaint_modal_popup').modal('hide');
                generateNewComplaintReport();
                generateCloseCasesReport();
            }
        },
        error: function(xhr, error) {
            $.LoadingOverlay("hide");
            manageErrorResponse(xhr, error);
        }
    });
}

/*
 * This method is used to get invoices data.
*/
function getInvoicesData() {
    //$("#not_accepted_btn").addClass("hide");
    complaint_id = $("#_popup_complaint_id").val();
    _Invoice_API_Error = 0;

    var request = {};
    request.complaint_id = complaint_id;
    request.user_id = getUserId();

    $.LoadingOverlay("show");
    $.ajax({
        type:"GET",
        url:REQUEST_URL+'/api/dashboard/popup/invoice',
        data:request,
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', getBearerToken());
        },
        success: function(response) {
            $.LoadingOverlay("hide");
            //console.log('Complaint accepted - '+JSON.stringify(response));
            if (response.Status) {

                $("#not_accepted_btn").addClass("hide");

                var complaint = response.Data;
                $("#_popup_customer_id").val(complaint.invoices.CustomerNo);
                var invoices = complaint.invoices
                if (invoices.Invoices) {
                    invoices = invoices.Invoices;
                } else {
                    invoices = [];
                }

                global_complaint_order_invice = invoices;

                var invoices_data = '';
                $.each(invoices, function(key, invoice) {

                    var InvoiceNo = invoice.InvoiceNo;
                    var InvoiceDate = invoice.InvoiceDate;
                    var LineID = invoice.InvoiceLines[0].LineID;
                    var OrderNo = invoice.InvoiceLines[0].OrderNo;
                    var ArticleNo = invoice.InvoiceLines[0].ArticleNo;
                    var Quantity = invoice.InvoiceLines[0].Quantity;
                    var NetPrice = invoice.InvoiceLines[0].NetPrice;
                    var Discount1 = invoice.InvoiceLines[0].Discount1;

                    invoices_data += '<div class="form-check">'+
                        '<label class="form-check-label radio_label">'+
                            '<input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios'+LineID+'" value="'+key+'">'+
                            'InviceNo:'+InvoiceNo+', Line ID:'+LineID+', OrderNo:'+OrderNo+', ArticleNo:'+ArticleNo+', NetPrice:'+NetPrice+', Discount1:'+Discount1+
                            '<i class="input-helper"></i></label>'+
                        '</label>'+
                    '</div>';
                });

                $("#invoices_data").html(invoices_data);
                $("#radio_button_grid").removeClass("hide");
                $("#accepted_btn").attr("disabled", "disabled");

                $('input[type=radio][name=optionsRadios]').change(function(event) {
                    $("#radio_button_grid").addClass("hide");
                    $("#accepted_message_box").removeClass("hide");
                    $("#popup_back_button").removeClass("hide");

                    $("#accepted_btn").removeAttr("disabled");

                    var key = $(this).val();
                    global_selected_invoicerecord = global_complaint_order_invice[key];
                    order_no = global_selected_invoicerecord.InvoiceLines[0].OrderNo;

                    var company_name = $("#_company_name").val();
                    var complaint_number = $("#_complaint_number").val();

                    var box = "Hei, "+company_name+
                        "&#13;&#10;&#13;&#10;vi har behandlet reklamasjonssak "+complaint_number+"- denne er godkjent og vi vil kredittere ordrenummer "+order_no+"."+
                        "&#13;&#10;&#13;&#10;med vennlig hilsen";
                    $("#complaint_messagebox").html(box);
                });
            }
        },
        error: function(xhr, error) {
            $.LoadingOverlay("hide");
            $("#not_accepted_btn").removeClass("hide");
            manageErrorResponse(xhr, error);

            if (xhr.responseJSON.Data) {
                var Invoice_API_Error = xhr.responseJSON.Data.Invoice_API_Error;
            } else {
                var Invoice_API_Error = error.Data.Invoice_API_Error;
            }

            _Invoice_API_Error = Invoice_API_Error;

            if (Invoice_API_Error == 1) {
                $("#complaint_popup_footer2").show();
            }
        }
    });
}

/*
 * Back button for not accepted screen.
*/
function notAcceptBackButton() {
    $("#notaccepted_message_box").addClass('hide');
    $("#not_accept_back_button").addClass('hide');
    $("#not_accepted_btn").removeClass('hide');
    $("#not_accepted_btn2").removeClass('hide');
    $("#accepted_btn").show();
    $("#accepted_btn2").show();

    if (_Invoice_API_Error == 1) {
        $("#complaint_popup_footer2").show();
    }
}

/*
 * This method is used to reject the complaint.
*/
function notAccepted(id) {

    if ($("#notaccepted_message_box").hasClass('hide')) {

        var company_name = $("#_company_name").val();
        var complaint_number = $("#_complaint_number").val();
        var complaint_received_date = $("#_complaint_received_date").val();
        var complaint_messagebox2 = "Hei, "+company_name+
                "&#13;&#10;&#13;&#10;Vi har gått igjennom reklamasjonen mottatt "+complaint_received_date+", med reklamasjonsnummer "+complaint_number+
                "&#13;&#10;&#13;&#10;Denne er ikke godkjent i tråd med våre retningslinjer for retur og klage"+
                "&#13;&#10;&#13;&#10;"+
                "Med vennlig hilsen";


        $("#complaint_messagebox2").html(complaint_messagebox2);
        $("#notaccepted_message_box").removeClass('hide');
        $("#not_accept_back_button").removeClass('hide');
        $("#accepted_btn").hide();

        if (id == 2) {
            $("#accepted_btn2").hide();
            $("#not_accepted_btn").addClass('hide');
        }

        if (id == 1) {
            $("#complaint_popup_footer2").hide();
        }

        return;
    }

    complaint_id = $("#_popup_complaint_id").val();
    notaccept_message = $("#complaint_messagebox2").val();

    var request = {};
    request.id = complaint_id;
    request.not_accept_message = notaccept_message;
    request.user_id = getUserId();

    $.LoadingOverlay("show");
    $.ajax({
        type:"POST",
        url:REQUEST_URL+'/api/dashboard/popup/notaccept',
        data:request,
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', getBearerToken());
        },
        success: function(response) {
            $.LoadingOverlay("hide");
            //console.log('Not accepted - '+JSON.stringify(response));
            if (response.Status) {
                $.toaster({ priority: 'success', title: 'Success', message: response.Message });
                $('#complaint_modal_popup').modal('hide');
                generateNewComplaintReport();
                generateCloseCasesReport();
            }
        },
        error: function(xhr, error) {
            $.LoadingOverlay("hide");
            manageErrorResponse(xhr, error);
        }
    });
}

/*
 * This method is used to get complaint data for graph.
*/
function getMultiLineComplaintGraph() {
    var graph_from_date = $("#graph_from_date").val();
    var graph_to_date = $("#graph_to_date").val();

    var request = {};
    request.from_date = graph_from_date;
    request.to_date = graph_to_date;
    request.user_id = getUserId();

    $.LoadingOverlay("show");
    $.ajax({
        type:"GET",
        url:REQUEST_URL+'/api/dashboard/complaint/chart',
        data:request,
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', getBearerToken());
        },
        success: function(response) {
            $.LoadingOverlay("hide");
            //console.log('Dashboard complaint chart - '+JSON.stringify(response));
            if (response.Status) {
                drawLineChart(response.Data);
            }
        },
        error: function(xhr, error) {
            $.LoadingOverlay("hide");
            manageErrorResponse(xhr, error);
        }
    });
}

/*
 * This method is used to draw the line chart.
*/
function drawLineChart(data) {

    $("#complaint_graph").empty();
    $("#complaint_graph").html('<canvas id="linechart-multi"></canvas>');

    var lables = data.lables;
    var datasets = data.datasets;
    var datasetArr = [];
    var colors = ['#f54242','#70a331','#f5bf42', '#6b6a57', '#969e9a', '#389688', '#91c3cc', '#088bd1', '#63559e', '#0f0e0f', '#803980', '#380e2e', '#f00c8d', '#b80725', '#145957', '#cdd615', '#11f005', '#660bd6', '#0bd46c', '#c8ded6', '#a3c2d1', '#93a1c9', '#2d1569', '#8f87b0',];
    $.each(datasets, function(index, dataset) {

        var randomColor = Math.floor(Math.random()*16777215).toString(16);

        temp = {
            label: dataset.legend,
            data: dataset.value,
            borderColor: '#'+randomColor,//colors[index],
            backgroundColor: '#'+randomColor,//colors[index],
            borderWidth: 2,
            fill: false
        };

        datasetArr.push(temp);
    });
    
    //Line Chart
    var multiLineData = {
        labels: lables,
        datasets: datasetArr
    };

    if($(window).width() > 767) {
        position = 'bottom';
    } else {
        position = 'top';
    }

    var options = {
        responsive: true,
        title: { display: true, text: "Complaint Chart" },
        legend: { position:  position},
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                },
                scaleLabel: {
                    display: true,
                    labelString: "Complaints",
                }
            }],
            // xAxes: [{
            //     ticks: {
            //         beginAtZero: true
            //     },
            //     scaleLabel: {
            //         display: true,
            //         labelString: "Months",
            //     }
            // }]
        },
        tooltips: {
            mode: 'index',
            intersect: true,
        },
        hover: {
            mode: 'nearest',
            intersect: false
        },
        elements: {
            point: {
                radius: 0
            }
        }
    };

    if ($("#linechart-multi").length) {
        var multiLineCanvas = $("#linechart-multi").get(0).getContext("2d");
        var lineChart = new Chart(multiLineCanvas, {
            type: 'line',
            data: multiLineData,
            options: options,
        });
    }
}

/*
 * View close cases report.
*/
function generateCloseCasesReport() {
    var cases_from_date = $("#close_cases_from_date").val();
    var cases_to_date = $("#close_cases_to_date").val();

    var req_data = {};
    req_data.from_date = cases_from_date;
    req_data.to_date = cases_to_date;
    req_data.user_id = getUserId();

    if(close_cases_table != null) {
        close_cases_table.destroy();
        $('#close_cases tbody').empty();
    }

    $.LoadingOverlay('show');

    setTimeout(function() {
        $.LoadingOverlay('hide');
    }, 1000);

    close_cases_table = $('#close_cases').DataTable({
        "processing": true,
        "serverSide": true,
        "iDisplayLength": 10,
        "language": {
            search: ""
        },
        "aaSorting": [[0, "DESC"]],
        "ajax": {
            "url": REQUEST_URL + '/api/dashboard/close/cases',
            "dataType": "json",
            "type": "GET",
            "data": req_data,
            "beforeSend" : function(xhr) {
                xhr.setRequestHeader('Authorization', getBearerToken());
            },
        },
        "columns": [
            { "data": "complaint_number"},
            { "data": "customer_id"},
            { "data": "dealer_name"},
            { "data": "complaint_link"},
            { "data": "model_number"},
            { "data": "manufacturer_code"},
            { "data": "complaint_message"},
            { "data": "status"}
        ],
        "columnDefs": [
            { "orderable": false, "targets": [3,7] },
            //{ "orderable": true, "targets": [1, 2, 3] }
        ]
    });


    $("#close_cases_filter input").removeClass("form-control-sm");
}

/**
 * This function is use for close cases report in pdf format.
*/
function closeCasesPDF(flag) {

    var search = close_cases_table.search();
    var order = close_cases_table.order();
    var direction = order[0][1];
    var dircolumnnumber = order[0][0];
    var cases_from_date = $("#close_cases_from_date").val();
    var cases_to_date = $("#close_cases_to_date").val();

    var req_data = {};
    req_data.user_id = getUserId();
    req_data.from_date = cases_from_date;
    req_data.to_date = cases_to_date;
    req_data.search = search;
    req_data.order = dircolumnnumber;
    req_data.dir = direction;

    $.LoadingOverlay("show");
    url = REQUEST_URL+'/api/dashboard/export/closecasesreport';
    $.ajax({
        type: 'GET',
        dataType: 'json',
        data : req_data,
        url : url,
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', getBearerToken());
        },
        success: function(result) {
            $.LoadingOverlay("hide");
            //console.log('Export Data : '+JSON.stringify(result));
            if (result.Status == true) {
                var tabdata = result.Data.export;
                var dates = result.Data.dates;
                downloadPdf(tabdata, dates);
            } else {
                toastr.error("Sorry, No data found for downloading.");
            }
        },
        error: function (xhr, error) {
            $.LoadingOverlay("hide");
            manageErrorResponse(xhr, error);
        }
    });
}

/**
 * This method is used to download pdf file.
*/
function downloadPdf(tabdata, dates){
    var pdfstructure = {
            pagesize: 'A2',
            pageMargins: [ 10, 40, 10, 40 ],
            content: [
                { text: 'Close Cases Report', style: 'header2' },
                { text: 'Report Range: '+dates.from_date+' To '+dates.to_date},
                { text: 'Report Date: '+dates.current_date}
            ],
            styles: {
                header: {
                    fontSize: 18,
                    bold: true,
                    margin: [0, 0, 0, 10]
                },
                header2: {
                    fontSize: 18,
                    bold: true,
                    margin: [200, 0, 0, 10]
                },
                subheader: {
                    fontSize: 25,
                    bold: true,
                },
                tableExample: {
                    fontSize: 10,
                    margin: [0, 5, 0, 5]
                },
                tableHeader: {
                    bold: true,
                    fontSize: 12,
                    color: 'black',
                    fillColor: '#C0C0C0',
                }
            },
            defaultStyle: {}
    };

    var tab = {
        style: 'tableExample',
        table: {
            widths: [40,30, 60, 100, 50, 50, 120, 53],
            headerRows: 1,
            body: tabdata,
            pageBreak: 'after',
        }
    };

    pdfstructure.content.push(tab);
    var isIE = /*@cc_on!@*/ false || !!document.documentMode;
    if (isIE) {
        pdfMake.createPdf(pdfstructure).download("CloseCases.pdf");
    } else {
        pdfMake.createPdf(pdfstructure).print();
    }
}

/**
 * This method is used to appliy the media query.
*/
function mediaQueries(pageload = 1) {
    //console.log('mediaQueries');
    if($(window).width() < 767) {
        $("#gererate_complaint_report").removeClass('text-right');
        $("#gererate_complaint_report").addClass('m_t_10');
        $("#view_graph_button").removeClass('text-right');
        $("#view_graph_button").addClass('m_t_10');

        $("#generate_close_report_button").removeClass('text-center');
        $("#generate_close_report_button").addClass('m_t_10');
        $("#generate_close_pdf_button").removeClass('text-right');
        $("#generate_close_pdf_button").addClass('m_t_10');
        //$("#gererate_button_media_query").addClass('text-center');
        $("#complaint_popup_footer").removeClass('text-right');
        $("#complaint_popup_footer").addClass('text-center');

        $("#complaint_popup_first_block").removeClass('col-6');
        $("#complaint_popup_first_block").addClass('col-5');

    } else {
        $("#gererate_complaint_report").addClass('text-right');
        $("#gererate_complaint_report").removeClass('m_t_10');
        $("#view_graph_button").addClass('text-right');
        $("#view_graph_button").removeClass('m_t_10');

        $("#generate_close_report_button").addClass('text-center');
        $("#generate_close_report_button").removeClass('m_t_10');
        $("#generate_close_pdf_button").addClass('text-right');
        $("#generate_close_pdf_button").removeClass('m_t_10');

        $("#complaint_popup_footer").removeClass('text-center');
        $("#complaint_popup_footer").addClass('text-right');

        $("#complaint_popup_first_block").removeClass('col-5');
        $("#complaint_popup_first_block").addClass('col-6');
    }

    if (pageload == 0) {
        var image_width = $(".more_image_count_thumbnail").innerWidth();
        left = (image_width/2) - 15;
        if (left > 25) {
            $(".more_image_count").css("left", left);
        }

        var image_width = $(".more_image_count_popup_thumbnail").innerWidth();
        left = (image_width/2) - 15;
        if (left > 25) {
            $(".more_image_count_popup").css("left", left);
        }
    }
}

/**
 * This method is used to get complaint data for update.
*/
function editComplaint(complaint_id, selector) {
    var request = {};
    request.complaint_id = complaint_id;
    request.user_id = getUserId();

    $("#claims_reason").val('');
    $("#claim_type").val('');

    $.LoadingOverlay("show");
    $.ajax({
        type:"GET",
        url:REQUEST_URL+'/api/dashboard/popup/get',
        data:request,
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', getBearerToken());
        },
        success: function(response) {
            $.LoadingOverlay("hide");
            //console.log('Dashboard Slider - '+JSON.stringify(response));
            if (response.Status) {
                var edit_complaint = response.Data;
                $("#edit_complaint_id").val(edit_complaint.id);
                $("#model_name_input").val(edit_complaint.model_name);
                $("#model_number_input").val(edit_complaint.model_number);
                $("#color_code_input").val(edit_complaint.color_code);
                $("#manufacturer_code_input").val(edit_complaint.manufacturer_code);
                $("#size_input").val(edit_complaint.size);
                $("#gender_input").val(edit_complaint.gender);
                $("#version_input").val(edit_complaint.version);
                $("#free_text_input").val(edit_complaint.free_text);
                $("#customer_reference_number").val(edit_complaint.customer_reference_number);

                if (edit_complaint.claims_reason_id != null) {
                    $("#claims_reason").val(edit_complaint.claims_reason_id);
                }

                if (edit_complaint.claim_type != null) {
                    $("#claim_type").val(edit_complaint.claim_type);
                }

                $('#update_complaint').modal('show');
            }
        },
        error: function(xhr, error) {
            $.LoadingOverlay("hide");
            manageErrorResponse(xhr, error);
        }
    });
}

/**
 * This method is used to update complaint data.
*/
function updateComplaintData() {

    var edit_complaint_id = $("#edit_complaint_id").val();
    var model_name_input = $("#model_name_input").val();
    var model_number_input = $("#model_number_input").val();
    var color_code_input = $("#color_code_input").val();
    var manufacturer_code_input = $("#manufacturer_code_input").val();
    var size_input = $("#size_input").val();
    var gender_input = $("#gender_input").val();
    var version_input = $("#version_input").val();
    var claims_reason_id = $("#claims_reason").val();
    //var claim_type = $("#claim_type").val();
    var free_text_input = $("#free_text_input").val();
    var customer_reference_number = $("#customer_reference_number").val();

    var request = {};
    request.complaint_id = edit_complaint_id;
    request.model_name = model_name_input;
    request.model_number = model_number_input;
    request.color_code = color_code_input;
    request.manufacturer_code = manufacturer_code_input;
    request.size = size_input;
    request.gender = gender_input;
    request.version = version_input;
    request.claims_reason = claims_reason_id;
    //request.claim_type = claim_type;
    request.free_text = free_text_input;
    request.customer_reference_number = customer_reference_number;
    request.user_id = getUserId();

    $.LoadingOverlay("show");
    $.ajax({
        type:"POST",
        url:REQUEST_URL+'/api/complaint/edit/complaint',
        data:request,
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', getBearerToken());
        },
        success: function(response) {
            $.LoadingOverlay("hide");
            //console.log('Dashboard Slider - '+JSON.stringify(response));
            if (response.Status) {
                $.toaster({ priority: 'success', title: 'Success', message: response.Message });
                bindComplaintPopupData(response.Data, 'edit');
                $('#update_complaint').modal('hide');
                complaint = response.Data;
                var product_details = '<div>'+complaint.gender+' '+complaint.model_name+' '+complaint.version+'</div>'+
                '<div>'+complaint.model_number+' '+complaint.color_code+'</div>'+
                '<div>'+complaint.manufacturer_code+'</div>'+
                '<div>'+complaint.size+'</div>';

                $(current_slider_visibility).find('.product_details').html(product_details);
            }
        },
        error: function(xhr, error) {
            $.LoadingOverlay("hide");
            manageErrorResponse(xhr, error);
        }
    });
}

/*
 * Get claims reason
*/
function getClaimsReason() {

    var request = {};

    //$.LoadingOverlay("show");
    $.ajax({
        type:"GET",
        url:REQUEST_URL+'/api/dashboard/claims/list',
        data:request,
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', getBearerToken());
        },
        success: function(response) {
            //$.LoadingOverlay("hide");

            if (response.Status) {
                var claim_reason = '<option value="">Select claims reason</option>';
                $.each(response.Data, function(index, value) {
                    claim_reason += '<option value="'+value.id+'">'+value.id+'  '+value.name+'</option>';
                });

                $("#claims_reason").html(claim_reason);

                $("#my_html_body").removeAttr("style");

            }
        },
        error: function(xhr, error) {
            //$.LoadingOverlay("hide");
            manageErrorResponse(xhr, error);
        }
    });
}