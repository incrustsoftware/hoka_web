<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//**Login page**
Route::get('/', 'User\UserController@loginView');
Route::get('/login', 'User\UserController@loginView')->name('login');
Route::get('/logout', 'User\UserController@logoutView')->name('logout');
Route::get('/support', 'User\UserController@supportView')->name('support');

//**Dashboard**
Route::get('/dashboard/{user_token}/{web_token}', 'Dashboard\DashboardController@index')->name('dashboard.dashboard');
Route::get('/complaint/{token}', 'Dashboard\DashboardController@closeComplaintDetail');
