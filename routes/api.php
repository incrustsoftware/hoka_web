<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', 'User\UserController@login');
//Route::post('resetpassword', 'Users\UserController@resetPassword');
//Route::post('register', 'User\UserController@create');
//Route::get('emails', 'Complaint\ComplaintController@sendComplaintReceiveEmails');
//Route::post('s3/upload', 'Complaint\ComplaintController@uploadComplaintImagesToS3');
//https://claims.run.no/api/s3/upload

//Create a REST web services for users
Route::group(['prefix' => 'complaint'], function () {
    Route::post('store/new', 'Complaint\ComplaintController@registerComplaint');
    //Route::post('store', 'Complaint\ComplaintController@registerComplaintData');
    //Route::post('store/test', 'Complaint\ComplaintController@registerComplaintTest');
    //Route::post('read/image', 'Complaint\ComplaintController@getOCRData');
    //Route::post('read/image1', 'Complaint\ComplaintController@getOCRData1');
    //Route::get('token', 'Complaint\ComplaintController@manageTokens');
    //Route::get('complaint_number', 'Complaint\ComplaintController@updateAllComplaintNumber');
});

Route::group(['middleware' => ['auth:api', 'cors']], function () {
    
	//Create a REST web services for users
    Route::group(['prefix' => 'users'], function () {
        Route::get('get', 'User\UserController@show');
        Route::post('changepassword', 'User\UserController@changePassword');
        Route::get('logout', 'User\UserController@logout');
    });

    //Create a REST web services for users
    Route::group(['prefix' => 'complaint'], function () {
        Route::post('edit/complaint', 'Complaint\ComplaintController@editComplaintData');
    });

    //Create a REST web services for dashboard
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('slider/get', 'Dashboard\DashboardController@getComplaintDataForSlider');
        Route::get('popup/get', 'Dashboard\DashboardController@getComplaintData');
        Route::post('popup/accept', 'Dashboard\DashboardController@acceptComplaint');
        Route::post('invoicerrror/accept', 'Dashboard\DashboardController@acceptComplaintToInvoiceError');
        Route::post('popup/notaccept', 'Dashboard\DashboardController@notAcceptComplaint');
        Route::get('complaint/chart', 'Dashboard\DashboardController@getDashboardChart');
        Route::get('popup/invoice', 'Dashboard\DashboardController@getComplaintInvoiceData');
        Route::get('close/cases', 'Dashboard\DashboardController@getCloseCases');
        Route::get('export/closecasesreport', 'Dashboard\DashboardController@getCloseCasesReport');
        Route::get('claims/list', 'Dashboard\DashboardController@getClaimsReason');
    });
});