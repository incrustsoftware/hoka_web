<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_complaint_data_id')->default(NULL)->nullable(True)->unsigned();
            $table->integer('invoice_no')->nullable(False);
            $table->integer('line_id')->nullable(False);
            $table->integer('order_no')->nullable(False);
            $table->string('article_no')->nullable(True)->default(NULL);
            $table->integer('quantity')->nullable(False);
            $table->float('net_price', 8, 2)->nullable(True)->default(NULL);
            $table->float('discount1', 8, 2)->nullable(True)->default(NULL);
            $table->float('discount2', 8, 2)->nullable(True)->default(NULL);
            $table->float('discount3', 8, 2)->nullable(True)->default(NULL);
            $table->string('credit_note_endpoint', 500)->nullable(True)->default(NULL);
            $table->string('credit_note_body', 500)->nullable(True)->default(NULL);
            $table->DateTime('invoice_date')->nullable(True)->default(NULL);
            $table->string('accept_message', 1000)->nullable(True)->default(NULL);
            $table->json('json_data')->nullable(True)->default(NULL);
            $table->boolean('complaint_status')->default(NULL)->nullable(True);
            $table->boolean('is_active')->default(1)->nullable(True);
            $table->boolean('is_delete')->default(0)->nullable(True);
            $table->timestamps();

            $table->foreign('product_complaint_data_id')->references('id')->on('product_complaint_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_data');
    }
}
