<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateComplaintDataTableForClaimsReason extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_complaint_data', function ($table) {
            $table->integer('claims_reason_id')->after('token')->default(NULL)->nullable(True)->unsigned();

            $table->foreign('claims_reason_id')->references('id')->on('claims_reason');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('product_complaint_data', function ($table) {
            $table->dropForeign('product_complaint_data_claims_reason_id_foreign');
            $table->dropColumn('claims_reason_id');
        });
    }
}
