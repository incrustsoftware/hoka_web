<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname',50)->default(NULL)->nullable(True);
            $table->string('lastname',50)->default(NULL)->nullable(True);
            $table->string('mobile_number')->default(NULL)->nullable(True);
            $table->integer('role_id')->default(NULL)->nullable(True)->unsigned();
            $table->string('username')->nullable(False);
            $table->string('email',100)->nullable(False);
            $table->string('password',300)->nullable(False);
            $table->string('profile_picture',300)->nullable(True);
            $table->integer('created_by')->default(NULL)->nullable(True);
            $table->integer('updated_by')->default(NULL)->nullable(True);
            $table->boolean('is_active')->default(1)->nullable(true);
            $table->boolean('is_delete')->default(0)->nullable(true);
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('role_id')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
