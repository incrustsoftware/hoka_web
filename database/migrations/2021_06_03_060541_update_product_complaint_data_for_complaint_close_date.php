<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductComplaintDataForComplaintCloseDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_complaint_data', function ($table) {
            $table->dateTime('complaint_close_date')->after('complaint_status')->default(NULL)->nullable(True);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_complaint_data', function ($table) {
            $table->dropColumn('complaint_close_date');
        });
    }
}
