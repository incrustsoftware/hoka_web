<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductComplaintImagesForNewFilepath extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_complaint_images', function ($table) {
            $table->string('s3_product_image')->after('product_image')->default(NULL)->nullable(True);
            $table->string('product_image')->nullable(True)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_complaint_images', function ($table) {
            $table->dropColumn('s3_product_image');
            $table->string('product_image')->nullable(False)->change();
        });
    }
}
