<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductComplaintDataForInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_complaint_data', function ($table) {
            $table->boolean('complaint_status')->after('updated_by')->default(NULL)->nullable(True);
            $table->string('manufacturer_code')->after('product_details')->default(NULL)->nullable(True);
            $table->string('color_code')->after('product_details')->default(NULL)->nullable(True);
            $table->string('size')->after('product_details')->default(NULL)->nullable(True);
            $table->string('model_number')->after('product_details')->default(NULL)->nullable(True);
            $table->string('version')->after('product_details')->default(NULL)->nullable(True);
            $table->string('model_name')->after('product_details')->default(NULL)->nullable(True);
            $table->string('gender')->after('product_details')->default(NULL)->nullable(True);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_complaint_data', function ($table) {
            $table->dropColumn('gender');
            $table->dropColumn('model_name');
            $table->dropColumn('version');
            $table->dropColumn('model_number');
            $table->dropColumn('size');
            $table->dropColumn('color_code');
            $table->dropColumn('manufacturer_code');
            $table->dropColumn('complaint_status');
        });
    }
}
