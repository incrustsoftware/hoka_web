<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductComplaintImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_complaint_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_complaint_data_id')->default(NULL)->nullable(True)->unsigned();
            $table->string('product_image')->nullable(False);
            $table->integer('created_by')->default(NULL)->nullable(True);
            $table->integer('updated_by')->default(NULL)->nullable(True);
            $table->boolean('is_active')->default(1)->nullable(true);
            $table->boolean('is_delete')->default(0)->nullable(true);
            $table->timestamps();

            $table->foreign('product_complaint_data_id')->references('id')->on('product_complaint_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_complaint_images');
    }
}
