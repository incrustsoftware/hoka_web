<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductComplaintDataTableForClaimAndPurchasedate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_complaint_data', function ($table) {
            $table->date('purchase_date')->after('zip_code')->default(NULL)->nullable(True);
            $table->string('claim_type',20)->after('zip_code')->default(NULL)->nullable(True);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_complaint_data', function ($table) {
            $table->dropColumn('claim_type');
            $table->dropColumn('purchase_date');
        });
    }
}
