<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductComplaintDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_complaint_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->default(NULL)->nullable(True);
            $table->string('product_details')->default(NULL)->nullable(True);
            $table->string('dealer_name')->default(NULL)->nullable(True);
            $table->string('company')->default(NULL)->nullable(True);
            $table->string('complaint_email')->default(NULL)->nullable(True);
            $table->string('article_no')->default(NULL)->nullable(True);
            $table->string('phone')->default(NULL)->nullable(True);
            $table->string('city')->default(NULL)->nullable(True);
            $table->string('address1')->default(NULL)->nullable(True);
            $table->string('address2')->default(NULL)->nullable(True);
            $table->string('zip_code')->default(NULL)->nullable(True);
            $table->integer('created_by')->default(NULL)->nullable(True);
            $table->integer('updated_by')->default(NULL)->nullable(True);
            $table->boolean('is_active')->default(1)->nullable(true);
            $table->boolean('is_delete')->default(0)->nullable(true);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_complaint_data');
    }
}
