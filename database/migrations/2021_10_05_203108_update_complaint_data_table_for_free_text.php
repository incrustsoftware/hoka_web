<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateComplaintDataTableForFreeText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_complaint_data', function ($table) {
            $table->text('free_text')->after('claims_reason_id')->default(NULL)->nullable(True);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_complaint_data', function ($table) {
            $table->dropColumn('free_text');
        });
    }
}
