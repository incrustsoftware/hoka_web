<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductComplaintDataForCreditOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_complaint_data', function ($table) {
            $table->string('complaint_message', 1000)->after('complaint_status')->default(NULL)->nullable(True);
            $table->string('credit_order_number')->after('complaint_status')->default(NULL)->nullable(True);
            $table->string('token')->after('is_delete')->default(NULL)->nullable(True);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_complaint_data', function ($table) {
            $table->dropColumn('token');
            $table->dropColumn('credit_order_number');
            $table->dropColumn('complaint_message');
        });
    }
}
