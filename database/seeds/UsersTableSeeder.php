<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data['firstname'] = 'Admin';
        $data['lastname'] = 'User';
        $data['mobile_number'] = 9889765423;
        $data['role_id'] = 1;
        $data['username'] = 'admin';
        $data['email'] = 'admin@gmail.com';
        $data['password'] = bcrypt('qwerty');

        $user = User::where('is_active', 1)->first();

        if (!$user) {
	        User::create($data);
        }
    }
}
