<?php

use Illuminate\Database\Seeder;
use App\Model\Roles;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array("Admin");
        $roles = Roles::whereIn('rolename', $data)
        	->pluck('rolename')
        	->toArray();
        	 
        $inputData = array_diff($data, $roles);
        if (count($inputData) > 0) {
	        foreach ($inputData as $value) {
	            $add['rolename'] =  $value;
	            Roles::create($add);
	        }
        }
    }
}
