<?php

use Illuminate\Database\Seeder;
use App\Model\ClaimsReason;

class ClaimsReasonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array('BOND','WPROOF','STRAP','SEAM','MISM','LINNG','LEATH','MAT TEAR','HARDWARE','SOCKLNR/FBED','EYE','CRAK','COLOR','BLED');
        $claims_reason = ClaimsReason::whereIn('name', $data)
        	->pluck('name')
        	->toArray();
        	 
        $inputData = array_diff($data, $claims_reason);
        if (count($inputData) > 0) {
	        foreach ($inputData as $value) {
	            $add['name'] =  $value;
	            ClaimsReason::create($add);
	        }
        }
    }
}
